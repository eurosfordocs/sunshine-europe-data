install:
	pip3 install virtualenv
	virtualenv venv --python=python3
	. venv/bin/activate
	pip3 install -r requirements.txt
	python main.py schema

test:
	. venv/bin/activate
	python main.py schema
	PYTHONPATH=. pytest

.PHONY: install test
