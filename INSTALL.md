# Manual install procedure

This procedure has not been tested on Windows, only on Linux and Mac. 

## Dependancies

This project uses `Python3` and all librairies listed in [requirements](requirements.txt). 

We highly recommend using a virtual environment to install these libraries, the following instructions use virtualenv.

You can test that `Python3`, `pip3` and `virtualenv` are installed with these commands:

    python3 --version
    pip3 --version
    virtualenv --version

If you find an error please install / reinstall `Python3`, `pip3` and `virtualenv`, instruction depend on your OS. 

## Installation 

clone the project

    git clone https://gitlab.com/eurosfordocs/sunshine-europe.git
    cd sunshine-europe

Call the following commands successively: 

	virtualenv venv --python=python3
	. venv/bin/activate
	pip3 install -r requirements.txt
	python main.py schema

You can test the successful install by calling: 

    python -m pytest -x tests
