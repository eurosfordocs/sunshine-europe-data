# Sunshine Europe

Collecting sunshine database across Europe.


## Installation

After cloning this repository, use command 
```bash
make install
```
to create a virtualenv an install dependencies. See [INSTALL.md](INSTALL.md) for more details

## importing Schemas
Running `make install` will import schemas in folder `schemas` from the sunshing-europe-website repository, which is the source of truth:
https://gitlab.com/eurosfordocs/sunshine-europe-website/-/tree/master/schemas
It will also build Enums based on the fields of these schemas (`src/constants/schema_enums`)

If you need to reimport the schemas + rebuild the enums, call: 

```bash
python main.py schmema
```
## Usage

Folder organisation :
- [notebooks](notebooks) for exploratory work 
- [src](src) for common preparatory work and useful functions 
- [test](tests) for automatic tests


## PDF parser

For PDF countries, the parsing is more complex. For new files, the pdf parsing parsing part should be run in a terminal, at it may ask for user confirmation about the interpretation of columns headers. Have a look at the notebook "pdf_countries_all_steps.py"

## Data download / upload

The data is now separated from the codebase, stored in an OpenStack Swift container. The OpenStack credentials should be entered in the `.env` file. Once this is done, you can download data by calling: 
```bash
python main.py download <FOLDER> [-sf <SUBFOLDER>]
```
* `<FOLDER>` should be either `process_data` or `normalized_data`
* `-sf` can be any subfolder of the above folder, if ommited all subfolders are downloaded

Similarly, to upload data once you're happy with the resutls, you can call: 
```bash
python main.py upload <FOLDER> [-sf <SUBFOLDER>]
```

After uploading the data, we compare the list of local files vs distant files. If files are in the distant container but not in the local folder (eventually subfolder), it will ask for confirmation to delete the distant files too. 

Typical examples: 
```bash
python main.py download process_data -sf italy
python main.py upload process_data -sf italy
python main.py upload normalized_data
```