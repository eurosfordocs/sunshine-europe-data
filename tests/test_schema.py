import logging
import os
import glob

import pytest
from tableschema import Schema

from src.constants.schemas import SCHEMA_DIR, NORMALIZED_SCHEMA_DIR, NORMALIZED_DIR, PROCESS_DIR, HARMONIZED_SCHEMA_DIR
from src.utils import get_all_schema_path, validate_csv


@pytest.mark.parametrize('schema_path', get_all_schema_path(SCHEMA_DIR))
def test_tableschema_is_valid(schema_path):
    schema = Schema(schema_path)
    if not schema.valid:
        for error in schema.errors:
            logging.info("Error in schema at path '{}'".format(schema_path))
            logging.error(error)
        assert schema.valid


@pytest.mark.parametrize('schema_path', get_all_schema_path(SCHEMA_DIR))
def test_tableschema_name(schema_path):
    filename = os.path.basename(schema_path)[:-5]
    schema = Schema(schema_path)
    name = schema.descriptor["name"]
    assert name == filename


def test_get_all_schema_path_return_schemas():
    number_of_schemas = len(list(get_all_schema_path(SCHEMA_DIR)))
    assert number_of_schemas >= 2


def get_all_schema_harmonized_csv_pairs():
    fps = glob.glob(f'{PROCESS_DIR}/*/2_harmonized/*')
    logging.info(fps)
    schema_path = os.path.join(HARMONIZED_SCHEMA_DIR, f"link.json")
    for fp in fps:
        if fp.endswith('.csv'):
            yield schema_path, fp


def get_all_schema_normalized_csv_pairs():
    for table_name in ['activity', 'directory', 'entity', 'link', 'publication']:
        for dirpath, dirnames, filenames in os.walk(NORMALIZED_DIR):
            for file in filenames:
                if file.startswith(table_name + "__"):
                    assert file.endswith('.csv')
                    csv_path = os.path.join(dirpath, file)
                    schema_path = os.path.join(NORMALIZED_SCHEMA_DIR, f"{table_name}.json")
                    yield schema_path, csv_path


@pytest.mark.parametrize("schema_path,csv_path", get_all_schema_normalized_csv_pairs())
def test_normalized_schema(schema_path, csv_path):
    print(schema_path, csv_path)
    assert validate_csv(csv_path, schema_path)


@pytest.mark.parametrize("schema_path,csv_path", get_all_schema_harmonized_csv_pairs())
def test_harmonized_schema(schema_path, csv_path):
    print(schema_path, csv_path)
    assert validate_csv(csv_path, schema_path)
