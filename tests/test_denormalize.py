import pandas as pd
import numpy as np

from tableschema import Schema

from src.denormalize import list_columns_to_map, fit_dataframe_to_schema
from src.utils import get_all_schema

MOCK_SCHEMA = Schema(
    {"name": "activity", 
     "fields": [
        {"name": "hello"},
        {"name": "world"},
        {"name": "this"},
        {"name": "is"},
        {"name": "a"},
        {"name": "schema"},
        ]})

def test_list_columns_to_map():
    df = pd.DataFrame(columns=[])
    schema = get_all_schema()[0]

    assert list_columns_to_map(df, schema) == []

    df = pd.DataFrame(columns=schema.field_names)

    assert list_columns_to_map(df, schema) == []

    df = pd.DataFrame(columns=schema.field_names + ["not_in_schema", "not_in_schema2"])

    assert list_columns_to_map(df, schema) == ["not_in_schema", "not_in_schema2"]


def test_fit_dataframe_to_schema():
    df = pd.DataFrame(np.array([np.arange(10)]*3).T, columns=["world", "!", "hello"])
    schema = MOCK_SCHEMA

    df_schema = fit_dataframe_to_schema(df, schema)
    assert (df_schema.columns.values == schema.field_names).all()
    assert (df_schema["world"] == df["world"]).all() and (df_schema["hello"] == df_schema["hello"]).all()


