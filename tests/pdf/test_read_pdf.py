import pytest

from src.constants.efpia.header import N_MAX_HEADERS, N_MIN_HEADERS
from src.pdf.readers.camelot_pdf_reader import CamelotPdfReader

READER = CamelotPdfReader()
READER.dl_dir = "process_data/sweden/0_downloads"


def test_read_pdf() -> None:
    # No RND
    READER.pdf_filename = "sweden__actelion__2017.pdf"
    tableList, rnd_df = READER._read_pdf()
    assert N_MIN_HEADERS <= tableList[0].shape[1] <= N_MAX_HEADERS
    assert rnd_df is None

    # With RND as last page
    READER.pdf_filename = "sweden__gilead__2017.pdf"
    tableList, rnd_df = READER._read_pdf()
    assert N_MIN_HEADERS <= tableList[0].shape[1] <= N_MAX_HEADERS
    assert rnd_df is not None

    # With RND as last page
    READER.pdf_filename = "sweden__otsuka__2017.pdf"
    tableList, rnd_df = READER._read_pdf()
    assert N_MIN_HEADERS <= tableList[0].shape[1] <= N_MAX_HEADERS
    assert rnd_df.shape == (3,20)