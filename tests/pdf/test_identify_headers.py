import pytest
import pandas as pd

from src.constants.efpia.header import Header, OPTIONAL_HEADERS
from src.pdf.identify_headers import identify_headers, check_labels
from src.pdf.utils import Efpia_Pdf_Parsing_Exception
from src.pdf.settings import settings

all_headers = [x.value for x in Header]
required_headers = [x for x in all_headers if x not in OPTIONAL_HEADERS]



def test_check_labels():
    # all labels
    assert check_labels(all_headers, "no_file")

    # required only
    assert check_labels(required_headers, "no_file")

    # missing label
    with pytest.raises(Efpia_Pdf_Parsing_Exception):
        check_labels(required_headers[:-1], "no_file")

    # duplicate label
    with pytest.raises(Efpia_Pdf_Parsing_Exception):
        check_labels(required_headers + [Header.FULL_NAME], "no_file")

    # other labels
    assert check_labels(required_headers + ["test", None], "no_file")


def test_identify_headers():
    default_headers = [
        "hpc",
        "full name",
        "city",
        "country",
        "address",
        "unique country identifier",
        "donations and grants to hcos",
        "sponsorship agreements with hcos/third parties appointed by hcos to manage an event",
        "registration fees",
        "travel & accommodation",
        "fees",
        "related expenses agreed in the fee for service or consultancy contract, including travel & accommodation relevant to the contract",
        "total",
    ]
    df = pd.DataFrame([default_headers])
    identified_headers, columns_to_delete =  identify_headers(df, "no_file")
    assert identified_headers == [
        Header.ROW_TYPE,
        Header.FULL_NAME,
        Header.CITY,
        Header.COUNTRY,
        Header.ADDRESS,
        Header.UNIQUE_IDENTIFIER,
        Header.DONATION_AND_GRANT,
        Header.EVENT_SPONSORSHIP,
        Header.REGISTRATION_FEES,
        Header.TRAVEL_ACCOMMODATION,
        Header.SERVICE_AND_CONSULTANCY_FEES,
        Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
        Header.TOTAL,
    ]
    assert columns_to_delete == []

    # automatic column identification based on position
    headers = [
        "", # will be assigned to row type
        "full name",
        "city",
        "country",
        "address",
        settings.DICTIONARY["headers"]["blacklist"][-1], # assigned to None
        "unique country identifier",
        "donations and grants to hcos",
        "publi", # assigned to none
        "sponsorship agreements with hcos/third parties appointed by hcos to manage an event",
        "registration fees",
        "travel & accommodation",
        "fees",
        "related expenses agreed in the fee for service or consultancy contract, including travel & accommodation relevant to the contract",
        settings.DICTIONARY["headers"]["blacklist"][0], # will be RND
        "total",
    ]

    df = pd.DataFrame([headers])
    identified_headers, columns_to_delete =  identify_headers(df, "no_file")
    assert identified_headers == [
        Header.ROW_TYPE,
        Header.FULL_NAME,
        Header.CITY,
        Header.COUNTRY,
        Header.ADDRESS,
        Header.UNIQUE_IDENTIFIER,
        Header.DONATION_AND_GRANT,
        Header.EVENT_SPONSORSHIP,
        Header.REGISTRATION_FEES,
        Header.TRAVEL_ACCOMMODATION,
        Header.SERVICE_AND_CONSULTANCY_FEES,
        Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
        Header.RND,
        Header.TOTAL,
    ]
    assert columns_to_delete == [5,8]



