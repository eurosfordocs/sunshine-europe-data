from src.pdf.utils import remove_na, format_text, get_number, remove_cid

def test_remove_na() -> None:
    assert remove_na("n/a") == ""
    assert remove_na("n /a") == ""
    assert remove_na("test n/a test") == "test  test"
    assert remove_na("test n/a test n/a") == "test  test "

def test_format_text() -> None:
    assert format_text("test    n/a  TEST  n /  a") == "test test"
    assert format_text("test    n/a TEST n /a ", False) == "test n/a test n /a"
    assert format_text("test 8 (cid:10)\n(cid:10)") == "test 8"

def test_get_number() -> None:
    assert get_number(19) == 19
    assert get_number(3.14) == 3.14
    assert get_number(6.26e-34) == 6.26e-34

    assert get_number("19") == 19
    assert get_number("3.14") == 3.14
    assert get_number("3,14") == 3.14
    # fails, but not an issue for pdf reader
    # assert get_number("6.26e-34") == 6.26e-34

    assert get_number("not a number") == None
    assert get_number("..,...,") == None

    assert get_number("7.200 kr") == 7200
    assert get_number("123456789") == 123456789
    assert get_number("123 456 789") == 123456789
    assert get_number("123    456   789") == 123456789
    assert get_number("123, 456, 789") == 123456789
    assert get_number("123,456,789") == 123456789
    # 123.456.789 isn't valid
    assert get_number("123,456.789") == 123456.789
    assert get_number("123, 456. 78 9") == 123456.789


    assert get_number("123.456") == 123456
    assert get_number("3.500") == 3500
    assert get_number("FR. 1234,45") == 1234.45

    # , can be used as dec separator if only two digits after
    assert get_number("123,45") == 123.45
    # . can be used for 10³ separator if there is at least 3 elements after

    # non consistent
    assert get_number("123,456,78") == None
    assert get_number("123456,789") == None
    assert get_number("123 456,789") == None
    assert get_number("123,456,78") == None

    assert get_number("1.345") == 1345
    assert get_number("1.234.567") == 1234567
    assert get_number("1.234.56") == None
    assert get_number("1.234.567,89") == 1234567.89
    assert get_number("10.431,96") == 10431.96

    assert get_number("13.67%") == 13.67
    assert get_number("13.67 %") == 13.67

    assert get_number("27’373") == 27373
    assert get_number("504.810") == 504810

    assert get_number("17.500 €") == 17500

    assert get_number("0.1367") == 0.1367

    assert get_number("12346 USD 293.43") == 12346
    assert get_number("12,345.67 USD 293.43") == 12345.67
    assert get_number("12 345. 67 USD 293.43") == 12345.67

    assert get_number("value 12346") == 12346
    assert get_number("value = 12,345.67") == 12345.67
    assert get_number("for HCP:12 345. 67") == 12345.67

    assert get_number("value 12346 USD 293.43") == 12346
    assert get_number("value = 12,345.67 USD 293.43") == 12345.67
    assert get_number("for HCP:12 345. 67 USD 293.43") == 12345.67

    assert get_number("0") == 0.
    assert get_number("3%") == 3

    assert get_number("2,986,467.07") == 2986467.07

    assert get_number("According to article 12 of the code of conduct we...") == None


def test_remove_cid():
    assert remove_cid("21324") == "21324"
    assert remove_cid("15(cid:10)\n(cid:10)").strip() == "15"