#!/usr/bin/env python

import click
from src.import_schema import import_schema
from src.data_storage import download_folder, upload_folder


@click.group("Entry point for project commands")
def cli():
    pass


@cli.command(help="import schema from public repo sunshine-europe-website.")
def schema():
    import_schema()


@cli.command(help="Uploads data to Swift server. <folder> should be passed as arguement, either 'normalized_data' or 'process_data'\n"
                  "option -sf can be used to upload only a subfolder")
@click.argument('folder')
@click.option('-sf')
def upload(folder, sf):
    upload_folder(folder, sf)


@cli.command(help="Downloads data from Swift server. <folder> should be passed as arguement, either 'normalized_data' or 'process_data'\n"
                  "option -sf can be used to download only a subfolder")
@click.argument('folder')
@click.option('-sf')
def download(folder, sf):
    download_folder(folder, sf)


if __name__ == '__main__':
    cli()
