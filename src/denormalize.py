from typing import List, Callable

import pandas as pd
from pandas.core.frame import DataFrame
from tableschema.schema import Schema

from src.utils import get_all_schema


def list_columns_to_map(df: DataFrame, schema: Schema) -> List[str]:
    """
    For a specific schema, returns the columns of the dataframe which should not appear to fit the final schema
    """
    return [x for x in df.columns if x not in schema.field_names]

def fit_dataframe_to_schema(df: DataFrame, schema: Schema) -> DataFrame:
    """
    Create the correct dataframe for a specific schema.
    Uses the data of the current dataframe if the columns exists.
    """
    schema_df = pd.DataFrame(index=df.index, columns=schema.field_names)

    for col in schema.field_names:
        if col in df:
            schema_df[col] = df[col]

    return schema_df




if __name__ == '__main__':
    schemas = get_all_schema()
    for schema in schemas:
        for fk in schema.foreign_keys:
            if fk['reference']['resource'] == "address":
                print(schema.descriptor['name'])
