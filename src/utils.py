import os
import re
from datetime import date
from typing import List, Dict

import requests
from currency_converter import CurrencyConverter
from goodtables import validate
from tableschema import Schema

from src.constants.schemas import NORMALIZED_SCHEMA_DIR

EXCHANGE_RATES = {}


def to_eur(amount: float, currency: str, year=2018):
    if currency == "EUR" or currency is None:
        return amount

    er_key = f"{currency}{year}"

    if er_key not in EXCHANGE_RATES:
        d = date(int(year), 6, 1)
        EXCHANGE_RATES[er_key] = CurrencyConverter(fallback_on_missing_rate=True).convert(1, currency=currency,
                                                                                          new_currency="EUR", date=d)

    return EXCHANGE_RATES[er_key] * amount


def download(url, file_path):
    r = requests.get(url, stream=True, verify=True)
    i = 0
    with open(file_path, 'wb+') as f:
        for chunk in r.iter_content(100 ** 5):
            i += 1
            f.write(chunk)


def get_all_schema(schema_dir=NORMALIZED_SCHEMA_DIR) -> List[Schema]:
    return [Schema(schema_path) for schema_path in get_all_schema_path(schema_dir)]


def get_all_schema_path(schema_dir=NORMALIZED_SCHEMA_DIR) -> List[str]:
    for root, dirs, files in os.walk(schema_dir):
        dirs.sort()
        for file in sorted(files):
            if not file.endswith('.json'):
                continue
            schema_path = os.path.join(root, file)
            yield schema_path


def validate_csv(csv_path: str, schema_path: str, **kwargs) -> bool:
    report = validate(csv_path, schema=schema_path, row_limit=10 ** 8, fail_fast=True, **kwargs)
    if not report["valid"]:
        print(report)
    return report["valid"]


def is_running_in_docker():
    proc_cgroup_file = os.path.join('/', 'proc', 'self', 'cgroup')
    if not os.path.exists(proc_cgroup_file):
        return False

    with open(proc_cgroup_file, 'r') as f:
        for line in f.readlines():
            t = line.split('/')
            if len(t) < 2:
                continue
            if t[1] == 'docker':
                return True

    return False


def download_file(url, file_path, verify=True):
    CHUNK_SIZE = 1000
    ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
    headers = {"user-agent": ua}
    r = requests.get(url, verify=verify, stream=True, headers=headers)
    with open(file_path, 'wb') as f:
        for chunk in r.iter_content(CHUNK_SIZE):
            f.write(chunk)


def reverse_string_dict(d: Dict[str, str]) -> Dict[str, str]:
    return {v: k for k, v in d.items()}


def parse_file_name(filename: str) -> (str, str):
    split = filename.split('__')

    country, company, year = None, None, None

    if len(split) == 3:
        country = split[0]
        company = split[1]
        if re.match(r"^20\d\d$", split[2][0:4]):
            year = split[2][0:4]

    return company, year, country


# Print iterations progress
def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


if __name__ == '__main__':
    validate_csv("schemas/efpia/link.csv", "schemas/efpia/link.json")
