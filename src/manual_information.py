import csv
import logging
import os

if os.path.split(os.getcwd())[-1] == "src":
    os.chdir("..")
from src import utils
from src.constants.schema_enums.normalized.publication import Publication as NP
from src.constants.schemas import MANUAL_DIR, PROCESS_DIR


GDOCS={
    "greece": {
        "gdoc_id": "1ASMggy-sFFYZPdY3CdB_um2k1xY44Hv8XkigefKu4pI",
        "download_pdfs": True,
        "gids": {"publication": "2097865200"}
    },
    "switzerland": {
        "gdoc_id": "1J52L-ahrlTuMUp8ryJGhzFTrTaoNBe0LFb4P-XjdTME",
        "download_pdfs": True,
        "gids": {"publication": "1390119656", "entity": "1286972100"}
    },"germany": {
        "gdoc_id": "1YGQ1iyIekbcbePjsY4mouip0iAnUKWY_nXSAGGQY9Aw",
        "download_pdfs": True,
        "gids": {"publication": "2097865200", "entity": "825148314"}
    },"sweden": {
        "gdoc_id": "1stEdku2b854dm3jNzJWdGYIFqdDn2NTWfY1fj8p9Zgg",
        "download_pdfs": True,
        "gids": {"publication": "1390119656", "entity": "1286972100"}
    },"spain": {
        "gdoc_id": "11v_a_joSYUJ92690KPNILLr_OoN7t6SSYUUEF1CmYnE",
        "download_pdfs": True,
        "gids": {"publication": "277313653", "entity": "864861169"}
    },"italy": {
        "gdoc_id": "1MDtbF2VNNnEawU2xZ2am5Ia0Z8zqrrFWXjy0rfMAig4",
        "download_pdfs": True,
        "gids": {"publication": "0", "entity": "752737381"}
    },
    "sunshine_europe_manual": {
        "gdoc_id": "1GUsxT727fCHo1KC4uDxvikSN3a7MvRTdBDA6OxLIfyI",
        "download_pdfs": False,
        "gids": {"directory": "1487254134", "publication": "1321505412", "entity": "1653732258"}
    },
    "industry_deduplication": {
        "gdoc_id": "18ZfgPQTsKZk2d7kfc3mHoAbB7zvJE1LphDRIxc2V61k",
        "download_pdfs": False,
        "gids": {"entity_relation": "931131855", "entity": "422589281"}
    }
}


def download_manual_information():
    for folder in GDOCS:
        gdoc_id = GDOCS[folder]["gdoc_id"]
        for sheet_name in GDOCS[folder]["gids"]:
            gid = GDOCS[folder]["gids"][sheet_name]
            url = f"https://docs.google.com/spreadsheets/d/{gdoc_id}/export?format=csv&gid={gid}"
            logging.info(f"Downloading {sheet_name} for {folder} from {url}")
            file_name = f"{sheet_name}__{folder}.csv"
            path = os.path.join(MANUAL_DIR, file_name)
            utils.download(url, path)


def download_pdfs():
    for folder in GDOCS:
        if GDOCS[folder]["download_pdfs"]:
            publication_path = os.path.join(MANUAL_DIR, f"publication__{folder}.csv")
            with open(publication_path, 'r') as f:
                publications = csv.DictReader(f)
                i = 0
                for row in publications:
                    i += 1
                    pdf_url = row[NP.SOURCE_URL]
                    pdf_file_name = f"{row[NP.PUBLICATION_ID]}.pdf"
                    full_path = os.path.join(PROCESS_DIR, folder, "0_downloads",  pdf_file_name)

                    if pdf_file_name not in os.listdir(os.path.join(PROCESS_DIR, folder, "0_downloads")) and pdf_url != "":
                        logging.info(f"{i}:\tDownloading {row[NP.PUBLICATION_ID]}:from url: {pdf_url}")
                        try:
                            utils.download_file(pdf_url, full_path, verify=False)
                        except Exception as e:
                            logging.info(f"{i}:\tProblem trying to donwload {row[NP.PUBLICATION_ID]} from url \n{pdf_url}\n {e}")

                    else:
                        logging.info(f"{i}:\tSkipping {pdf_file_name}, it's already there")


if __name__ == '__main__':
    #download_manual_information()
    download_pdfs()

