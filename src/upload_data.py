import logging
import os

import paramiko
from dotenv import load_dotenv
from src.constants import schemas

load_dotenv()
KEY_FILE_PATH = os.getenv("KEY_FILE_PATH")
SERVER = os.getenv("SERVER")
SERVER_USER = os.getenv("SERVER_USER")
REMOTE_FOLDER = os.getenv("REMOTE_FOLDER")



class UploadDataException(Exception):
    pass


def put_dir(sftp, source, target):
    """
    Uploads the contents of the source directory to the target path. The
    target directory needs to exists. All subdirectories in source are
    created under target.
    """
    for item in os.listdir(source):
        if item != ".DS_Store":
            if os.path.isfile(os.path.join(source, item)):
                # to avoid uploading hidden files
                if item.endswith(".csv"):
                    sftp.put(os.path.join(source, item), '%s/%s' % (target, item))
                    logging.info(f"file uploaded: {item}")
            else:
                sftp.mkdir('%s/%s' % (target, item))
                logging.info(f"created directory: {'%s/%s' % (target, item)}")
                put_dir(sftp, source=os.path.join(source, item), target='%s/%s' % (target, item))


def upload_data():
    """
    uploads data to the sftp server of Sunshine-Europe-Website.
    path to a key file should be defined in .env file, this key file should be authorized on the server
    """
    if KEY_FILE_PATH is None or SERVER is None or SERVER_USER is None or REMOTE_FOLDER is None:
        raise UploadDataException("KEY_FILE_PATH, SERVER, SERVER_USER and REMOTE_FOLDER need to be defined in .env file.")

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    k = paramiko.RSAKey.from_private_key_file(KEY_FILE_PATH)
    ssh.connect(hostname=SERVER, username=SERVER_USER, pkey=k)
    sftp = ssh.open_sftp()

    rm_cmd = f"rm -rf {REMOTE_FOLDER}"
    ssh.exec_command(rm_cmd)
    stdin, stdout, stderr = ssh.exec_command(rm_cmd)  # Non-blocking call
    _ = stdout.channel.recv_exit_status() # blocking call, so we are sure that the files are deleted before recreating the folder

    logging.info(f"deleted directory: {REMOTE_FOLDER} and sub-directories")

    sftp.mkdir(REMOTE_FOLDER)
    logging.info(f"created directory: {REMOTE_FOLDER}")

    put_dir(sftp, schemas.NORMALIZED_DIR, REMOTE_FOLDER)
    sftp.close()
    ssh.close()
    logging.info(f"Normalized data uploaded successfully to sftp server {SERVER}, to folder {REMOTE_FOLDER}")


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    upload_data()
