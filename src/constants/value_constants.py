
class EntityType:
    HCO = "HCO"
    HCP = "HCP"
    INDUSTRY = "Industry"
    PO = "PO"


class DisclosureType:
    INDIVIDUAL = "individual"
    AGGREGATED = "aggregated"
    RND = "research and development"


class Type:
    CASH = "Cash or equivalent"
    IN_KIND = "In-kind items and services"


class Category:
    RND = DisclosureType.RND
    GRANT = "Grant"
    EVENT_SPONSORSHIP = "Event sponsorship"
    GIFT = "Gift"
    TRAVEL_ACCOMMODATION = "Travel and lodging"
    CONSULTING_FEE = "Consulting fee"
    SERVICE_AND_CONSULTANCY_RELATED_EXPENSES = "Service and consultancy related expenses"
    JOINT_WORKING = "Joint-Working"
    FINANCIAL_SUPPORT = "Financial support"
    OTHER_SUPPORT = "Other support"