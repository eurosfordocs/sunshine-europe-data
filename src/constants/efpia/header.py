from enum import Enum


class Header(Enum):
    ROW_TYPE = "row_type"
    FULL_NAME = "full_name"
    CITY = "city"
    COUNTRY = "country"
    ADDRESS = "address"
    UNIQUE_IDENTIFIER = "unique_identifier"
    DONATION_AND_GRANT = "donation_and_grant"
    EVENT_SPONSORSHIP = "event_sponsorship"
    REGISTRATION_FEES = "registration_fees"
    TRAVEL_ACCOMMODATION = "travel_accommodation"
    SERVICE_AND_CONSULTANCY_FEES = "service_and_consultancy_fees"
    SERVICE_AND_CONSULTANCY_RELATED_EXPENSES = "service_and_consultancy_related_expenses"
    RND = "rnd"
    TOTAL = "total"

    def __get__(self, *args):
        return self.value

OPTIONAL_HEADERS = [Header.ROW_TYPE,
                    Header.UNIQUE_IDENTIFIER,
                    Header.RND]

INDIVIDUAL_HEADERS = [Header.FULL_NAME,
                      Header.CITY,
                      Header.COUNTRY,
                      Header.ADDRESS,
                      Header.UNIQUE_IDENTIFIER]

HCO_HEADERS = [Header.DONATION_AND_GRANT]

VALUE_HEADERS = [Header.DONATION_AND_GRANT,
                 Header.EVENT_SPONSORSHIP,
                 Header.REGISTRATION_FEES,
                 Header.TRAVEL_ACCOMMODATION,
                 Header.SERVICE_AND_CONSULTANCY_FEES,
                 Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
                 Header.TOTAL]

# some times we have two extra columns for activity (corresponding to the UK model)
N_MAX_HEADERS = len(Header) + 2
N_MIN_HEADERS = len(Header) - len(OPTIONAL_HEADERS)
