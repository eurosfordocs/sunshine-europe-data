from os.path import join as pjoin

SCHEMA_DIR = "schemas"


HARMONIZED_SCHEMA_DIR = pjoin(SCHEMA_DIR, "efpia")
NORMALIZED_SCHEMA_DIR = pjoin(SCHEMA_DIR, "normalized")

NORMALIZED_DIR = pjoin("data", "normalized_data")
PROCESS_DIR = pjoin("data", "process_data")

MANUAL_DIR = pjoin(NORMALIZED_DIR, "sunshine_europe_manual")
