import json

import pandas as pd

from src.constants.efpia.header import Header, VALUE_HEADERS
from src.constants.schema_enums.efpia.link import Link as EL
from src.constants.value_constants import *
from src.pdf.process_df import RowType
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


def get_new_df():
    """
    Creates and returns a new df with the columns defined in schemas/efpia/link.json
    """
    schema = open('schemas/efpia/link.json')
    data = json.load(schema)
    columns = [data['fields'][k]['name'] for k in range(len(data['fields']))]
    return pd.DataFrame(columns=columns)


def standardize_hcp_ind(hcp_ind_df, year, company, currency):
    df = get_new_df()

    df[EL.RECIPIENT_FULL_NAME] = hcp_ind_df[Header.FULL_NAME]
    df[EL.LINK_PUBLICATION_IDENTIFIER] = None
    df[EL.PUBLICATION_YEAR] = year
    df[EL.SOURCE_ORGANIZATION_FULL_NAME] = company
    df[EL.DISCLOSURE_TYPE] = DisclosureType.INDIVIDUAL
    df[EL.RECIPIENT_TYPE] = EntityType.HCP
    #df[EL.RECIPIENT_LOCATION] =
    df[EL.RECIPIENT_ADDRESS] =  hcp_ind_df[Header.ADDRESS]
    df[EL.RECIPIENT_CITY] = hcp_ind_df[Header.CITY]
    df[EL.RECIPIENT_COUNTRY] = hcp_ind_df[Header.COUNTRY]
    # df[EL.RECIPIENT_PERSON_TITLE] =
    # df[EL.RECIPIENT_PERSON_FIRST_NAME] =
    # df[EL.RECIPIENT_PERSON_LAST_NAME] =
    # df[EL.RECIPIENT_PERSON_PROFESSION] =
    # df[EL.RECIPIENT_PERSON_SPECIALTY] =
    # df[EL.RECIPIENT_PERSON_ROLE] =
    #df[EL.RECIPIENT_PERSON_ORGANIZATION] = 
    if Header.UNIQUE_IDENTIFIER in hcp_ind_df:
        df[EL.RECIPIENT_IDENTIFIER] = hcp_ind_df[Header.UNIQUE_IDENTIFIER]
    df[EL.AMOUNT__DONATION_AND_GRANT] = hcp_ind_df[Header.DONATION_AND_GRANT]
    df[EL.AMOUNT__EVENT_SPONSORSHIP] = hcp_ind_df[Header.EVENT_SPONSORSHIP]
    df[EL.AMOUNT__REGISTRATION_FEES] = hcp_ind_df[Header.REGISTRATION_FEES]
    df[EL.AMOUNT__TRAVEL_ACCOMMODATION] = hcp_ind_df[Header.TRAVEL_ACCOMMODATION]
    df[EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES] = hcp_ind_df[Header.SERVICE_AND_CONSULTANCY_FEES]
    df[EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES] = hcp_ind_df[Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES]


    # df[EL.AMOUNT__JOINT_WORKING] = 
    # df[EL.DETAIL__JOINT_WORKING] = 
    df[EL.AMOUNT__TOTAL] = hcp_ind_df[Header.TOTAL]
    df[EL.CURRENCY] = currency

    return df


def standardize_hco_ind(hco_ind_df, year, company, currency):
    df = get_new_df()

    df[EL.RECIPIENT_FULL_NAME] = hco_ind_df[Header.FULL_NAME]
    df[EL.LINK_PUBLICATION_IDENTIFIER] = None
    df[EL.PUBLICATION_YEAR] = year
    df[EL.SOURCE_ORGANIZATION_FULL_NAME] = company
    df[EL.DISCLOSURE_TYPE] = DisclosureType.INDIVIDUAL
    df[EL.RECIPIENT_TYPE] = EntityType.HCO
    # df[EL.RECIPIENT_LOCATION] =
    df[EL.RECIPIENT_ADDRESS] =  hco_ind_df[Header.ADDRESS]
    df[EL.RECIPIENT_CITY] = hco_ind_df[Header.CITY]
    df[EL.RECIPIENT_COUNTRY] = hco_ind_df[Header.COUNTRY]
    # df[EL.RECIPIENT_PERSON_TITLE] =
    # df[EL.RECIPIENT_PERSON_FIRST_NAME] =
    # df[EL.RECIPIENT_PERSON_LAST_NAME] =
    # df[EL.RECIPIENT_PERSON_PROFESSION] =
    # df[EL.RECIPIENT_PERSON_SPECIALTY] =
    # df[EL.RECIPIENT_PERSON_ROLE] =
    # df[EL.RECIPIENT_PERSON_ORGANIZATION] =
    # df[EL.RECIPIENT_IDENTIFIER] =
    df[EL.AMOUNT__DONATION_AND_GRANT] = hco_ind_df[Header.DONATION_AND_GRANT]
    df[EL.AMOUNT__EVENT_SPONSORSHIP] = hco_ind_df[Header.EVENT_SPONSORSHIP]
    df[EL.AMOUNT__REGISTRATION_FEES] = hco_ind_df[Header.REGISTRATION_FEES]
    df[EL.AMOUNT__TRAVEL_ACCOMMODATION] = hco_ind_df[Header.TRAVEL_ACCOMMODATION]
    df[EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES] = hco_ind_df[Header.SERVICE_AND_CONSULTANCY_FEES]
    df[EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES] = hco_ind_df[Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES]
    # df[EL.AMOUNT__JOINT_WORKING] =
    # df[EL.DETAIL__JOINT_WORKING] =
    df[EL.AMOUNT__TOTAL] = hco_ind_df[Header.TOTAL]
    df[EL.CURRENCY] = currency

    return df


def standardize_agg(amount_df, recipients_df, percent_df, recipient_type, year, company, currency):
    amount_df.reset_index(drop=True, inplace=True)
    recipients_df.reset_index(drop=True, inplace=True)
    percent_df.reset_index(drop=True, inplace=True)

    df = get_new_df()
    df[EL.AMOUNT__TOTAL] = amount_df[Header.TOTAL]
    df[EL.LINK_PUBLICATION_IDENTIFIER] = None
    df[EL.PUBLICATION_YEAR] = year
    df[EL.SOURCE_ORGANIZATION_FULL_NAME] = company
    df[EL.DISCLOSURE_TYPE] = DisclosureType.AGGREGATED
    df[EL.RECIPIENT_TYPE] = str(recipient_type).upper()
    df[EL.CURRENCY] = currency

    def insert_agg_values(line, source_df):
        for header in list(VALUE_HEADERS):
            if header != Header.TOTAL:
                df[f"{line}__{header}"] = source_df[header]



    insert_agg_values("amount", amount_df)
    insert_agg_values("number_of_recipients", recipients_df)
    insert_agg_values("percentage_of_recipients", percent_df)
    return df


def standardize_rnd(rnd_df, year, company, currency):
    df = get_new_df()

    df[EL.AMOUNT__TOTAL] = rnd_df[Header.TOTAL]
    df[EL.LINK_PUBLICATION_IDENTIFIER] = None
    df[EL.PUBLICATION_YEAR] = year
    df[EL.SOURCE_ORGANIZATION_FULL_NAME] = company
    df[EL.DISCLOSURE_TYPE] = DisclosureType.RND
    df[EL.RECIPIENT_TYPE] = EntityType.HCO
    df[EL.CURRENCY] = currency

    return df


def standardize(df, year, company, currency):
    """
    generates a df following the format described in schemas/efpia/link.json from a df following the efpia pdf format.
    :param df: source df
    :param year:
    :param company:
    :param currency:
    :return: a different df
    """
    df_list = []

    df_list.append(standardize_hcp_ind(hcp_ind_df=df[df[Header.ROW_TYPE] == RowType.HCP_IND],
                                       year=year, company=company, currency=currency))

    df_list.append(standardize_agg(amount_df=df[df[Header.ROW_TYPE] == RowType.HCP_AGG_AMOUNT],
                                   percent_df=df[df[Header.ROW_TYPE] == RowType.HCP_AGG_PERCENT ],
                                   recipients_df=df[df[Header.ROW_TYPE] == RowType.HCP_AGG_RECIPIENTS],
                                   recipient_type= EntityType.HCP,
                                   year=year, company=company, currency=currency))

    df_list.append(standardize_hco_ind(hco_ind_df=df[df[Header.ROW_TYPE] == RowType.HCO_IND],
                                       year=year, company=company, currency=currency))

    df_list.append(standardize_agg(amount_df=df[df[Header.ROW_TYPE] == RowType.HCO_AGG_AMOUNT],
                                   percent_df=df[df[Header.ROW_TYPE] == RowType.HCO_AGG_PERCENT ],
                                   recipients_df=df[df[Header.ROW_TYPE] == RowType.HCO_AGG_RECIPIENTS],
                                   recipient_type= EntityType.HCO,
                                   year=year, company=company, currency=currency))

    df_list.append(standardize_rnd(rnd_df=df[df[Header.ROW_TYPE] == RowType.RND],
                                   year=year, company=company, currency=currency))

    if len(df_list) > 0:
        # create a unique ID using index
        standard_df = pd.concat(df_list, ignore_index=True)
        standard_df["link_publication_identifier"] = standard_df.index
        if len(standard_df.columns) != 40:
            raise Efpia_Pdf_Parsing_Exception(f"Expected 40 columns in resulting DF, found {len(standard_df.columns)}: {standard_df.columns}")
        return standard_df




