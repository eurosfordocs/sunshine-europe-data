# Parsing EFPIA PDFs

## Concept
Parsing an EFPIA PDFS goes in steps
1) Read the file: 
    * Open the file
    * Identify the columns of the tables
    * Remove unnecessary columns on each of the tables. 
    * If necessary, merge all the tables in one dataframe
    
    This is done in the class FileReader which has 2 subclasses: `CamelotPdfReader` or `XlsReader`. A lot of common logic is either in the parent class `FileReader`, and the logic to identify the headers is in static methods in `identify_headers.py`
    
2) Process the resulting DF. 
    * Remove all headers row and useless rows (no values, etc)
    * Determine the row type (HCP individual, HCP aggregated, etc.)
    * Check if the values make sense according to the row type, discard row otherwise

3) Standardize the processed df
    * Adapt to the schema defined in `Schemas/EFPIA`
    
The result of this process should be data in the `harmonized` folder, ready to be normalized. 

## Usage
### A) Prepare the list of PDFs to donwload
Inspire yourself from: https://docs.google.com/spreadsheets/d/1YGQ1iyIekbcbePjsY4mouip0iAnUKWY_nXSAGGQY9Aw/edit#gid=2097865200
the spreadsheet should have 3 tabs `publication`, `entity`, and `directory`

### B) Downlads the PDFs
Add your document id and the sheet IDs in the `GDOCS` global variable in [manual_information.py](manual_information.py), then run it. This should download all the PDFs listed in your doc, check the data folder. 

### C) Manual operations on the PDFs
* Remove any password on them using SmallPDF, you can batch execute this. 
* Bulk export them with Adobe Acrobat Pro. Rename them `<pdf file name>__adobe.xlsx`.
* Bulk export them with Adobe SmallPDF. Rename them `<pdf file name>__smallpdf.xlsx`.

### D) Execute the pdf parser on the folder
Follow the example of the notebook `germany_all_steps.py`
Basically call the function `parse_efpia_pdf_folder` of [efpia_pdf_parser.py](efpia_pdf_parser.py)

### E) if needed, "zoom" on on one file
See function `zoom` in `sweden_all_steps.py`

