import logging
import math
import re
from enum import Enum
from typing import Optional
import pandas as pd

from src.constants.efpia import header as headerConst
from src.pdf import utils
from src.pdf.settings import settings
from src.pdf.utils import Efpia_Pdf_Line_Parsing_Exception, Efpia_Pdf_Parsing_Exception, No_Value_In_Line_Exception


class RowType (Enum):
    HCP_IND = 1
    HCP_AGG_AMOUNT = 2
    HCP_AGG_RECIPIENTS = 3
    HCP_AGG_PERCENT = 4
    HCO_IND = 5
    HCO_AGG_AMOUNT = 6
    HCO_AGG_RECIPIENTS = 7
    HCO_AGG_PERCENT = 8
    RND = 9

    def __get__(self, *args):
        return self.value


IND_ROW_TYPES = [RowType.HCP_IND, RowType.HCO_IND]
HCP_ROW_TYPES = [RowType.HCP_IND, RowType.HCP_AGG_AMOUNT, RowType.HCP_AGG_RECIPIENTS, RowType.HCP_AGG_PERCENT]
HCO_ROW_TYPES = [RowType.HCO_IND, RowType.HCO_AGG_AMOUNT, RowType.HCO_AGG_RECIPIENTS, RowType.HCO_AGG_PERCENT, RowType.RND]



def should_keep_row(row, line):
    """
    Returns False if
     * the row is a header or
     * the row is empty or
     * the row has no value in the 6 columns on the right
     * the row has nothing in the "full name" column
    """
    right_empty_cols = 0
    for raw_text in reversed(row):
        text = utils.format_text(raw_text, should_remove_na=False)
        number_value = utils.get_number(text)
        if number_value is not None:
            break
        right_empty_cols += 1


    # we take out any line that has no value column filled: nothing in the 6 columns from the right.
    # or any row where only 2 columns on the left are filled (for rnd table headers)
    if right_empty_cols > 6 or right_empty_cols >= len(row) - 2:
        return False

    empty_row = True
    non_header_text = 0
    for raw_text in row:
        text = utils.format_text(raw_text, should_remove_na=True)
        if text != '':
            empty_row = False
            if settings.get_exact_match(settings.Sections.HEADERS, text) is None:
                non_header_text += 1

    if headerConst.Header.FULL_NAME in row and utils.format_text(row[headerConst.Header.FULL_NAME]) == "":
        return False

    return not empty_row and non_header_text > 1


def is_row_agg(row, line):
    """
    Returns true if row is aggregated. Aggregated row have only 1 value in the 5 individual columns
    """
    non_empty_cells = 0
    for header in headerConst.INDIVIDUAL_HEADERS:
        if header in row:
            value = utils.format_text(row[header])
            if value != "":
                non_empty_cells += 1
    return non_empty_cells == 1


def remove_useless_rows(df, filename):
    """
    removes emtpy lines or headers, resets the index
    """
    lines_removed = 0
    removed_although_fn = 0
    for line, row in df.iterrows():
        if not should_keep_row(row, line):
            df.drop(line, inplace=True)
            lines_removed += 1
            if row[headerConst.Header.FULL_NAME] != "":
                removed_although_fn += 1
                # logging.debug(f"removing line {line} lines, with full name {row[headerConst.Header.FULL_NAME]}")
    df.reset_index(drop=True, inplace=True)
    logging.debug(f"{filename}: removed {lines_removed} lines, including {removed_although_fn} with non emtpy Full Name. {df.shape[0]} lines remain (headers or lines without value)")


def clean_address(df):
    for i, row in df.iterrows():
        if row[headerConst.Header.CITY] is not None:
            postal_re = re.search(r'(^\d+)[a-zA-Z]', str(row[headerConst.Header.CITY]))
            if postal_re and postal_re.group(1):
                postal_code = postal_re.group(1)
                df.at[i, headerConst.Header.CITY] = row[headerConst.Header.CITY][len(postal_code) + 1:]
                df.at[i, headerConst.Header.ADDRESS] = f"{row[headerConst.Header.ADDRESS]} {postal_code}"
    return df


def process_rows(df, filename, rnd_df):
    """
    determines the row type and adds it it the first column (row_type)
    performs a sanity check of the row values and removes the line if checks fails
    """
    rnd_row = None
    if rnd_df is not None and rnd_df.shape[0] > 0:
        for line, row in rnd_df.iterrows():
            rnd_row = process_rnd_row(row, filename)
            if rnd_row is not None:
                logging.debug(f"{filename}: sucessfully processed rnd row with value {rnd_row[headerConst.Header.TOTAL]}")
                break

    current_row_type = RowType.HCP_IND # first line is individual HCP

    rows_in_errors = 0
    blocks_first_lines = settings.get_blocks_first_lines(filename)
    for line, row in df.iterrows():
        full_name = row[headerConst.Header.FULL_NAME]

        if blocks_first_lines is not None and full_name in blocks_first_lines:
            current_row_type = blocks_first_lines[full_name]
            logging.info(f"switching to RT {RowType(current_row_type).name} based on manual value! at line {line}, with name {full_name}")

        elif full_name in [x.name for x in list(RowType)]:
            current_row_type = RowType[full_name].value
            logging.info(f"switching to RT {RowType(current_row_type).name} based on full name column: {full_name}")

        # for individual rows, we stay on this row type until we find an aggregated row.
        # For aggregated rows, we always change type as there is only 1 line per row type.
        elif current_row_type in IND_ROW_TYPES:
            if is_row_agg(row, line):
                current_row_type += 1
                logging.debug(f"\t\tswitching to RT {RowType(current_row_type).name} at line {line}, with name {full_name}")
        else:
            current_row_type += 1
            if current_row_type not in [rt.value for rt in RowType]:
                raise Efpia_Pdf_Parsing_Exception(f"impossible to identify row types, line {line -1} ({full_name}) is identified as RND although it's not the last line")

        df.at[line, headerConst.Header.ROW_TYPE] = current_row_type

        if current_row_type == RowType.RND:
            rnd_row = process_rnd_row(row, filename)
            df.drop(line, inplace=True)
        else:
            try:
                row_sanity_check(filename, line, row, current_row_type)

            except No_Value_In_Line_Exception:
                pass
            except Efpia_Pdf_Line_Parsing_Exception as e:

                rows_in_errors += 1
                logging.debug(str(e) + f" Line will be removed. ({rows_in_errors})")

                if rows_in_errors > 40:
                    raise Efpia_Pdf_Parsing_Exception("more than 40 lines with errors, likely the blocks were not identified correctly")
                df.drop(line, inplace=True)

    if rnd_row is not None:
        df = df.append(rnd_row, ignore_index=True)

    df = clean_address(df)
    return df


def process_rnd_row(row, filename) -> Optional[dict]:
    """
    RND rows can have less columns, and the value can be either be on the last column or the one before last.
    This function manages theses cases and returns a normalised row to be added to the DF.
    """

    values_rtl = []
    for raw_text in reversed(row):
        number = utils.get_number(raw_text)
        if number is not None:
            values_rtl.append(number)

    if len(values_rtl) >= 3:
        logging.debug(f"{filename}: more than two values on this RND row, skipping")
        return None
    elif len(values_rtl) == 0:
        logging.debug(f"{filename}: no value on this RND row, skipping")
        return None
    else:
        rnd_amount = values_rtl[len(values_rtl) -1]
        rnd_row = {}
        for h in list(headerConst.Header):
            rnd_row[h.value] = None
        rnd_row[headerConst.Header.ROW_TYPE] = RowType.RND
        rnd_row[headerConst.Header.TOTAL] = rnd_amount
        rnd_row[headerConst.Header.FULL_NAME] = "rnd"
        return rnd_row


def row_sanity_check(filename, line, row, row_type):
    """
    checks different things:
        * all rows should have the column "full name" filled
        * all rows should have at least one of the value fields filled
        * HCOs fields should be empty or N/A for HPC lines
        * Ind fields should have all ind fields filled
    raises an Efpia_Pdf_Line_Parsing_Exception if any problem encontered
    """
    # in any case, the full_name column should be filled
    if utils.format_text(row[headerConst.Header.FULL_NAME]) == '':
        raise Efpia_Pdf_Line_Parsing_Exception(f"{filename}, line {line}: full_name column is empty.")

    full_name = utils.format_text(row[headerConst.Header.FULL_NAME])

    # All individual fields but address (name, country, city) should be filled.
    #if row_type in IND_ROW_TYPES:
    #    for h in headerConst.INDIVIDUAL_HEADERS:
    #        if  h not in headerConst.OPTIONAL_HEADERS and h != headerConst.Header.ADDRESS and utils.format_text(row[h]) == '':
    #            raise Efpia_Pdf_Line_Parsing_Exception(f"{filename}, line {line} ({full_name}) was detected as {RowType(row_type).name} but the column {h} is empty.")

    # If row is HCP, all hco field should be empty
    if row_type in HCP_ROW_TYPES:
        for h in headerConst.HCO_HEADERS:
            if row[h] is not None and not pd.isnull(row[h]) and utils.format_text(row[h]) != '':
                raise Efpia_Pdf_Line_Parsing_Exception(f"{filename}, line {line} ({full_name}) was detected as {RowType(row_type).name} but the hco field {h} is not empty.")

    # In any case, at least one value field should be filled
    no_value = True
    for h in headerConst.VALUE_HEADERS:
        if utils.get_number(row[h]) is not None and utils.get_number(row[h]) > 0:
            no_value = False
            break
    if no_value:
        raise No_Value_In_Line_Exception(f"{filename}, line {line} ({full_name}): all value columns are empty. Removing line")


def df_sanity_check(df, filename):
    """
    cheecks that all mandatory row types are present in the DF, logs it otherwise
    """
    unique_row_types = list(dict.fromkeys(df[headerConst.Header.ROW_TYPE]))


    for rt in list(RowType):
        if rt.value not in unique_row_types and rt.value not in IND_ROW_TYPES:
            logging.debug(f"\t\t{filename}: after processing rows, missing row type {rt.name}")
            if rt.value in IND_ROW_TYPES:
                raise Efpia_Pdf_Parsing_Exception(f"no rows identified as {rt.name}.")

    # AGG Rows: if there is a "number" or "percent" row, there has to be the AGG Amount row
    if (RowType.HCP_AGG_PERCENT in unique_row_types or RowType.HCP_AGG_RECIPIENTS in unique_row_types) and not RowType.HCP_AGG_AMOUNT in unique_row_types:
        raise Efpia_Pdf_Parsing_Exception("no HCP_AGG_AMOUNT row although there is a PERCENT or NUMBER row.")

    if (RowType.HCO_AGG_PERCENT in unique_row_types or RowType.HCO_AGG_RECIPIENTS in unique_row_types) and not RowType.HCO_AGG_AMOUNT in unique_row_types:
        raise Efpia_Pdf_Parsing_Exception("no HCO_AGG_AMOUNT row although there is a PERCENT or NUMBER row.")

    agg_recipients_rows = df[df[headerConst.Header.ROW_TYPE].isin([RowType.HCO_AGG_RECIPIENTS, RowType.HCP_AGG_RECIPIENTS])]
    for index, row in agg_recipients_rows.iterrows():
        for h in headerConst.VALUE_HEADERS:
            value = utils.get_number(row[h])
            if value is not None and value < 1:
                raise Efpia_Pdf_Parsing_Exception(f"{RowType(row[headerConst.Header.ROW_TYPE])} is below 1 ({value}), likely row types are incorrect. Dismissing file.")

    # checks that there is one line at least
    if df.shape[0] == 0:
        raise Efpia_Pdf_Parsing_Exception("no line left in DF after removing useless ones")


def process_df(df, filename, rnd_df):
    """
    analyses each row of the DF
        if there is no "row type" columns, adds it as the first column
        removes useless rows (rows without values or headers)
        fills the first column "row_type"
        perform a sanity check, removes row if check fails.
    :return:
    """
    df.fillna(value="", inplace=True)

    if headerConst.Header.ROW_TYPE not in df.columns:
        df.insert(loc=0, column=headerConst.Header.ROW_TYPE, value="")
        df.reset_index(drop=True, inplace=True)

    remove_useless_rows(df, filename)
    if rnd_df is not None:
        remove_useless_rows(rnd_df, filename)
        logging.debug(f"{filename}: after removing headers rows, rnd df has {rnd_df.shape[0]} rows")

    # parsing value columns to float. For other columns, if string: replacing \n, \t, \r by spaces, removing N/A and trailing spaces.
    for h in headerConst.Header:
        if h.value in headerConst.VALUE_HEADERS:
            df[h.value] = df[h.value].astype(str)
            df[h.value] = df[h.value].map(lambda x: utils.get_number_null_if_zero(x))
        elif h.value in df:
            df[h.value] = df[h.value].map(lambda x: utils.clean_str_value(x))

    df = process_rows(df, filename, rnd_df)

    df_sanity_check(df, filename)

    # removing the RnD columns (now useless, we made sure the value is in the Total column)
    if headerConst.Header.RND in df.columns:
        df.drop(columns=[headerConst.Header.RND])

    return df


