import difflib
import json
import logging
import os
from enum import Enum
from typing import Union


from dotenv import load_dotenv

load_dotenv()

PDF_SETTINGS_DIR_PATH = os.getenv("PDF_SETTINGS_DIR", "data/process_data/pdf_settings")
dictionary_file_path = os.path.join(PDF_SETTINGS_DIR_PATH, "efpia_pdf_dictionary.json")
status_file_path = os.path.join(PDF_SETTINGS_DIR_PATH, "files_status.json")

# variable corresponding to the file efpi_pdf_dictionary.json
DICTIONARY = None

# variable corresponding to the file file_status.json
FILES_STATUS = None

DEFAULT_LINE_SCALE = 120
DEFAULT_LINE_SCALE_STEP = 5


# The dictionary is divided in Sections. For now there is only 1, but it may be useful in the future.
class Sections(Enum):
    HEADERS = "headers"


class File_status():
    BLACKLISTS = "blacklists"
    FOUND_LINE_SCALE = "found_line_scale"


class BlacklistReason():
    USER_CHOICE = "user_choice"
    INCONSTANT_COLUMNS_NUMBER= "many_tables_with_different_columns_numbers"
    NO_TABLE_IN_PDF = "no table in pdf"


def load_dictionary():
    global DICTIONARY
    with open(dictionary_file_path, "r") as f:
        DICTIONARY = json.load(f)


def add_to_dictionary(section: Sections, key, value):
    DICTIONARY[section.value][key].append(value)
    save_dictionary()


def save_dictionary():
    with open(dictionary_file_path, "w") as f:
        json.dump(DICTIONARY, f, indent=4, ensure_ascii=False)


def get_exact_match(section: Sections, value) -> Union[str, None]:
    for key in DICTIONARY[section.value]:
        for synomym in DICTIONARY[section.value][key]:
            if value == synomym:
                if key == "blacklist":
                    return None
                return key
    return None


def get_fuzzy_match(section: Sections, value):
    best_score = 0
    likeliest_key = None

    for key in DICTIONARY[section.value]:
        for synonym in DICTIONARY[section.value][key]:
            score = difflib.SequenceMatcher(None, str(value), str(synonym)).ratio()
            if score > best_score:
                best_score = score
                likeliest_key = key
                logging.debug(
                    f"likeliest {section.name} for {str(value)} is {likeliest_key}, with a difflib score of {best_score}"
                )

    if best_score <= 0.1 or likeliest_key == "blacklist":
        likeliest_key = None
    return likeliest_key


def is_blacklisted(section: Sections, value):
    return value in DICTIONARY[section.value]["blacklist"]


def load_files_status():
    global FILES_STATUS
    with open(status_file_path, "r") as f:
        FILES_STATUS = json.load(f)


def set_found_line_scale(filename):
    if filename not in FILES_STATUS[File_status.FOUND_LINE_SCALE]:
        FILES_STATUS[File_status.FOUND_LINE_SCALE].append(filename)
        save_files_status()


def add_file_to_blacklist(filename, reader_id, reason=""):
    FILES_STATUS[File_status.BLACKLISTS][reader_id][filename] = reason
    save_files_status()


def is_file_blacklisted(filename, reader_id):
    return filename in FILES_STATUS[File_status.BLACKLISTS][reader_id]


def save_files_status():
    with open(status_file_path, "w") as f:
        json.dump(FILES_STATUS, f, indent=4, ensure_ascii=False)


def get_line_scale(filename):
    if filename in FILES_STATUS["line_scale"]:
        return FILES_STATUS["line_scale"][filename]
    return DEFAULT_LINE_SCALE


def get_blocks_first_lines(filename):
    if filename in FILES_STATUS["blocks_first_lines"]:
        return FILES_STATUS["blocks_first_lines"][filename]
    return None


def is_file_found_scale(filename):
    return filename in FILES_STATUS[File_status.FOUND_LINE_SCALE]


def increase_line_scale(filename):
    FILES_STATUS["line_scale"][filename] = (
        get_line_scale(filename) + DEFAULT_LINE_SCALE_STEP
    )
    save_files_status()


def decrease_line_scale(filename):
    FILES_STATUS["line_scale"][filename] = (
        get_line_scale(filename) - DEFAULT_LINE_SCALE_STEP
    )
    save_files_status()


def init():
    load_dictionary()
    load_files_status()

