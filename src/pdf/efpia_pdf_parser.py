import argparse
import csv
import logging
import os
from pathlib import Path
from src.manual_information import MANUAL_DIR

from src.constants.schema_enums.normalized.publication import Publication as NP
from src.pdf import utils
from src.pdf.readers.camelot_pdf_reader import CamelotPdfReader
from src.pdf.readers.xls_reader import XlsReader
from src.pdf.standardize import standardize
from src.pdf.utils import Efpia_Pdf_Parsing_Exception

READERS = [XlsReader("finereader"),
           XlsReader("adobe"),
           XlsReader("smallpdf")
          #, CamelotPdfReader()
           ]

DEFAULT_CURRENCY = "EUR"


def add_result(results, reader_id, result, file):
    if reader_id not in results:
        results[reader_id] = {}

    if result in results[reader_id]:
        results[reader_id][result]["count"] += 1
        if result not in ["parsed" , "already parsed"]:
            results[reader_id][result]["files"].append(file)
    else:
        results[reader_id][result]= {
            "count":1,
            "files":[file]
        }

    logging.info(f"\t\t{reader_id}: {result}")


def parse_efpia_pdf(pdf_file_name:str, dl_dir:str, harmonized_dir:str, currency=None, results=None):
    """
    Parses the given `pdf_file_name` from `dl_dir`. Data will be added in the `harmonized_dir`.
    Checks in the `normalized_dir` for an existing publication to read the currency from.
    If no corresponding publication, uses the currency param, or EUR as default.
    If a param `results` is given, adds the result of each FileReader to the results. If not, propagate the exceptions
    """

    country = dl_dir.split(os.path.sep)[2]

    for reader in READERS:
        try:
            df = reader.read(dl_dir, harmonized_dir, pdf_file_name)

            company, year = utils.parse_file_name(pdf_file_name.lower())
            publication_id = pdf_file_name[:-4]
            currency = get_publication_currency(country, publication_id, currency)

            standard_df = standardize(df, year, company, currency=currency)

            csv_file = os.path.join(harmonized_dir, f"{pdf_file_name.lower()[0:-4]}.csv")
            standard_df.to_csv(csv_file, index=False)

            if results is not None:
                add_result(results, reader.reader_id, "parsed", pdf_file_name)
            else:
                logging.info(f"File {pdf_file_name} successfully parsed with Reader {reader.reader_id}")

        except Efpia_Pdf_Parsing_Exception as e:
            if results is not None:
                add_result(results, reader.reader_id, str(e), pdf_file_name)
            else:
                logging.info(f"{reader.reader_id}: {str(e)}")


def parse_efpia_pdf_folder(dl_dir, harmonized_dir, currency=None):
    """
    :param currency: if no corresponding existing publication in the manual folder, will use this param as the currency.

    If not possible to parse the pdf, it will be added to a blacklist.
    File blacklist, pdf parsing options and confirmed header interpretations are stored in data/process_data/pdf_settings
    """
    results = {}

    files = os.listdir(dl_dir)
    files.sort()
    pdf_count = sum(1 if f[-3:] == "pdf" else 0 for f in files)
    logging.info(f"{pdf_count} PDFs in {dl_dir}")
    i = 0
    for file in files:
        if file[-3:] == "pdf":
            i += 1
            logging.info(f"File {i} / {pdf_count}: {file}")
            parse_efpia_pdf(file, dl_dir, harmonized_dir, currency=currency, results=results)
    msg = ""
    for reader in results:
        msg += f"\n===========  {reader.upper()}  ==========="
        for k, v in results[reader].items():
            msg += f"\n{k}: {v}"
    logging.info(msg)


def get_publication_currency(country, publication_id, currency_param):
    logging.info(country)
    file_name = f"publication__{country}.csv"
    if file_name in os.listdir(MANUAL_DIR):
        file_path = os.path.join(MANUAL_DIR, file_name)
        with open(file_path, "r") as f:
            publications = csv.DictReader(f)
            for p in publications:
                if p[NP.PUBLICATION_ID] == publication_id:
                    logging.info(f"publication {publication_id} found in {file_path}, using the currency. ")
                    return p[NP.CURRENCY]

            logging.info(f"{publication_id} not found in {file_path}, using EUR.")

    if currency_param is not None:
        logging.info(f"No existing publication, using currency passed as param")
        return currency_param

    return DEFAULT_CURRENCY





if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Parse an EFPIA PDF')
    parser.add_argument('filepath', type=str, nargs=1, help='path of the pdf file')
    parser.add_argument("--o", dest="output", help="output folder")
    parser.add_argument("--currency", dest="currency", default="EUR", help="currency used in the report")
    args = parser.parse_args()

    p = Path(args.filepath[0])
    assert p.is_file(), Exception(f"{p} is not a file")
    in_dir = p.parent
    filename = p.name
    
    out_dir = args.output
    if out_dir is None:
        out_dir = in_dir
    
    out_dir = Path(out_dir)
    assert out_dir.is_dir(), Exception(f"Wrong output directory {out_dir}")

    parse_efpia_pdf_folder(in_dir, out_dir, filename, args.currency)