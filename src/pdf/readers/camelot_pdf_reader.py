import logging
import os
import re
from typing import Tuple, List, Optional

import PyPDF2
import camelot
import pandas as pd

from src.constants.efpia.header import N_MAX_HEADERS, N_MIN_HEADERS
from src.pdf import process_df, identify_headers
from src.pdf.readers.file_reader import FileReader
from src.pdf.settings import settings
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


class CamelotPdfReader(FileReader):

    def __init__(self):
        self.reader_id = "camelot"

    def _read(self, dl_dir, pdf_filename):
        self.pdf_filename = pdf_filename
        self.dl_dir = dl_dir

        table_list, rnd_df = self._read_pdf()

        headers, columns_to_delete = identify_headers.identify_headers(table_list[0], self.pdf_filename, self.reader_id)

        df = self._merge_dfs(df_list=table_list, headers=headers, columns_to_delete=columns_to_delete)

        df = process_df.process_df(df, self.pdf_filename, rnd_df)

        return df

    def _read_pdf(self) -> Tuple[List[camelot.core.Table], Optional[pd.core.frame.DataFrame]]:
        """
        reads the filename of the dir. Details:
            tries to read file with split_text=True, tries without if there is an Exception from Camelot
            uses the scaleline from the setting file for the file or the default value
            if more than N_MAX_HEADERS (14) columns found, or less than N_MIN_HEADERS (12), the scale line is adjusted (see settings.py)
            Adds the file to blacklist if not solution can be found so that the resulting DFs have 13 or N_MAX_HEADERS (14) columns

        :return: a table list, one per pdf page.
        """

        line_scale = self._get_line_scale()

        table_list = self._get_tables(line_scale, pages="all")

        rnd_df = None

        min_cols = min(table_list[x].df.shape[1] for x in range(len(table_list)))
        max_cols = max(table_list[x].df.shape[1] for x in range(len(table_list)))

        if min_cols < N_MIN_HEADERS or max_cols > N_MAX_HEADERS:

            # check if we are not in the case where only the last table has less column
            problem_not_on_last_page = False

            for x in range(len(table_list)):
                col_number = table_list[x].df.shape[1]
                if (col_number < N_MIN_HEADERS or col_number > N_MAX_HEADERS) and x < len(table_list) - 1:  # not on last page
                    problem_not_on_last_page = True
                logging.debug(f"file {self.pdf_filename}: page {x + 1}: {table_list[x].df.shape[1]} columns")

            if problem_not_on_last_page:
                settings.add_file_to_blacklist(self.pdf_filename, self.reader_id, settings.BlacklistReason.INCONSTANT_COLUMNS_NUMBER)
                raise Efpia_Pdf_Parsing_Exception(f"file {self.pdf_filename}: {len(table_list)} pages, "
                                                  f"between {min_cols} and {max_cols} columns per page in the parsed pdf, "
                                                  f"adding file to blacklist")

            rnd_df = table_list[-1].df
            table_list = table_list[:-1]
        else:
            logging.debug(f"file {self.pdf_filename}: {len(table_list)} pages, "
                          f"between {min_cols} and {max_cols} columns per page in the parsed pdf.")

        settings.set_found_line_scale(self.pdf_filename)
        dfs = [x.df for x in table_list]
        return dfs, rnd_df

    def _merge_dfs(self, df_list: List[pd.DataFrame], headers: List[str],
                  columns_to_delete: List[str]) -> pd.core.frame.DataFrame:
        """
        takes the table list and merge it in a single DF.
        Adds the first column "block type" if necessary
        Sets the headers the params headers
        :return: the merged DF
        """
        page = 0
        for df in df_list:
            page += 1
            # if we miss one column, we add one empty on the left, it's the hcp/hco block going over more than a page.
            if len(df.columns) == len(headers) + len(headers) + len(columns_to_delete) - 1:
                new_df = df.reindex(columns=["new_col0"] + df.columns.tolist())
                new_df.reset_index(drop=True, inplace=True)
                new_df.drop(new_df.columns[columns_to_delete], axis=1, inplace=True)
                new_df.columns = headers
            elif len(df.columns) == len(headers) + len(columns_to_delete):
                df.drop(df.columns[columns_to_delete], axis=1, inplace=True)
                df.columns = headers
            else:
                settings.add_file_to_blacklist(self.pdf_filename, self.reader_id, settings.BlacklistReason.INCONSTANT_COLUMNS_NUMBER)
                msg = f"unexpected column number on page {page} of file {self.pdf_filename}, expected " \
                      f"{len(headers) + len(columns_to_delete)} columns (or " \
                      f"{len(headers) + len(columns_to_delete) - 1}) but there are {len(df.columns)}. File added to blacklist"
                raise Efpia_Pdf_Parsing_Exception(msg)

        merged_df = pd.concat(df_list)
        merged_df.reset_index(drop=True, inplace=True)
        return merged_df

    def _get_line_scale(self):
        if settings.is_file_found_scale(self.pdf_filename):
            return settings.get_line_scale(self.pdf_filename)

        # the file is new
        file_path = os.path.join(self.dl_dir, self.pdf_filename)
        line_scale = settings.get_line_scale(self.pdf_filename)

        table_list = self._get_tables(line_scale, pages="1")

        if len(table_list) == 0:
            table_list = self._get_tables(line_scale, pages="all")

        if len(table_list) == 0:
            settings.add_file_to_blacklist(self.pdf_filename, self.reader_id, settings.BlacklistReason.NO_TABLE_IN_PDF)
            raise Efpia_Pdf_Parsing_Exception(settings.BlacklistReason.NO_TABLE_IN_PDF)

        first_table_cols = table_list[0].df.shape[1]

        if first_table_cols < N_MIN_HEADERS:
            settings.increase_line_scale(self.pdf_filename)
            logging.info(f"file {self.pdf_filename}:  less than {N_MIN_HEADERS} columns on first table ({first_table_cols}), "
                         f"increased line_scale to {settings.get_line_scale(self.pdf_filename)}")
            return self._get_line_scale()
        elif first_table_cols > N_MAX_HEADERS:
            settings.decrease_line_scale(self.pdf_filename)
            logging.info(f"file {self.pdf_filename}:  more than {N_MAX_HEADERS} columns on first table ({first_table_cols}), "
                         f"decreased line_scale to {settings.get_line_scale(self.pdf_filename)}")
            return self._get_line_scale()

        return line_scale

    def _get_tables(self, line_scale: int, pages: str):
        file_path = os.path.join(self.dl_dir, self.pdf_filename)
        try:
            table_list = camelot.read_pdf(filepath=file_path, pages=pages, line_scale=line_scale, split_text=True)
        except IndexError:
            logging.error(f"file {self.pdf_filename}: IndexError from Camelot. Trying without 'split_text' option")
            try:
                table_list = camelot.read_pdf(filepath=file_path, pages=pages, line_scale=line_scale)
            except IndexError:
                raise Efpia_Pdf_Parsing_Exception(f"file {self.pdf_filename}: IndexError from Camelot. Giving up.")

        except (NotImplementedError, PyPDF2.utils.PdfReadError):
            raise Efpia_Pdf_Parsing_Exception(
                f"file {self.pdf_filename}: This file seems to be password-protected or corrupted, print it again, save as pdf and try again")

        table_list = self.non_empty_tables(table_list)

        if table_list is None:
            raise Efpia_Pdf_Parsing_Exception(f"unable to read file {self.pdf_filename}")

        return table_list

    @staticmethod
    def non_empty_tables(table_list):
        new_table_list = []
        for t in table_list:
            if re.search(r"[a-z0-9]", str(t.data).lower()):
                new_table_list.append(t)

        return new_table_list
