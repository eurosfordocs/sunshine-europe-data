import os

from src.pdf.settings import settings
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


class FileReader:

    def _read(self, dl_dir, pdf_filename):
        pass

    def read(self, dl_dir, harmonized_dir, pdf_filename):

        harmonized_filename = f"{pdf_filename.lower()[:-3]}csv"

        if harmonized_filename in os.listdir(harmonized_dir):
            raise Efpia_Pdf_Parsing_Exception("already parsed")

        if settings.is_file_blacklisted(pdf_filename, self.reader_id):
            raise Efpia_Pdf_Parsing_Exception("blacklisted")

        return self._read(dl_dir, pdf_filename)