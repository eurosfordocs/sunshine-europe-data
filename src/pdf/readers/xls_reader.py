import logging
import os
from typing import List

import pandas as pd

from src.constants.efpia.header import N_MAX_HEADERS, N_MIN_HEADERS
from src.pdf import process_df, identify_headers
from src.pdf.readers.file_reader import FileReader
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


class XlsReader(FileReader):

    def __init__(self, suffix):
        self.suffix = suffix
        self.reader_id = f"xls_{suffix}"

    def _xls_file_name(self, pdf_filename):
        return f"{pdf_filename[0:-4]}__{self.suffix}.xlsx"

    def _read(self, dl_dir, pdf_filename):

        xls_file_name = self._xls_file_name(pdf_filename)

        if xls_file_name not in os.listdir(dl_dir):
            raise Efpia_Pdf_Parsing_Exception(f"no xls file with suffix {self.suffix}")

        file_path = os.path.join(dl_dir, xls_file_name)
        df_dict = pd.read_excel(file_path, sheet_name=None, header=None, engine="openpyxl")

        dfs = [df_dict[x] for x in df_dict]

        harmonized_dfs = self._harmonize_sheets(dfs, pdf_filename)

        if harmonized_dfs is not None:
            headers, columns_to_delete = identify_headers.identify_headers(harmonized_dfs[0], pdf_filename, reader_id=self.reader_id)
            df = self._merge_dfs(harmonized_dfs, headers, columns_to_delete)

            df.fillna(value="", inplace=True)
            df = process_df.process_df(df, pdf_filename, None)
            return df
        else:
            min_cols = min(x.shape[1] for x in dfs)
            max_cols = max(x.shape[1] for x in dfs)
            if min_cols < N_MIN_HEADERS:
                raise Efpia_Pdf_Parsing_Exception(f"not enough columns")
            if max_cols > N_MAX_HEADERS:
                raise Efpia_Pdf_Parsing_Exception(f"too many columns")
            raise Efpia_Pdf_Parsing_Exception("not harmonizable, even without last page")

    @staticmethod
    def _merge_dfs(dfs: List[pd.DataFrame], headers: List[str], columns_to_delete: List[str]) -> pd.core.frame.DataFrame:
        for df in dfs:
            # if we miss one column, we add one empty on the left, it's the hcp/hco block going over more than a page.
            df.drop(df.columns[columns_to_delete], axis=1, inplace=True)
            df.columns = headers

        merged_df = pd.concat(dfs)
        merged_df.reset_index(drop=True, inplace=True)
        return merged_df

    @staticmethod
    def _remove_empty_columns(df):
        empty_cols = [col for col in df.columns if df[col].isnull().all()]
        # Drop these columns from the dataframe
        return df.drop(empty_cols, axis=1, inplace=False)

    @staticmethod
    def _are_sheets_harmonized(dfs):
        min_cols = min(x.shape[1] for x in dfs)
        max_cols = max(x.shape[1] for x in dfs)
        return min_cols == max_cols and min_cols >= N_MIN_HEADERS and max_cols <= N_MAX_HEADERS

    def _harmonize_sheets(self, dfs, filename, is_second_try=False):
        # easy case
        if self._are_sheets_harmonized(dfs):
            return dfs

        # try removing empty columns
        no_empty_cols_dfs = []
        for df in dfs:
            no_empty_cols_dfs.append(self._remove_empty_columns(df))
        if self._are_sheets_harmonized(no_empty_cols_dfs):
            logging.debug(f"{filename}: harmnonized after removing empty columns")
            return no_empty_cols_dfs

        # try identifying headers on all pages to remove useless columns on each sheet.

        col_identified_dfs = []
        header_list = []
        i = 0
        try:
            for df in dfs:
                i +=1
                headers, columns_to_delete = identify_headers.identify_headers(df, filename, sheet=i, reader_id=self.reader_id)
                header_list.append(headers)
                col_identified_df = df.drop(df.columns[columns_to_delete], axis=1, inplace=False)
                col_identified_df.columns = headers
                col_identified_dfs.append(col_identified_df)
        except Efpia_Pdf_Parsing_Exception:
            # since we are just trying to see if it works, it's ok to mute this Exception
            pass

        if len(col_identified_dfs) > 0 and self._are_sheets_harmonized(col_identified_dfs):
            # checking that all the headers lists identified are the same
            if header_list.count(header_list[0]) == len(header_list):
                logging.info(f"{filename}: harmonized after identifying columns on all pages")
                return col_identified_dfs
            else:
                logging.info(f"{filename}: after identifying columns, same col number on each sheet but different columns")

        # try removing the last sheet, likely the rnd table
        if not is_second_try and len(dfs) > 1:
            dfs.pop()
            logging.info(f"{filename}: impossible to harmonize all excel sheet, removing the last one and trying again")
            return self._harmonize_sheets(dfs, filename, is_second_try=True)



