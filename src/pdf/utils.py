import logging
import math
import re
import time
from typing import Union

from src import utils as utils_


class Efpia_Pdf_Parsing_Exception(Exception):
    pass

class Efpia_Pdf_Line_Parsing_Exception(Exception):
    pass

class No_Value_In_Line_Exception(Efpia_Pdf_Line_Parsing_Exception):
    pass



def parse_file_name(filename):
    if not (filename.endswith(".pdf") or filename.endswith(".xlsx") ):
        raise Efpia_Pdf_Parsing_Exception(f"File {filename} is neither a pdf nor an xslx")

    company, year, country = utils_.parse_file_name(filename)
    if company is None:
        raise Efpia_Pdf_Parsing_Exception(f"File {filename} is not named correctly. Name should be company__YEAR.pdf")
    return company, year


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



def remove_na(string: str) -> str:
    """
    Removes "n/a" or "n / a"
    """
    return re.sub(r"n\s*/\s*a", "", string)

def remove_cid(string: str) -> str:
    """
    Remove errored CID characters that pdf2text can return
    In the form of (cid:int) where int is an integer
    
    For example, parsing "data/0_downloads/sweden/en/Sanofi__2017.pdf" returns for the line 137
    137 | Number of Recipients in aggregate disclosure -... | 15(cid:10)\n(cid:10)| 8(cid:10)\n(cid:10) 
    """
    return re.sub(r"\(cid:\d+\)", "", string)

def all_sep_to_whitespace(string: str) -> str:
    """
    Replace all "whitespace character" (like tabs, newline) with a normal whitespace
    """
    return re.sub(r"\s+", " ", string)


def format_text(string: str, should_remove_na = True, shoud_remove_cid = True, to_lower_case=True) -> str:
    """
    Replace all whitespace by a single space, then strip and lower text. By default removes N/A value, but you can pass remove_na to False to avoid it.
    """
    txt = str(string)
    if to_lower_case:
        txt = txt.lower()

    if should_remove_na:
        txt = remove_na(txt)

    if shoud_remove_cid:
        txt = remove_cid(txt)

    return all_sep_to_whitespace(txt).strip()


def clean_str_value(value):
    """
    replaces any line break and tabs by a space, then removes trailing spaces. Removes "N/A".
    leaves non str values untouched.
    :param value:
    :return:
    """
    if not isinstance(value, str):
        return value

    value = str(value)
    value = value.replace("\n", " ")
    value = value.replace("\r", " ")
    value = value.replace("\t", " ")
    value = format_text(value, to_lower_case=False)
    return value


def get_number_null_if_zero(value:str) -> Union[int, float, None]:
    number = get_number(value)
    if number is None or number == 0 or math.isnan(number):
        return None
    return number


def get_number(value:str) -> Union[int, float, None]:
    """
    parse a string and return a valid float number
    return the first valid float number "1234 USD in 2019" -> 1234
    should be quite permissive with spaces since we obtain the string from a pdf
    """
    if (isinstance(value, int) or isinstance(value, float)) and not math.isnan(value):
        return value

    value = format_text(value)
    # if we have too many non whitespace or number characters, it's propably a text cell
    # we don't want to detect "According to article 12 of the code of conduct we..." as 12
    non_dec_count = len(re.findall(r"[^\d\s]", value))
    if non_dec_count > 15:
        return None

    # this is ugly but it works well: we have a problem if there is FR. in front of the number, because of the dot in the string group.
    # I have seen it only for swiss francs, so removing this like this.
    if value[0:3] == "fr.":
        value = value[3:].strip()

    # apostrophes can be only 1000 separators, removing them
    value = value.replace('’', "")
    value = value.replace("'", "")

    # first, let get everything before the first character different from
    numberMatch = re.match(r"^[^\d,.]*(\d[\d,.\s]*)[^\d,.]*", value)
    if numberMatch is None:
        return None


    numberGroup = numberMatch.group(1) # get the number group
    numberGroup = re.sub(r"\s", "", numberGroup) # remove any whitespace


    try:
        # python 'float()' function doesn't support number with ,
        # we have several situation
        dot_count = numberGroup.count(".")
        comma_count = numberGroup.count(",")

        # - no "," or ".", it's an integer let's parse it
        if dot_count == 0 and comma_count == 0:
            return float(numberGroup)

        # - only one ".", used as "dec" separator if less than 2 elements after
        if dot_count == 1 and comma_count == 0:
            if len(numberGroup.split(".")[-1]) <= 2:
                return float(numberGroup)

        # - only one ".", used as "dec" separator if only 1 0 before
        if dot_count == 1 and comma_count == 0:
            if  numberGroup.split(".")[0] == "0":
                return float(numberGroup)

        # - only one ",", used as "dec" separator if less than 2 elements after
        if dot_count == 0 and comma_count == 1:
            if len(numberGroup.split(",")[-1]) <= 2:
                return float(re.sub(r",", ".", numberGroup))

        # - at least one "," and maybe one "."
        # check if all "," are before "." and consistent
        if dot_count <= 1 and comma_count >= 1:
            if re.match(r"^0*\d{1,3}(,\d{3})+(\.\d+)?$", numberGroup):
                return float(re.sub(r",", "", numberGroup))
        
        # - at least one "." and maybe one ","
        # check if all "." are before "," and consistent
        if dot_count >= 1 and comma_count <= 1:
            if re.match(r"^0*\d{1,3}(\.\d{3})*(,\d+)?$", numberGroup):
                numberGroup = re.sub(r"\.", "", numberGroup)
                numberGroup = re.sub(r",", ".", numberGroup)
                return float(numberGroup)

        return None
    
    except ValueError as e:
        logging.warning(f"failed to parse {numberGroup} from {value} : {e}")

    return None


def diff_time(start_time):
    diff_sec =  time.time() - start_time
    hour, minutes, sec = "", "", ""
    if diff_sec > 3600:
        hour = f"{math.floor(diff_sec / 3600)} hours, "
    if diff_sec > 60:
        minutes = f"{math.floor(diff_sec % 3600 / 60)} minutes, "
    sec = f"{round(diff_sec % 3600 % 60)} seconds"
    return hour + minutes + sec

