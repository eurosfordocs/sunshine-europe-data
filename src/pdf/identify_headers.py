import collections
import logging
import re
from typing import List

from pandas.core.frame import DataFrame

from src.constants.efpia.header import Header, OPTIONAL_HEADERS
from src.pdf import utils
from src.pdf.settings import settings
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


def confirm_header_with_user(label, likeliest_header, filename, col, line, sheet=None, reader_id="camelot"):
    """
    Asks user to confirm the label for a text.
    :param label: the text found in the pdf
    :param likeliest_header: the value that best corresponds to the label give values in dictionary so far
    :param filename, col, line: for logging purpose
    :return: the user choice, or None
    """
    sheet_str = f", sheet {sheet}" if sheet is not None else ""
    choice = input(f"\nfile: {filename}{sheet_str}, col: {col}, line: {line}\n" +
                   f"Label:\t\t{utils.bcolors.HEADER}{label}{utils.bcolors.ENDC}\nSuggestion:\t{utils.bcolors.OKBLUE}{likeliest_header}{utils.bcolors.ENDC}" +
                   f"\n{utils.bcolors.OKGREEN}[Y] Accept suggestion{utils.bcolors.ENDC}"
                   f"\t{utils.bcolors.WARNING}[X] Add label to blacklist{utils.bcolors.ENDC}"
                   f"\t[C] Chose another header"
                   f"\t{utils.bcolors.FAIL}[Q] Dismiss entire file{utils.bcolors.ENDC}'"
                   f"\t[S] Skip without blacklisting\n'")
    if choice.lower() == 'y':
        settings.add_to_dictionary(settings.Sections.HEADERS, likeliest_header, label)
        return likeliest_header
    elif choice.lower() == 'x':
        settings.add_to_dictionary(settings.Sections.HEADERS, "blacklist", label)
        return None
    elif choice.lower() == 'q':
        settings.add_file_to_blacklist(filename, reader_id, settings.BlacklistReason.USER_CHOICE)
        raise Efpia_Pdf_Parsing_Exception("File added to the blacklist")
    elif choice.lower() == 's':
        return None
    else:
        options = {}
        i = 65 # char(65) = "A"
        text = ""
        for h in list(Header):
            options[chr(i).lower()] = h.value
            text += f"\n[{chr(i)}] {h}"
            i += 1
        text += "\n"
        choice = input(text).lower()
        if choice in options:
            settings.add_to_dictionary(settings.Sections.HEADERS, options[choice], label)
            return options[choice]
    return None


def check_labels(columns_labels: List[str], filename: str) -> bool:
    """
    Sanity check of the identified headers. Raises exception if:
        * some headers are duplicated
        * any mandatory headers are missing
    :param columns_labels: the identified headers
    :param filename: for logging purpose
    :return: n/a
    """
    for h in list(Header):
        if h.value not in columns_labels and h.value not in OPTIONAL_HEADERS:
            raise utils.Efpia_Pdf_Parsing_Exception(f"{h} not found in columns")

    duplicates = [item for item, count in collections.Counter(columns_labels).items() if count > 1]
    if duplicates not in [[], [None]]:
        raise utils.Efpia_Pdf_Parsing_Exception(f"duplicate column(s): {duplicates}")

    logging.debug(f"file {filename}: headers parsed correctly: {columns_labels}")
    return True


def identify_headers(df: DataFrame, filename: str, reader_id="camelot", sheet=None) -> List[str]:
    """
    identifies the columns of the df
    :param filename: for logging purposes
    :return:
    """
    # Assertion: the headers are in the first 10 lines
    head_df = df.head(20)
    columns_labels = []
    column_index = 0
    columns_to_delete = []
    for column_id in head_df.columns:
        column = head_df[column_id]
        header = None
        # for each line, we look for a column name which exact match

        for line in reversed(range(head_df.shape[0])):
            text = utils.format_text(column[line])
            if text != "":
                header = settings.get_exact_match(settings.Sections.HEADERS, text)
            if header is not None:
                logging.debug(f"file {filename}: column {column_id} is {header} (found on line {line}), label is: {text}")
                columns_labels.append(header)
                break

        if header is None:
            for line in column.index:

                text = utils.format_text(column[line])
                # we discard empty cells or N/A cells, cells containing "publi" ("date of publication..", "published on..."), and the ones already blacklisted
                if text != "" and "publi" not in text and not settings.is_blacklisted(settings.Sections.HEADERS, text) and not re.search(r"^[\d\.]+$", text):
                    likeliest_header = settings.get_fuzzy_match(settings.Sections.HEADERS, text)
                    header = confirm_header_with_user(text, likeliest_header, filename, column_id, line, sheet, reader_id)

                if header is not None:
                    logging.debug(f"file {filename}: USER CHOICE: column {column_id} is {header} (found on line {line}), label is: {text}")
                    columns_labels.append(header)
                    break

            if header is None:
                if column_index == 0:
                    logging.debug(f"file {filename}: No header was found for col 0, assuming this is BLOCK_TYPE")
                    columns_labels.append(Header.ROW_TYPE)

                elif column_index == len(df.columns) - 2:
                    logging.debug(f"file {filename}: No header was found for penultimate column, assuming this is RND")
                    columns_labels.append(Header.RND)

                else:
                    logging.debug(f"file {filename}: No header was found for col {column_id}, dismissing this column")
                    columns_to_delete.append(column_index)

        column_index += 1

    check_labels(columns_labels, filename)
    return columns_labels, columns_to_delete