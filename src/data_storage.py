
import logging
import os
import pathlib
from dotenv import load_dotenv
import subprocess


load_dotenv()


def init():
    os.environ["OS_AUTH_URL"] = os.getenv("OS_AUTH_URL")
    os.environ["OS_IDENTITY_API_VERSION"] = os.getenv("OS_IDENTITY_API_VERSION")
    os.environ["OS_USERNAME"] = os.getenv("OS_USERNAME")
    os.environ["OS_PASSWORD"] = os.getenv("OS_PASSWORD")
    os.environ["OS_TENANT_ID"] = os.getenv("OS_TENANT_ID")
    os.environ["OS_TENANT_NAME"] = os.getenv("OS_TENANT_NAME")
    os.environ["OS_REGION_NAME"] = os.getenv("OS_REGION_NAME")


def upload_folder(folder, subfolder):
    if folder not in ["normalized_data", "process_data"]:
        logging.error(f"Invalid folder value: {folder}")
        return None

    init()
    container = f"sunshine_europe_{folder}"

    if subfolder is not None and not os.path.exists(os.path.join("data", folder, subfolder)):
        logging.error(f"Subfolder {subfolder} not in data/{folder}")
        return None

    os.chdir(f"data/{folder}")

    # uploading the folder
    sf = subfolder if subfolder is not None else "."
    logging.info(f"starting to upload {folder}{('/' + subfolder) if subfolder is not None else ''} to the swift repo. This may take a while. ")
    r = subprocess.run(["swift", "upload", "--changed", "--skip-identical", container, sf], stdout=subprocess.PIPE)
    uploaded_files = str(r.stdout)[2:-3].split("\\n")
    uploaded_files_str = '\n\t'.join(uploaded_files)
    logging.info(f"Following files were uploaded if there weren't up to date already: \n\t{uploaded_files_str}")

    # after uploading files, we need to check for deleted files, we compare the local list to the distant list
    args = ["swift", "list", container]
    local_files = [str(x) for x in pathlib.Path(".").rglob("*") if x.is_file()]

    if subfolder is not None:
        args.extend(["-p", subfolder])
        local_files = [str(x) for x in pathlib.Path(".").rglob("*") if x.is_file() and str(x).startswith(f"{subfolder}/")]

    r = subprocess.run(args, stdout=subprocess.PIPE)
    distant_files = str(r.stdout)[2:-3].split("\\n")

    deleted_files = [x for x in distant_files if x not in local_files]
    deleted_files_str = '\n\t'.join(deleted_files)

    if len(deleted_files) > 0:
        user_choice = input(f"The following files are on the distant folder, but not in the local folder you just uploaded: "
                            f"\n\t{deleted_files_str}\n\nDo you want to delete them (Y/N): ")
        if user_choice.lower() == "y":
            for f in deleted_files:
                r = subprocess.run(["swift", "delete", container, f], stdout=subprocess.PIPE)
                logging.info(f"\tdeleted: {str(r.stdout)[2:-3]}")
    else:
        logging.info("Nothing to delete")
    os.chdir("../..")


def download_folder(folder, subfolder):
    if folder not in ["normalized_data", "process_data"]:
        logging.info(f"Invalid folder value: {folder}")
        return None
    init()
    container = f"sunshine_europe_{folder}"

    if not os.path.exists(f"data/{folder}"):
        os.mkdir(f"data/{folder}")

    os.chdir(f"data/{folder}")
    logging.info(f"starting to download {folder} into data/{folder}, this could take a while")
    if subfolder is None:
        args = ["swift", "download", "--skip-identical", container]
    else:
        args = ["swift", "download", "--skip-identical", "-p", subfolder, container]
    r = subprocess.run(args, stdout=subprocess.PIPE)
    downloaded_files = str(r.stdout)[2:-3].split("\\n")
    downloaded_files_str = '\n\t'.join(downloaded_files)
    logging.info(f"Downloaded following files: \n\t{downloaded_files_str}")
    os.chdir("../..")

