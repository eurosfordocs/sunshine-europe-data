import csv
import logging
import math
import os
import re

import numpy
import pandas as pd

from src.constants.schema_enums.efpia.link import Link as EL
from src.constants.schema_enums.normalized.entity import Entity as NE
from src.constants.schema_enums.normalized.link import Link as NL
from src.constants.schema_enums.normalized.publication import Publication as NP
from src.constants.value_constants import *
from . import utils
from src.constants.schemas import PROCESS_DIR, NORMALIZED_DIR
from src.manual_information import MANUAL_DIR


# Fields that can contain a value in the EFPIA file format. Each has the type and the category corresponding to it.
EFPIA_AMOUNT_FIELDS = {
    EL.AMOUNT__DONATION_AND_GRANT: {NL.TYPE: Type.CASH, NL.CATEGORY: Category.GRANT},
    EL.AMOUNT__EVENT_SPONSORSHIP: {NL.TYPE: Type.CASH, NL.CATEGORY: Category.EVENT_SPONSORSHIP},
    EL.AMOUNT__REGISTRATION_FEES: {NL.TYPE: Type.IN_KIND, NL.CATEGORY: Category.GIFT},
    EL.AMOUNT__TRAVEL_ACCOMMODATION: {NL.TYPE: Type.IN_KIND, NL.CATEGORY: Category.TRAVEL_ACCOMMODATION},
    EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES: {NL.TYPE: Type.CASH, NL.CATEGORY: Category.CONSULTING_FEE},
    EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES: {NL.TYPE: Type.CASH, NL.CATEGORY: Category.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES},
    EL.AMOUNT__JOINT_WORKING: {NL.TYPE: Type.CASH, NL.CATEGORY: Category.JOINT_WORKING}
}
RND = {NL.TYPE: Type.CASH, NL.CATEGORY: Category.RND}


class NormalizeException(Exception):
    pass


def check_efpia_csv(df, filename):
    for field in list(EL):
        if field.value not in df.columns:
            raise NormalizeException(f"Column '{field.value}' missing in {filename}")
    if len(list(EL)) != len(df.columns):
        raise NormalizeException(f"Error parsing {filename}. Expected {len(list(EL))} columns, found {len(df.columns)}")


class Normalizer:

    def __init__(self, folder_name, in_dir, out_dir,
                 generate_source_organization=False,
                 publication_id=None, hco_directory_id=None, hcp_directory_id=None, country=None):
        self._folder_name = folder_name
        self._in_dir = in_dir
        self._out_dir = out_dir
        self._generate_source_organization = generate_source_organization
        self._publication_id = publication_id
        self._hco_directory_id = hco_directory_id
        self._hcp_directory_id = hcp_directory_id
        self._country = country
        self._entities = []
        self._industry_entities = []
        self._links = []
        self._entities_max_ids = {}
        self._link_max_ids = {}
        self._rows_tot = 0
        self._manual_entities={}
        self._manual_publications={}

        self.load_manual_data()

    def load_manual_data(self):
        for filename in os.listdir(MANUAL_DIR):
            if filename.startswith("publication__"):
                with open(os.path.join(MANUAL_DIR, filename), "r") as f:
                    publications = csv.DictReader(f)
                    for p in publications:
                        self._manual_publications[p[NP.PUBLICATION_ID]] = p

            if filename.startswith("entity__"):
                with open(os.path.join(MANUAL_DIR, filename), "r") as f:
                    entities = csv.DictReader(f)
                    for e in entities:
                        self._manual_entities[e[NE.ENTITY_ID]] = e

    def _get_new_entity_id(self, publication_id: str):
        if publication_id in self._entities_max_ids:
            self._entities_max_ids[publication_id] += 1
        else:
            self._entities_max_ids[publication_id] = 1
        return self._entities_max_ids[publication_id]

    def _get_new_link_id(self, publication_id: str):
        if publication_id in self._link_max_ids:
            self._link_max_ids[publication_id] += 1
        else:
            self._link_max_ids[publication_id] = 1
        return self._link_max_ids[publication_id]

    def _new_industry_entity(self, entity_id, full_name):
        """
        If not already existing, creates a new publishing entity.
        """
        for e in self._industry_entities:
            if e[NE.ENTITY_ID] == entity_id:
                return e

        entity = {}
        for k in list(NE):
            entity[k.value] = None

        entity[NE.ENTITY_ID] = entity_id
        entity[NE.FULL_NAME] = full_name
        entity[NE.ENTITY_TYPE] = EntityType.INDUSTRY
        entity[NE.COUNTRY] = self._country
        entity[NE.IS_PERSON] = False
        self._industry_entities.append(entity)
        return entity

    def _new_entity(self, row, publication_id):
        """
        Creates, stores and returns a new entity if the row is Individual, returns None otherwise.
        """
        if row[EL.DISCLOSURE_TYPE] != DisclosureType.INDIVIDUAL:
            return None

        entity = {}
        for k in list(NE):
            entity[k.value] = None

        entity_id = f"{publication_id}_{self._get_new_entity_id(publication_id)}"
        entity[NE.ENTITY_ID] = entity_id
        entity[NE.FULL_NAME] = row[EL.RECIPIENT_FULL_NAME]
        entity[NE.IS_PERSON] = row[EL.RECIPIENT_TYPE] == EntityType.HCP
        entity[NE.PERSON_FIRST_NAME] = row[EL.RECIPIENT_PERSON_FIRST_NAME]
        entity[NE.PERSON_LAST_NAME] = row[EL.RECIPIENT_PERSON_LAST_NAME]
        entity[NE.PERSON_PROFESSION] = row[EL.RECIPIENT_PERSON_PROFESSION]
        entity[NE.PERSON_SPECIALTY] = row[EL.RECIPIENT_PERSON_SPECIALTY]
        entity[NE.ENTITY_TYPE] = row[EL.RECIPIENT_TYPE]
        entity[NE.ADDRESS] = row[EL.RECIPIENT_ADDRESS]
        entity[NE.CITY] = row[EL.RECIPIENT_CITY]
        entity[NE.COUNTRY] = row[EL.RECIPIENT_COUNTRY]

        if row[EL.RECIPIENT_IDENTIFIER] is not None:
            if row[EL.RECIPIENT_TYPE] == EntityType.HCP:
                entity[NE.DIRECTORY_ID] = self._hcp_directory_id
            else:
                entity[NE.DIRECTORY_ID] = self._hco_directory_id
            entity[NE.DIRECTORY_ENTITY_ID] = row[EL.RECIPIENT_IDENTIFIER]



        self._entities.append(entity)
        return entity

    def _new_link(self, publication_id, recipient, source_organization, row, type, category, amount, year, source_organization_id, number_recipients=None):
        """
        Creates and stores a new link corresponding to the params.
        """
        link = {}
        for k in list(NL):
            link[k.value] = None

        link_id = f"{publication_id}_{self._get_new_link_id(publication_id)}"
        link[NL.LINK_ID] = link_id
        link[NL.PUBLICATION_ID] = publication_id

        if recipient is not None: #individual links
            link[NL.RECIPIENT_ENTITY_ID] = recipient[NE.ENTITY_ID]
            link[NL.RECIPIENT_ENTITY_TYPE] = recipient[NE.ENTITY_TYPE]
            link[NL.NUMBER_OF_AGGREGATED_RECIPIENTS] = 1
        elif row[EL.DISCLOSURE_TYPE] != DisclosureType.RND: #aggregated links
            link[NL.RECIPIENT_ENTITY_TYPE] = row[EL.RECIPIENT_TYPE]
            link[NL.NUMBER_OF_AGGREGATED_RECIPIENTS] = number_recipients
        else: #rnd disclosures
            link[NL.RECIPIENT_ENTITY_TYPE] = DisclosureType.RND
            link[NL.NUMBER_OF_AGGREGATED_RECIPIENTS] = None

        if source_organization is not None:
            link[NL.SOURCE_ORGANIZATION_ID] = source_organization[NE.ENTITY_ID]
        else:
            link[NL.SOURCE_ORGANIZATION_ID] = source_organization_id

        link[NL.TYPE] = type
        link[NL.CATEGORY] = category
        if row[EL.PUBLICATION_YEAR] != "":
            link[NL.YEAR] = row[EL.PUBLICATION_YEAR]
        else:
            link[NL.YEAR] = year
        link[NL.VALUE_TOTAL_AMOUNT] = amount
        link[NL.VALUE_TOTAL_AMOUNT_EUR] = utils.to_eur(amount, row[EL.CURRENCY], year=row[EL.PUBLICATION_YEAR])
        link[NL.CURRENCY] = row[EL.CURRENCY]
        link[NL.IS_AGGREGATED] = True
        self._links.append(link)

    def _extract_links(self, row, publication_id, entity, source_organization, year, source_organization_id):
        """
        If row is not RND, creates and stores a link per value in the row
        If row is RND, creates and stores a link corresponding to the RND value
        """
        if row[EL.DISCLOSURE_TYPE] != DisclosureType.RND:
            for k in EFPIA_AMOUNT_FIELDS.keys():
                if row[k] is not None and not math.isnan(row[k]) and row[k] != 0:
                    recipients = 1
                    if k != EL.AMOUNT__JOINT_WORKING:
                        recipients = row[re.sub("amount", "number_of_recipients", k)]
                    self._new_link(publication_id, entity, source_organization, row,
                                   type=EFPIA_AMOUNT_FIELDS[k][NL.TYPE],
                                   category=EFPIA_AMOUNT_FIELDS[k][NL.CATEGORY],
                                   amount=row[k],
                                   year=year,
                                   source_organization_id= source_organization_id,
                                   number_recipients=recipients)
        else:
            self._new_link(publication_id, entity, source_organization, row,
                           type=RND[NL.TYPE],
                           category=RND[NL.CATEGORY],
                           amount=row[EL.AMOUNT__TOTAL],
                           year=year,
                           source_organization_id= source_organization_id
                           )

    def _normalize_row(self, row, publication_id, source_organization_id, year):
        """
        creates and stores the entity (if any) and the link(s) of the row
        """
        entity = self._new_entity(row, publication_id)

        source_organization = None
        industry_name = row[EL.SOURCE_ORGANIZATION_FULL_NAME].replace(" ", "-").lower()
        industry_entity_id = f"{self._folder_name}__{industry_name}"

        if source_organization_id == "":
            source_organization_id = industry_entity_id

        if self._generate_source_organization:
            source_organization = self._new_industry_entity(industry_entity_id, row[EL.SOURCE_ORGANIZATION_FULL_NAME])
        self._extract_links(row, publication_id, entity, source_organization, year, source_organization_id)

    def _normalize_csv(self, filename):
        """
        Reads an EFPIA csv, normalize all the rows in it.
        """
        file_path = os.path.join(self._in_dir, filename)
        df = pd.read_csv(file_path, low_memory=False)
        df = df.replace({numpy.nan: None})
        check_efpia_csv(df, filename)

        # filename examples: de__msd__2016.csv, links_2016.csv
        year = filename.split(".")[0][-4:]

        # publication_id example: de__msd__2016
        publication_id = filename[:-4]

        # source_organization_id example: de__msd
        source_organization_id = publication_id[:-6]

        if self._publication_id is not None:
            publication_id = self._publication_id
        # getting currency from first row
        currency = df[EL.CURRENCY].iloc[0]

        i = 0
        for line, row in df.iterrows():
            self._normalize_row(row, publication_id, source_organization_id, year)
            self._rows_tot += 1
            i += 1
            if df.shape[0] > 15000:
                utils.print_progress_bar(i, df.shape[0], f"Normalize {self._folder_name} {filename}".ljust(30))

    def normalize_folder(self):
        """
        Normalizes all the csv of the in_dir. generates links.csv and entities.csv in out_dir
        Creates or replace csv files link.csv, entity_link.csv.
        If `publication_id` is used, all the links are attached to the same publication. Ireland and UK uses this option.
            otherwise the publication_id will be taken from the file name
        """

        # for each csv file, check if they respect the efpia format, if so, call normalize_csv
        files = os.listdir(self._in_dir)
        files.sort(reverse=True)

        csv_count = sum(1 if f[-3:] == "csv" else 0 for f in files)
        logging.info(f"Found {csv_count} files to normalize in folder {self._in_dir}")

        i = 0
        for file in files:
            if file[-3:] == "csv":
                i += 1
                try:
                    self._normalize_csv(file)
                    if csv_count > 10:
                        utils.print_progress_bar(i, csv_count, f"Normalize {self._folder_name}".ljust(30))
                except NormalizeException as e:
                    logging.error(f"file {file}: {e}")

        entities_file = os.path.join(self._out_dir, "entity.csv")
        self._entities.extend(self._industry_entities)
        keys = self._entities[0].keys()
        with open(entities_file, 'w') as f:
            dict_writer = csv.DictWriter(f, keys)
            dict_writer.writeheader()
            dict_writer.writerows(self._entities)

        links_file = os.path.join(self._out_dir, "link.csv")
        keys = self._links[0].keys()
        with open(links_file, 'w', encoding="utf-8") as f:
            dict_writer = csv.DictWriter(f, keys)
            dict_writer.writeheader()
            dict_writer.writerows(self._links)

        logging.info(f"Normalized {csv_count} files, {self._rows_tot} rows in total. "
                     f"Extracted \n - {len(self._entities)} entities \n - {len(self._links)} links")


def normalise_folder(folder_name, generate_source_organization=False,
                     publication_id=None, in_dir=None, out_dir=None,
                     hco_directory_id=None, hcp_directory_id=None, country=None):
    if in_dir is None:
        in_dir = os.path.join(PROCESS_DIR, folder_name, "2_harmonized")
    if out_dir is None:
        out_dir = os.path.join(NORMALIZED_DIR, folder_name)

    normalizer = Normalizer(folder_name, in_dir, out_dir,
                            generate_source_organization=generate_source_organization,
                            publication_id=publication_id,
                            hco_directory_id=hco_directory_id,
                            hcp_directory_id=hcp_directory_id,
                            country=country)
    normalizer.normalize_folder()





