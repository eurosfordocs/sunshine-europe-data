import logging
import os
import shutil
from tempfile import NamedTemporaryFile

import requests

from src import utils
from src.constants.schemas import NORMALIZED_SCHEMA_DIR, HARMONIZED_SCHEMA_DIR, SCHEMA_DIR

SCHEMAS_URL = "https://gitlab.com/eurosfordocs/sunshine-europe-website/-/archive/master/sunshine-europe-website-master.zip?path=schemas"
UNZIPPED_FOLDER_NAME = "sunshine-europe-website-master-schemas"
ENUM_FOLDER = "src/constants/schema_enums"
SKELETON_FILE = os.path.join(ENUM_FOLDER, "enum_skeleton.txt")


class SchemaImportException(Exception):
    pass


def import_schema():
    """
    import the schemas from the public repo sunshine-europe-website
    generate field Enums based on these schemas
    don't call this function manually, run `python main.py schema`
    """
    import_schema_jsons()
    generate_all_enums()


def import_schema_jsons():
    logging.getLogger().setLevel(logging.INFO)
    if "LICENCE.txt" not in os.listdir():
        raise SchemaImportException("import_schema should be run via the main of the project, or with the command make install")

    temp_file = NamedTemporaryFile()
    logging.info(f"downloading schemas from website gitlabl, url: \n{SCHEMAS_URL}")
    r = requests.get(url=SCHEMAS_URL, stream=True)
    for chunk in r.iter_content():
        temp_file.write(chunk)

    temp_file.seek(0)
    shutil.unpack_archive(temp_file.name, format='zip')

    if SCHEMA_DIR in os.listdir(UNZIPPED_FOLDER_NAME):
        # remove the schemas folder if present
        if SCHEMA_DIR in os.listdir():
            shutil.rmtree(SCHEMA_DIR)
        # move unzipped_folder/schemas > schemas
        shutil.move(os.path.join(UNZIPPED_FOLDER_NAME, SCHEMA_DIR), SCHEMA_DIR)
        # removed unzipped_folder
        shutil.rmtree(UNZIPPED_FOLDER_NAME)
        logging.info("schemas updated from Sunshine Europe public repo")
    else:
        logging.warning("Tried to download updated schemas from public repo, but no folder called \"schemas\" in it. "
                        "Schemas were not updated.")




def write_enum_file(class_name, properties, sub_folder):
    """
    write an enum file using the skeleton file, replacing class_name and properties
    """
    enum_file_name = os.path.join(ENUM_FOLDER, sub_folder, f"{class_name.lower()}.py")
    with open(SKELETON_FILE, "r") as skeleton_file:
        if sub_folder not in os.listdir(ENUM_FOLDER):
            os.mkdir(os.path.join(ENUM_FOLDER, sub_folder))
        with open(enum_file_name, "w+") as enum_file:
            content = skeleton_file.read().replace("{class_name}", class_name).replace("{properties}", properties)
            enum_file.write(content)
            enum_file.close()


def generate_dir_enums(schema_dir):
    """
    generate the enums corresponding to the dir passed
    """
    sub_folder = os.path.split(schema_dir)[-1]
    schemas = utils.get_all_schema(schema_dir)
    if len(schemas) == 0:
        raise SchemaImportException(f"no schemas found in {schema_dir}, cannot generate Enums")
    for schema in schemas:
        properties = ""
        class_name = schema.descriptor.get("name").capitalize()
        for h in schema.headers:
            properties += f"\n    {h.upper()} = \"{h}\""
        write_enum_file(class_name, properties, sub_folder)


def generate_all_enums():

    # remove existing enums
    for f in os.listdir(ENUM_FOLDER):
        if os.path.isdir(f):
            shutil.rmtree(f)
    logging.info("deleted Enum files")

    generate_dir_enums(NORMALIZED_SCHEMA_DIR)
    generate_dir_enums(HARMONIZED_SCHEMA_DIR)

    logging.info("recreated Enum files")



