# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Scraping Denmark
#
#

# ## Imports

import os

import pandas as pd
from bs4 import BeautifulSoup

# Lets get the downloaded data.
country_dir = os.path.join("data", "process_data", "portugal")
downloads_dir = os.path.join(country_dir, "0_downloads")

if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")


# ## Description of the html struct
#
# Each entry of the data is separated into two rows.
#
# ```html
# <tbody><tr class="btngeral_gridview">
#                 <th scope="col"><a>Nome Entidade Contribuinte</a></th> 
#                 <th scope="col"><a>Tipo Declaração</a></th>
#                 <th scope="col"><a>Evento/Bem/Ação</a></th>
#                 <th scope="col"><a>Quantia (€)</a></th>
#                 <th scope="col"><>Nome Entidade Recetora</a></th>
#                 <th scope="col"><a>Estado</a></th>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  670,00</td><td>Fernanda Conceição Matos Linhares Martins</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  620,00</td><td>Ana Catarina Martins Lameiras</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  770,00</td><td>Dora Susana Lemos Rodrigues da Cruz Sargento</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  250,00</td><td>Marta Brandão Pinto Calçada</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                 1040,00</td><td>Pedro Pimentel Duarte</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  620,00</td><td>Liliana do Carmo Jorge Dias Torres</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  620,00</td><td>Maria João Morgado Nabais Baldo</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>24º Congresso Nacional MI</td><td>                  670,00</td><td>Carina Isabel Pereira Ramalho</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Fórum DT1</td><td>                  236,59</td><td>Ester Augusta Figueira Gama</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Fórum DT1</td><td>                  308,59</td><td>Carla Sónia Oliveira Meireles Bilhoto</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Fórum DT1</td><td>                   85,41</td><td>José Carlos Sousa Maia</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>XXV Congresso Português de Cirurgia da Mão</td><td>                  175,00</td><td>Paulo Gil Azevedo Ribeiro</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>13th Scientific and Annual Meeting ESCP</td><td>                  322,50</td><td>Olga Manuela Duarte Correia Oliveira</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Curso APDP - C7 Sistemas de Perfusão Subcutânea Contínua de Insulina</td><td>                  300,00</td><td>Nuno Filipe da Costa Bernardino Vieira</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>13th IHPBA World Congress</td><td>                  240,80</td><td>Jorge de Almeida Pereira</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>VII Reunião do Grupo de Estudos de Doenças do Miocárdio e Pericárdio</td><td>                 9345,00</td><td>Sociedade Portuguesa de Cardiologia</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>I reunião do CPPC e GRBCGC</td><td>                  615,00</td><td>Sociedade Portuguesa de Cardiologia</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Reunião Anual da Sociedade de Endocrinologia e Diabetologia Pediátrica da SPP</td><td>                 1230,00</td><td>SPP - Sociedade Portuguesa de Pediatria</td><td>Validado</td>
# 			</tr><tr class="grid-row-style">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>Fórum DT1</td><td>                  236,59</td><td>Sara Gabriela EstevesFerreira</td><td>Validado</td>
# 			</tr><tr class="grid-row-style-alter">
# 				<td style="font-weight:bold;">Sanofi - Produtos Farmacêuticos, Lda.</td><td>Medicamentos</td><td>17th European Congress of Internal Medicine</td><td>                  250,00</td><td>Maria Francisca Vaz Pinto Beires</td><td>Validado</td>
# 			</tr>
# 		</tbody>
# ```
# We have to parse and transform this structure.
#
# Since it's not that much the job of the scrapper to interpret the data, I can either :
# - create a JSON like structure in Python which mimics the HTML one
# - already create a row like, flat structure which will probably the output anyway
#
# I'll take the second option for now **but** since I don't know which one I'll need later, the first option is more versatile, and can be easily convert to a flat output.





def extract_portugal_data_from(html, year):
    output_rows = []
    
    name = ""
    company_name = ""
    fee = ""
    reason = ""
    declaration_type = ""
    isValid = ""
    
    soup = BeautifulSoup(html, features="html.parser")
    
    for row in soup.tbody.find_all("tr", recursive=False):
        data = row.find_all("td")
        if len(data) == 6:
            company_name = data[0].get_text(strip=True)
            declaration_type = data[1].get_text(strip=True)
            reason = data[2].get_text(strip=True)
            fee = data[3].get_text(strip=True)
            name = data[4].get_text(strip=True)
            isValid = data[5].get_text(strip=True)
            output_rows.append(
                            {"name": name,
                             "company_name": company_name,
                             "year": year,
                             "fee": fee,
                             "reason": reason,
                             "declaration_type": declaration_type,
                             "isValid": isValid,
                            })
        elif len(data) == 0:
            # pass, probably the headers
            pass
        else:
            raise Exception("invalid html")
    
    return output_rows


def get_filepaths(country, year):
    base_path = os.path.join(downloads_dir, year)
    return [os.path.join(base_path, filename) for filename in os.listdir(base_path) if filename.endswith(".html")]


years = ["2019", "2018", "2017", "2016", "2015", "2014", "2013"]
for year in years:
    files = get_filepaths("portugal", year)

    aggregated_data = []

    for filepath in files:
        with open(filepath, "r", encoding="utf-8") as f:
            try:
                html = f.read()
                rows = extract_portugal_data_from(html, year)
            except Exception as e:
                print(str(e) + " with file " + filepath)
            aggregated_data.extend(rows)
            
    df = pd.DataFrame(aggregated_data)
    df.head(20)
    print(f"Portugal - {year} has {df.size} entries")
    uniq_names = df["name"].unique()
    uniq_companies = df["company_name"].unique()
    print(f"There is {len(uniq_names)} unique names")
    print(f"There is {len(uniq_companies)} unique companies")
    extracted_path = os.path.join(country_dir, "1_extracted", f"{year}.csv")
    df.to_csv(extracted_path)
    print(f"extracted data saved to {extracted_path}")

# At this point we can check that our data is correct.
