# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os

import pandas as pd

pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 300

if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import src.utils as utils

extracted_folder = "data/process_data/romania/1_extracted"

normalized_folder = 'data/normalized_data/romania/'
if not os.path.exists(normalized_folder):
    os.mkdir(normalized_folder)

# +
df_link_uk = pd.read_csv("data/normalized_data/uk/link.csv")
link_columns = df_link_uk.columns

df_entity_uk = pd.read_csv("data/normalized_data/uk/entity.csv")
entity_columns = df_entity_uk.columns


def align_to_columns_list(df, columns_list):
    # fill missing columns
    df = df.copy()
    for col in columns_list:
        if col not in df.columns:
            df[col] = ''
    return df[columns_list]


def to_csv_append(df, filepath):
    if not os.path.isfile(filepath):
        df.to_csv(filepath, index=False)
    else:
        df.to_csv(filepath, index=False, mode='a', header=False)


# +
years = [2016, 2017, 2018, 2019]
kind = "sponsori"

df_list = []
for year in years:
    file = f"{extracted_folder}/romania__{year}__{kind}.csv"
    df = pd.read_csv(file, header=[0, 1])
    df["link_id"] = 'romania__' + str(year) + '__' + df.iloc[:, 0].map(str)
    df_list.append(df)

df = pd.concat(df_list)
# -

df = df.iloc[:, 2:]

# +

df.columns = pd.MultiIndex.from_tuples([
    ('sponsor', 'type'),
    ('sponsor', 'name'),

    ('recipient', 'name'),
    ('recipient', 'specialty'),
    ('recipient', 'address'),

    ('Sponsorizare', 'category'),
    ('Sponsorizare', 'details'),
    ('Sponsorizare', 'value_total_amount'),
    ('Sponsorizare', 'contract_date'),
    ('Sponsorizare', 'payment_date'),
    ('Sponsorizare', 'currency'),

    ('Onorarii', 'details'),
    ('Onorarii', 'value_total_amount'),
    ('Onorarii', 'associated_expense_value_total_amount'),
    ('Onorarii', 'contract_date'),
    ('Onorarii', 'payment_date'),

    ('Alte cheltuieli', 'details'),
    ('Alte cheltuieli', 'value_total_amount'),
    ('Alte cheltuieli', 'contract_date'),
    ('Alte cheltuieli', 'payment_date'),
    ('Alte cheltuieli', 'currency'),

    ('link_id', '')
])
# -

# #### associated_expense as a new category

# +
df[("Cheltuieli asociate", "value_total_amount")] = df[('Onorarii', 'associated_expense_value_total_amount')]

df[("Cheltuieli asociate", "details")] = df[('Onorarii', 'details')]
df[("Cheltuieli asociate", "contract_date")] = df[('Onorarii', 'contract_date')]
df[("Cheltuieli asociate", "payment_date")] = df[('Onorarii', 'payment_date')]

df = df.drop(columns=('Onorarii', 'associated_expense_value_total_amount'))
# -

# ## Create entity id and tables

df[('sponsor', 'id')] = (df.sponsor.type.fillna("") + '_' +
                         df.sponsor.name.fillna("")).map(lambda x: 'romania__' + x.replace("_", "").lower().replace(" ", "_"),
                                                         na_action='ignore')

df[('recipient', 'id')] = (df.recipient.name.fillna("") + '_' +
                           df.recipient.specialty.fillna("") + '_' +
                           df.recipient.address.fillna("")).map(
    lambda x: 'romania__recipient__' + str(hash(x))[-16:], na_action='ignore')

# +
df_sponsor_entity = df.sponsor.drop_duplicates()

df_sponsor_entity = df_sponsor_entity.rename(columns={
    "id": 'entity_id',
    "type": "entity_type",
    "name": "full_name"
})

df_sponsor_entity["is_person"] = False
df_sponsor_entity["entity_type"] = "Industry"
df_sponsor_entity["country"] = "Romania"

df_sponsor_entity = align_to_columns_list(df_sponsor_entity, entity_columns)
df_sponsor_entity.set_index('entity_id', inplace=True)
df_sponsor_entity = df_sponsor_entity[~df_sponsor_entity.index.duplicated(keep='first')]

df_sponsor_entity.to_csv(normalized_folder + f'entity__companies.csv')

# +
df_reciptient_entity = df.recipient.drop_duplicates()

df_reciptient_entity = df_reciptient_entity.rename(columns={
    "id": 'entity_id',
    "name": "full_name",
    "specialty": "person_specialty",
    "address": "address",

})

df_reciptient_entity["entity_type"] = "unknown"
df_reciptient_entity["country"] = "Romania"

df_reciptient_entity = align_to_columns_list(df_reciptient_entity, entity_columns)
df_reciptient_entity.to_csv(normalized_folder + f'entity__recipient.csv', index=False)

# -

# ## Create links table

# +
df = df.drop(columns=[
    ('sponsor', 'type'),
    ('sponsor', 'name'),

    ('recipient', 'name'),
    ('recipient', 'specialty'),
    ('recipient', 'address')
])

df = df.set_index([
    ('link_id', ''),
    ('sponsor', 'id'),
    ('recipient', 'id'),

])

df = df.stack(level=0)


df.index.names = ('link_id', 'source_organization_id', 'recipient_entity_id', 'general_category')


df = df.reset_index()


df["link_id"] = df["link_id"] + '_' + df["general_category"].map(lambda x : x.lower().replace(" ", "-"))


# -

# #### amount

df = df.dropna(subset=["value_total_amount"]).copy()


# #### details

# +
def get_column_formatter(prefix, str_format="{} : '{}' "):
    def format_value(value):
        if value is None or pd.isna(value):
            return ''
        else:
            return str_format.format(prefix, value)

    return format_value


df.details = (df.details + ' | ' +
              df.contract_date.map(get_column_formatter("contract date")) + ' | ' +
              df.payment_date.map(get_column_formatter("payment date")))

df = df.drop(columns=["contract_date", "payment_date"])
# -

df.category = df.category.fillna(df.general_category)

df = df.drop(columns="general_category")

# #### date

df["year"] = df["link_id"].str[9:13]

# #### clean currency

# +
df.currency = df.currency.str.upper()
df.currency = df.currency.map(lambda x: 'EUR' if x == 'EURO' else x)
df.currency = df.currency.fillna("RON")
df.loc[df.currency.map(len) > 3, "currency"] = 'RON'

#correcting typos in the data
df["currency"].replace("GPB", "GBP", inplace=True)
df["currency"].replace("LEI", "RON", inplace=True)
df["currency"].replace("UDS", "USD", inplace=True)

df["value_total_amount_eur"] = df.apply(lambda row: utils.to_eur(amount=row.value_total_amount, 
                                                                 currency=row.currency, 
                                                                 year=row.year), 
                                        axis=1)
assert df["value_total_amount_eur"].isna().sum() == 0
# -

# #### static values and rounding

df.value_total_amount = df.value_total_amount.round(2)
df.value_total_amount_eur = df.value_total_amount_eur.round(2)
df["publication_id"] = "romania"
df["is_aggregated"] = False
df["number_of_aggregated_recipients"] = 1
df = align_to_columns_list(df, link_columns)
df.to_csv(normalized_folder + f'link.csv', index=False)

# # Analyse

df_link = df.merge(df_sponsor_entity, left_on="source_organization_id", right_on="entity_id")

df_link["value_total_amount_ron"] = (df_link.value_total_amount_eur * 4.76).map(int)
df_link["value_total_amount_eur"] = df_link["value_total_amount_eur"].map(int)

df_total = df_link[df_link.year == "2019"].groupby(["full_name"])[["value_total_amount_eur", 'value_total_amount_ron']].sum()

df_total = df_total.sort_values("value_total_amount_eur", ascending=False)

df_total.head(30)

df_total.to_excel("notebooks/ignore/romania_total_per_company_2019.xlsx")


