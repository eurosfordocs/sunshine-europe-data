# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os
from datetime import datetime

import pandas as pd

if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")

from src.constants.schema_enums.normalized.link import Link as NL
from src.constants.schema_enums.normalized.entity import Entity as EL
from src.constants.value_constants import EntityType

link_columns = [k.value for k in list(NL)]
entity_columns = [k.value for k in list(EL)]

# Then we go from raw data to normalized data

# +
# Iterate on years available

entity_dfs = []
link_dfs = []


years = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
for year in years:
    path = f"data/process_data/portugal/1_extracted/{year}.csv"
    df = pd.read_csv(path)
    df = df.reset_index()
    # Normalized_df is the dataframe with the correct column names, that we will fill with the df data
    link_df = pd.DataFrame(columns=link_columns)
    industry_entity_df = pd.DataFrame(columns=entity_columns)
    recipient_df = pd.DataFrame(columns=entity_columns)
    
    
####### MAPPING from extracted data to normalized table
    link_df[NL.PUBLICATION_ID] = 'portugal'
    link_df[NL.LINK_ID] = 'portugal-link-' + str(year) + "-" + df['index'].astype(str)

    link_df[NL.SOURCE_ORGANIZATION_ID] = df['company_name'].map(lambda a: f"pt__{a.replace(' ', '_').lower()}")
    industry_entity_df[EL.ENTITY_ID] = df['company_name'].map(lambda a: f"pt__{a.replace(' ', '_').lower()}")
    industry_entity_df[EL.FULL_NAME] = df['company_name']
    industry_entity_df[EL.ENTITY_TYPE] = EntityType.INDUSTRY
    industry_entity_df[EL.COUNTRY] = 'Portugal'
    industry_entity_df[EL.IS_PERSON] = False

    link_df[NL.RECIPIENT_ENTITY_ID] = 'portugal-recipient-' + str(year) + "-" + df['index'].astype(str)

    recipient_df[EL.ENTITY_ID] = 'portugal-recipient-' + str(year) + "-" + df['index'].astype(str)
    recipient_df[EL.FULL_NAME] = df['name']
    recipient_df[EL.ENTITY_TYPE] = EntityType.HCP
    recipient_df[EL.IS_PERSON] = True

    link_df[NL.DETAILS] = df['reason']
    link_df[NL.YEAR] = df['year']
    link_df[NL.VALUE_TOTAL_AMOUNT] = df['fee'].map(lambda x: float(x.split(',')[0]) + float(x.split(',')[1]) * 1 / 100)
    link_df[NL.VALUE_TOTAL_AMOUNT_EUR] = df['fee'].map(lambda x: float(x.split(',')[0]) + float(x.split(',')[1]) * 1 / 100)
    link_df[NL.CURRENCY] = 'EUR'
    link_df[NL.PUBLICATION_ID] = 'portugal'
    link_df[NL.IS_AGGREGATED] = False


    ##### Saving the result to csv
    industry_entity_df.drop_duplicates(inplace=True)
    entity_df = pd.concat([industry_entity_df, recipient_df])
    link_dfs.append(link_df)
    entity_dfs.append(entity_df)

all_years_link_df = pd.concat(link_dfs)
all_years_entity_df = pd.concat(entity_dfs)
all_years_entity_df.drop_duplicates(inplace=True)

all_years_link_df.to_csv(path_or_buf=f'data/normalized_data/portugal/link.csv', index=False)
all_years_entity_df.to_csv(path_or_buf=f'data/normalized_data/portugal/entity.csv', index=False)



# -


