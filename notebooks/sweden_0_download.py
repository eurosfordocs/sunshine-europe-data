# +
import requests
from bs4 import BeautifulSoup
import logging
from src.constants.schema_enums.normalized.publication import Publication as NP
import os
import csv


def find_urls(page):
    r = requests.get(page)
    soup = BeautifulSoup(r.text, features="html.parser")
    field = soup.select("#paginationroom > div.result-row:not(.heading)")
    publications = []
    for company in field:

        publication = {}
        for k in list(NP):
            publication[k.value] = None

        year = company.select("div.result-col")[1].text

        publication["company_name"] = company.select("div.result-col")[0].text
        publication["company_id"] = company.select("div.result-col")[0].text.lower().split(" ")[0]

        publication[NP.PUBLISHING_ENTITY_ID] = "sw__lif"
        publication[NP.CURRENCY] = "SEK"
        publication[NP.COVERED_PERIOD_START_DATE] = f"{year}-01-01"
        publication[NP.COVERED_PERIOD_END_DATE] = f"{year}-12-31"

        for link in company.select("a"):
            if link.text == 'Värdeöverföringar':
                loc = link.get_attribute_list("href")[0]
                publication[NP.SOURCE_URL] = f"https://www.lif.se{loc}"
            if link.text == 'Länk till värdeöverföringar':
                publication[NP.SOURCE_URL] = link.get_attribute_list("href")[0]

        publications.append(publication)
    return publications



logging.getLogger().setLevel(logging.INFO)
page = "https://www.lif.se/etik/samarbetsdatabaser/?type=OpenValueTransferReports"

r = requests.get(page)

# print(r.text)

soup = BeautifulSoup(r.text, features="html.parser")

# print(report)

page_nav = soup.select("#PageNumber > option:nth-last-child(1)")
# print(page_nav)
last_page = int(page_nav[-1].get_text())

print(f"There are {last_page} pages to scrap")

pages = [page + '&' + 'page=' + str(i) for i in range(1, last_page + 1)]

publications = []
for page in pages:
    pubs = find_urls(page)
    publications.extend(pubs)
    logging.info(f"{page}: {len(pubs)} publications found")

publications_file = os.path.join("data", "publication.csv")
keys = publications[0].keys()
with open(publications_file, 'w') as f:
    dict_writer = csv.DictWriter(f, keys)
    dict_writer.writeheader()
    dict_writer.writerows(publications)
