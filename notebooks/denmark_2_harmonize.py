# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Denmark harmonize
#
# Don't forget to generate the intermediate extracted data (not stored on git) with `cd notebooks && python denmark_1_extract.py`

# +
import os
import re

import pandas as pd
from dateutil.parser import parse as date_parser
from src.utils import to_eur

from src.constants.schema_enums.normalized.link import Link as NL
from src.constants.schema_enums.normalized.entity import Entity as NE
from src.constants.value_constants import EntityType

from src.denormalize import list_columns_to_map, fit_dataframe_to_schema

# -

# +

df = pd.read_csv("data/1_extracted/denmark/all.csv", index_col=0)
# -


# +
translations_per_column = {
    "profession": {
        "Anden": "other",
        "Apoteker" :"pharmacist",
        "Behandlerfarmaceut": "treating pharmacist",
        "Læge": "doctor",
        "Sygeplejerske": "nurse",
        "Tandlæge": "dentist",
    }
}

for col in translations_per_column:
    df[col] = df[col].map(lambda x: translations_per_column[col][x])


# +
city_pattern = r"\s?([^\d]+)$"
def extract_city(address: str):
    city_match = re.search(city_pattern, address)
    
    return pd.Series([address[:city_match.start(1)].strip(), city_match.group().strip()])

# -

adress_and_city = df["address"].apply(extract_city)

df["city"] = adress_and_city[1]
df["address"] = adress_and_city[0]

counter = 0
def entity_id(registration_number):
    global counter
    if str(registration_number) != "nan":
        return f"dk_reg_number_{registration_number}"
    else:
        counter += 1
        return f"dk_{counter}"

# +
functions_per_column = {
    "reason": lambda x: re.match(r"Tilknytning: ?(.*)", x)[1],
    "fee": lambda x: float(re.match(r"DKK(.+)", x)[1].replace(",", "")),
    "valid_until": lambda x: date_parser(re.match(r"Gyldig til: (.+)", x)[1]).date(),
    "registration_number": lambda x: entity_id(x)
}

# currency is DKK

for col in functions_per_column:
    df[col] = df[col].map(functions_per_column[col])


# +
from tableschema import Schema
schema = Schema('schemas/normalized/link.json')

list_columns_to_map(df, schema)
# -



# +
from src.utils import reverse_string_dict

#typing: Dict[str, Tuple[Optional[str], Optional[func]]]
link_rename_dict = {
    NL.RECIPIENT_ENTITY_ID: "registration_number",
    NL.CATEGORY: "reason",
    NL.VALUE_TOTAL_AMOUNT: "fee"
}

link_df = df.rename(columns=reverse_string_dict(link_rename_dict))
# -
list_columns_to_map(df, schema)

# +
fill_constant_dict = {
    NL.PUBLICATION_ID: "denmark",
    NL.CURRENCY: "DKK",
    NL.IS_AGGREGATED: False
}

for k, v in fill_constant_dict.items():
    link_df[k] = v
# -

link_df[NL.LINK_ID] = df.apply(lambda x: "denmark-" + str(x.name), axis=1)
link_df[NL.SOURCE_ORGANIZATION_ID] = df['company_name'].map(lambda a: f"dk__{a.replace(' ', '_')}")
link_df[NL.DETAILS] = df["valid_until"].apply(lambda x: f"valid_until: {x}")
link_df[NL.VALUE_TOTAL_AMOUNT_EUR] = link_df[NL.VALUE_TOTAL_AMOUNT].map(lambda a: to_eur(amount=a, currency="DKK"))

link_df = fit_dataframe_to_schema(link_df, schema)


# -
# We also need to create corresponding entities to store the address. We reuse normalized_df

entity_schema = Schema("schemas/normalized/entity.json")



recipient_rename_dict = {
    NE.ENTITY_ID: "registration_number",
    NE.FULL_NAME: "name",
    NE.COUNTRY: "country",
    NE.CITY: "city",
    NE.PERSON_SPECIALTY: "profession",
    NE.ADDRESS: "address"
}

recipient_df = df.rename(columns=reverse_string_dict(recipient_rename_dict))

recipient_df[NE.ENTITY_TYPE] = "unknown"
recipient_df[NE.VALID_UNTIL] = None


recipient_df = fit_dataframe_to_schema(recipient_df, entity_schema)


source_df = fit_dataframe_to_schema(link_df, entity_schema)

source_df[NE.ENTITY_ID] = df['company_name'].map(lambda a: f"dk__{a.replace(' ', '_')}")
source_df[NE.FULL_NAME] = df['company_name']

source_df[NE.COUNTRY]= "Denmark"
source_df[NE.ENTITY_TYPE] = EntityType.INDUSTRY
source_df[NE.IS_PERSON] = False


source_df = fit_dataframe_to_schema(source_df, entity_schema)

all_entities_df = pd.concat([source_df, recipient_df])
all_entities_df.drop_duplicates(inplace=True)

# +

NORMALIZED_DENMARK_PATH = "data/normalized_data/denmark"

try:
    os.makedirs(NORMALIZED_DENMARK_PATH)
except FileExistsError:
    pass

link_df.to_csv(path_or_buf=os.path.join(NORMALIZED_DENMARK_PATH, "link.csv"), index=False)

all_entities_df.to_csv(path_or_buf=os.path.join(NORMALIZED_DENMARK_PATH, "entity.csv"), index=False)
