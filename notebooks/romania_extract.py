# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# RGPD https://www.anm.ro/anunt-important-16-01-2020

import requests
from IPython.core.display import display, HTML
import os
import re
import pandas as pd

if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")


# # Download

# +
def display_text(r):
    display(HTML(r.text))

def url(year=None, kind=None, page=None):
    url = "https://www.anm.ro/sponsorizari/"
    if not year:
        return url
    
    url = url + f"afisare-{year}"
    if not kind:
        return url
    url += "/" + kind
    if not page:
        return url
    return url + f"?page={page}"

def get_years():
    r = requests.get(url())
    years = list()
    for m in re.finditer("/sponsorizari/afisare-(20\d\d)/beneficiari", r.text):
        years.append(int(m.group(1)))
    return years

def get_max_page(year, kind):
    r = requests.get(url(year, kind))
    pages = list()
    for m in re.finditer(f"sponsorizari/afisare-{year}/{kind}\?page=(\d+)", r.text):
        pages.append(int(m.group(1)))

    return max(pages)


# -

dl_folder = "data/process_data/romania/0_downloads"
kinds = ["beneficiari", "sponsori"]

if not os.path.exists(dl_folder):
    os.mkdir(dl_folder)

years = get_years()
#years
for year in years:
    print(year)
    for kind in kinds:
        print("  ", kind)
        for page in range(1, get_max_page(year, kind) + 1):
            file = f"{dl_folder}/romania__{year}__{kind}__{page}.html"
            print("    ", file)
            r = requests.get(url(year, kind, page))
            with open(file, 'w') as f:
                f.write(r.text)

# # Extract

# +
extracted_folder = "data/process_data/romania/1_extracted"

if not os.path.exists(extracted_folder):
    os.mkdir(extracted_folder)


# -

def extract(year, kind):
    df_list = list()
    for page in range(1, get_max_page(year, kind) + 1):
        file = f"{dl_folder}/romania__{year}__{kind}__{page}.html"
        df_list.append(pd.read_html(file)[0])
    df = pd.concat(df_list)
    outfile = f"{extracted_folder}/romania__{year}__{kind}.csv"
    df.to_csv(outfile, index=False)


for year in years:
    for kind in kinds:
        print(year, kind)
        extract(year, kind)


