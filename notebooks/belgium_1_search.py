import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import time
import io
import pandas as pd
import requests
from string import ascii_uppercase
from src.constants.schemas import PROCESS_DIR
from src.pdf.utils import Efpia_Pdf_Parsing_Exception

FOLDER = "belgium"

# Session_id: open a session on your browser, pass the captcha (you have to do a search), then open the debugger, find the session id and replace the value below

session_id = 'AWP_CSESSIONA3150588=A570AA29B311B1F284E48F5E032FABF169F7EA04'

url = 'https://extranet.betransparent.be/uk/index.awp'
headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Sec-GPC': '1',
        'Origin': 'https://extranet.betransparent.be',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://extranet.betransparent.be/uk/index.awp',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Cookie': session_id
    }

hcp_dict_ids_df = None
hcp_dict_ids_fp = os.path.join(PROCESS_DIR, FOLDER, "hcp_dict_ids.csv")
hco_dict_ids_df = None
hco_dict_ids_fp = os.path.join(PROCESS_DIR, FOLDER, "hco_dict_ids.csv")
po_dict_ids_df = None
po_dict_ids_fp = os.path.join(PROCESS_DIR, FOLDER, "po_dict_ids.csv")

html_folder = os.path.join(PROCESS_DIR, FOLDER, "html")
start = time.time()
i = 0

def get_pa1(year, row, row_type):
    if row_type == "po":
        return f"{year}_PO_{row['company_id']}"
    elif row_type == "hcp":
        return f"{year}_HCW_{row['hcp_id']}"
    else:
        return f"{year}_HCI_{row['company_id'].rjust(10, '0')}"


def get_html_fp(year, row, row_type):
    if row_type == "po":
        return os.path.join(html_folder, f"po__{row['company_id']}__{year}.html")
    elif row_type == "hcp":
        return os.path.join(html_folder, f"hcp__{row['hcp_id']}__{year}.html")
    else:
        return os.path.join(html_folder, f"hco__{row['company_id'].rjust(10, '0')}__{year}.html")

def parse_row(row, last_file, row_type):
    no_data = 0
    for year in [2017,2018,2019]:
        pa1 = get_pa1(year, row, row_type)
        html_fp = get_html_fp(year, row, row_type)

        raw_data = f"WD_ACTION_=AJAXEXECUTE&EXECUTEPROC=index.FileToShow&WD_CONTEXTE_=A9&PA1={pa1}&PA2=JOHN%20DOE&PA3="
        request(raw_data)

        raw_data2 = 'WD_JSON_PROPRIETE_=%7B%22m_oProprietesSecurisees%22%3A%7B%7D%7D&WD_BUTTON_CLICK_=A27&WD_ACTION_=&A18=1&A1=DOE&A23=3&A2=1&A2_DEB=1&_A2_OCC=0&A20='
        r = request(raw_data2)
        if "Premiums and benefits" in r.text:  # we write the file only if there are results
            with open(html_fp, 'w') as f:
                f.write(r.text)
            logging.info(f"{pa1}: html saved")
            no_data = 0

        else:
            logging.info(f"{pa1}: no data")
            no_data +=1

        if no_data > 50:
            raise Exception("no data since 50 searches, the session must have expired")


def scrappe_all_ids():
    global hcp_dict_ids_df
    global hco_dict_ids_df
    global po_dict_ids_df

    # POs
    po_dict_ids_df = pd.read_csv(po_dict_ids_fp, dtype="str")
    po_dict_ids_df.columns = ["hcp_id","company_id","col3","full_name","imani_id","postal_code"]

    total_po = po_dict_ids_df.shape[0]

    po_dict_ids_df.sort_values(by=['company_id'], inplace=True)
    po_dict_ids_df.reset_index(inplace=True)

    existing_files = [x for x in os.listdir(html_folder) if x.endswith(".html") and "po__" in x ]
    existing_files.sort()
    if len(existing_files) > 0:
        last_file = existing_files[-1]
    else:
        last_file = ""

    logging.info(f"{total_po} POs")
    for index, row in po_dict_ids_df.iterrows():
        html_fp = get_html_fp("2019", row, "po")
        if last_file[0:-4] < html_fp.split("/")[-1]:
            parse_row(row, last_file, "po")
            if index % 10 == 0:
                secs = time.time() - start
                logging.info(f"{index} / {total_po}: {round(index / secs, 2)} IDs per sec")

    logging.info("all POs done!!!")



    # hcos
    hco_dict_ids_df = pd.read_csv(hco_dict_ids_fp, dtype="str")
    hco_dict_ids_df.columns = ["hcp_id","company_id","col3","full_name","imani_id","postal_code"]

    total_hco = hco_dict_ids_df.shape[0]

    hco_dict_ids_df.sort_values(by=['company_id'], inplace=True)
    hco_dict_ids_df.reset_index(inplace=True)

    existing_files = [x for x in os.listdir(html_folder) if x.endswith(".html")]
    existing_files.sort()
    if len(existing_files) > 0:
        last_file = existing_files[-1]
    else:
        last_file = ""

    logging.info(f"{total_hco} HCOs")
    for index, row in hco_dict_ids_df.iterrows():
        html_fp = get_html_fp("2019", row, "hco")
        if last_file[0:-4] < html_fp.split("/")[-1]:
            parse_row(row, last_file, "hco")
            if index % 10 == 0:
                secs = time.time() - start
                logging.info(f"{index} / {total_hco}: {round(index / secs, 2)} IDs per sec")

    logging.info("all HCOs done!!!")


    #hcps
    hcp_dict_ids_df = pd.read_csv(hcp_dict_ids_fp, dtype="str")
    hcp_dict_ids_df.columns = ["hcp_id","company_id","col3","full_name","imani_id","postal_code"]

    total_hcp = hcp_dict_ids_df.shape[0]

    hcp_dict_ids_df.sort_values(by=['hcp_id'], inplace=True)
    hcp_dict_ids_df.reset_index(inplace=True)

    existing_files = os.listdir(html_folder)
    existing_files.sort()
    last_file = existing_files[-1]
    already_done = len(existing_files)

    last_index_already_done = 0
    logging.info(f"{total_hcp} HCPs")
    for index, row in hcp_dict_ids_df.iterrows():
        html_fp = get_html_fp("2019", row, "hcp")
        if last_file[0:-4] < html_fp.split("/")[-1]:
            parse_row(row, last_file, "hcp")
            if index % 10 == 0:
                secs = time.time() - start
                logging.info(f"{index} / {total_hcp}: {round(3600 * (index - last_index_already_done) / secs, 2)} IDs per hour")
        else:
            last_index_already_done = index


NO_RESULT=0
RESULTS=1
TOO_MANY_RESULTS=2


def request(raw_data, retry=0):
    try:
        r = requests.post(url=url, headers=headers, data=raw_data, timeout=10)
        return r
    except (requests.Timeout, requests.exceptions.ConnectionError):
        # wait 30 sec and retry
        logging.info(f"there was a timeout, waiting {(retry+1)*60} seconds and trying again")
        time.sleep((retry+1)*60)
        return request(raw_data, retry=retry+1)
        pass


def hcp_search(to_search):

    global hcp_dict_ids_df
    global i
    global start

    # search given string
    raw_data = f"WD_ACTION_=AJAXEXECUTE&EXECUTEPROC=index.Search&WD_CONTEXTE_=A1&PA1=1&PA2={to_search.ljust(3, '*')}".encode("utf-8")

    r = request(raw_data)
    i+=1

    # each 50 search we save
    if i % 50 == 0:
        hcp_dict_ids_df.to_csv(hcp_dict_ids_fp, index=False, float_format="%.0f")
        secs = time.time() - start
        logging.info(f"{i}: {round(i / secs, 2)} searches per sec")

    if len(r.text) == 0:
        return NO_RESULT
    try:
        df = pd.read_csv(io.StringIO(r.text), sep="\t", quotechar="'", header=None)
        df.columns = hcp_dict_ids_df.columns
        if df.shape[0] >= 10:
            return TOO_MANY_RESULTS

        df.columns = hcp_dict_ids_df.columns
        hcp_dict_ids_df = hcp_dict_ids_df.append(df, ignore_index=True)
        return RESULTS
    except ValueError:
        logging.error(f"there was a \\n in the middle of a name in the returned tsv for searched string {to_search}, this stops this branch!")


def get_hcp_ids_by_dictionnary(prefix=""):
    global i
    global hcp_dict_ids_df
    if hcp_dict_ids_df is None:
        hcp_dict_ids_df = pd.read_csv(hcp_dict_ids_fp)

    if hcp_dict_ids_df.shape[0] > 0:
        last_name = hcp_dict_ids_df.iloc[hcp_dict_ids_df.index[-1], 3]
    else:
        last_name = ""

    for letter in ascii_uppercase:
        to_search=f"{prefix}{letter}"

        if to_search < last_name[0:len(to_search)]:
            # we can skip this entire letter
            logging.info(f"skipping {to_search}")
            continue

        if to_search == last_name[0:len(to_search)]:
            logging.info(f"{to_search}: approaching, getting a level deeper")
            get_hcp_ids_by_dictionnary(to_search.replace("*", ''))

        elif to_search > last_name[0:len(to_search)]:
            sr = hcp_search(to_search)
            logging.info(f"{' ' * len(prefix)}{to_search}: {sr}")
            if sr == TOO_MANY_RESULTS:
                get_hcp_ids_by_dictionnary(to_search.replace("*", ''))

        else:
            get_hcp_ids_by_dictionnary(to_search.replace("*", ''))

    hcp_dict_ids_df.to_csv(hcp_dict_ids_fp, index=False, float_format="%.0f")


def get_hco_ids_by_dictionnary(last_searched=""):
    global i
    global hco_dict_ids_df
    if hco_dict_ids_df is None:
        hco_dict_ids_df = pd.read_csv(hco_dict_ids_fp)

    for a in ascii_uppercase:
        for b in ascii_uppercase:
            for c in ascii_uppercase:
                to_search = f"{a}{b}{c}"
                if to_search > last_searched:
                    raw_data = f"WD_ACTION_=AJAXEXECUTE&EXECUTEPROC=index.Search&WD_CONTEXTE_=A1&PA1=2&PA2={to_search}".encode("utf-8")
                    r = request(raw_data)

                    i += 1

                    # each 50 search we save
                    if i % 50 == 0:
                        hco_dict_ids_df.to_csv(hco_dict_ids_fp, index=False, float_format="%.0f")
                        secs = time.time() - start
                        logging.info(f"{i}: {round(i / secs, 2)} searches per sec")

                    if len(r.text) > 0:
                        try:
                            df = pd.read_csv(io.StringIO(r.text), sep="\t", header=None)
                            df.columns = hco_dict_ids_df.columns
                            hco_dict_ids_df = hco_dict_ids_df.append(df, ignore_index=True)
                            logging.info(f"{to_search}: {df.shape[0]}")
                        except ValueError:
                            logging.error(
                                f"{to_search}: there was a \\n in the middle of a name in the returned tsv for searched string {to_search}, this stops this branch!")
                    else:
                        logging.info(f"{to_search}: 0")

    hco_dict_ids_df.drop_duplicates(inplace=True)
    hco_dict_ids_df.to_csv(hco_dict_ids_fp, index=False, float_format="%.0f")
    logging.info("finished")


def get_po_ids_by_dictionnary(last_searched=""):
    global i
    global po_dict_ids_df
    if po_dict_ids_df is None:
        po_dict_ids_df = pd.read_csv(po_dict_ids_fp)

    for a in ascii_uppercase:
        for b in ascii_uppercase:
            for c in ascii_uppercase:
                to_search = f"{a}{b}{c}"
                if to_search > last_searched:
                    raw_data = f"WD_ACTION_=AJAXEXECUTE&EXECUTEPROC=index.Search&WD_CONTEXTE_=A1&PA1=3&PA2={to_search}".encode("utf-8")
                    r = request(raw_data)
                    i += 1
                    # each 50 search we save
                    if i % 50 == 0:
                        po_dict_ids_df.to_csv(po_dict_ids_fp, index=False, float_format="%.0f")
                        secs = time.time() - start
                        logging.info(f"{i}: {round(i / secs, 2)} searches per sec")
                    if len(r.text) > 0:
                        try:
                            df = pd.read_csv(io.StringIO(r.text), sep="\t", header=None)
                            df.columns = po_dict_ids_df.columns
                            po_dict_ids_df = po_dict_ids_df.append(df, ignore_index=True)
                            logging.info(f"{to_search}: {df.shape[0]}")
                        except ValueError:
                            logging.error(
                                f"{to_search}: there was a \\n in the middle of a name in the returned tsv for searched string {to_search}, this stops this branch!")
                    else:
                        logging.info(f"{to_search}: 0")

    po_dict_ids_df.drop_duplicates(inplace=True)
    po_dict_ids_df.to_csv(po_dict_ids_fp, index=False, float_format="%.0f")
    logging.info("finished")

if __name__ == '__main__':
    #get_po_ids_by_dictionnary()
    scrappe_all_ids()
