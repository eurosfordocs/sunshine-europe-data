# # Ireland part 1
# This steps downloads html files from the irish website. It uses Selenium because there was some sessions params that were not easy to mimic. 
#
# # to download a new year
# Just run this notebook, you may have to donwload a new selenium chrome driver. The fresh html will replace the old ones for years y-1 and y-2. Once finished, check that you only have old files for years y-3 or older. If you have some, it is likely because some company changed names. I would then rename the company to the newer name in the older files to have only one name. If you do that you have to reflect thoses changes in the manual company entity deduplication sheet, but there should be very few changes each year. 

import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from src.pdf.utils import Efpia_Pdf_Parsing_Exception, diff_time
import time
from src.constants.schemas import PROCESS_DIR
logging.getLogger().setLevel(logging.INFO)

FOLDER = "ireland"
downloads_dir = os.path.join(PROCESS_DIR, FOLDER, "0_downloads")

driver_path = "/Users/luc/Downloads/chromedriver"


def load_first_url():
    driver.get("https://www.transferofvalue.ie")


def company_select():
    return Select(driver.find_element_by_name("ctl00$BodyContent$_company"))


def load_company_page(company_value):
    load_first_url()
    company_select().select_by_value(company_value)
    time.sleep(0.2)


# +
# close browser at the end of the script
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)

driver = webdriver.Chrome(driver_path, options=chrome_options)
driver.implicitly_wait(5)

start_time = time.time()
load_first_url()
i = 0

companies = [{
    "text": o.text.lower().replace(" ", "-"),
    "value": o.get_attribute('value')
} for o in company_select().options]

for company in companies:
    if int(company["value"]) >= 0:
        load_company_page(company["value"])
        year_select = driver.find_element_by_class_name("selectyear")
        years = [h.text for h in year_select.find_elements_by_tag_name("h2")]
        company_url = driver.current_url
        for year in years:
            url = f"{company_url}/{year}"
            driver.get(url)
            file_name = f"ireland__{company['text']}__{year}.html"
            file_path = os.path.join(downloads_dir, file_name)
            table = driver.find_elements_by_class_name("table-responsive")[2]
            html = table.get_attribute('innerHTML')
            with open(file_path, "w+") as file:
                file.write(html)
                i += 1
                time.sleep(1)
                if i % 10 == 0:
                    logging.info(f"So far, scrapped {i} tables in {diff_time(start_time)}")

logging.info(f"In total, parsed {i} files in {diff_time(start_time)}")
# -


