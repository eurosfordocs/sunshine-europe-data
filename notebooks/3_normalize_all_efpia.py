import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
from src.normalize import normalise_folder
logging.getLogger().setLevel(logging.INFO)


# ### Normalizing from EFPIA folders: 
# Most countries have the default setup: Italy, Sweden, Switzerland, Germany
# * Belgium, UK and Ireland have a single publication_id, as they are from a centralized platform
# * Spain always have an ID, which is the partially obfuscated DNI (hcp) or CIF (hco)
# * For all non pdf countries, we need to generate the publications

def all_pdf():
    normalise_folder("italy")
    normalise_folder("spain", hcp_directory_id="spain_dni_cif_pharmaindustria", hco_directory_id="spain_dni_cif_pharmaindustria")
    normalise_folder("sweden")
    normalise_folder("switzerland")
    normalise_folder("germany")

if __name__ == '__main__':

    #all_pdf()
    #normalise_folder("uk", generate_source_organization=True, country='UK', publication_id= "uk")
    #normalise_folder("ireland", generate_source_organization=True, country='Ireland', publication_id="ireland")
    normalise_folder("belgium", hcp_directory_id="belgium_inami", hco_directory_id="belgium_company_id", publication_id="belgium")


