import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import logging
from src.constants.schemas import PROCESS_DIR
from src.pdf import efpia_pdf_parser
from src.pdf.utils import Efpia_Pdf_Parsing_Exception
from notebooks.bayer_janssen_2_harmonize import harmonize_all
from notebooks.spain_pfizer_0_download import parse_jsons
import shutil

# ## parse_folder
# Extract pdf data from the excel files

def parse_folder(folder="germany", delete_existing_files=False):

    dl_dir = os.path.join(PROCESS_DIR, folder, "0_downloads")

    if "2_harmonized" not in os.listdir(os.path.join(PROCESS_DIR, folder)):
        os.mkdir(os.path.join(PROCESS_DIR, folder, "2_harmonized"))

    harmonized_dir = os.path.join(PROCESS_DIR, folder, "2_harmonized")

    if delete_existing_files:
        shutil.rmtree(harmonized_dir)
        os.mkdir(harmonized_dir)

    if "0_downloads" not in os.listdir(os.path.join(PROCESS_DIR, folder)):
        raise Efpia_Pdf_Parsing_Exception(f"{dl_dir} doest not exist, check where your run this from")

    efpia_pdf_parser.parse_efpia_pdf_folder(dl_dir, harmonized_dir)


# ## file_zoom
# parse a pdf file with all possible readers. Has much more detailed logs that the parse folder method. 

def file_zoom(pdf_file, currency="EUR"):
    folder = pdf_file.split("__")[0]

    dl_dir = os.path.join(PROCESS_DIR, folder, "0_downloads")
    harmonized_dir = os.path.join(PROCESS_DIR, folder, "2_harmonized")
    harmonized_csv_fn = pdf_file.replace(".pdf", ".csv")
    if harmonized_csv_fn in os.listdir(harmonized_dir):
        os.remove(os.path.join(harmonized_dir, harmonized_csv_fn))

    logging.getLogger().setLevel(logging.DEBUG)
    efpia_pdf_parser.parse_efpia_pdf(pdf_file, dl_dir, harmonized_dir, currency= currency)


def redo_all(delete_files=True):
    parse_folder("switzerland", delete_existing_files=delete_files)
    parse_folder("germany",     delete_existing_files=delete_files)
    parse_folder("sweden",      delete_existing_files=delete_files)
    parse_folder("spain",       delete_existing_files=delete_files)
    parse_folder("italy",       delete_existing_files=delete_files)


    if delete_files:
        # if we deleted the files, we are forced to redo also:

        #1) step 2 of bayer and janssen specific processes
        harmonize_all()

        #2) parsing pfizer spanish jsons
        parse_jsons()


if __name__ == '__main__':
    #redo_all(False)

    parse_folder("spain")

    #parse_folder("sweden", delete_existing_files=False)
    # parse_folder("switzerland", delete_existing_files=True)
    #parse_folder("switzerland")
    #parse_folder("italy", delete_existing_files=True)

    # parse_folder("sweden")
    #



# +
#file_zoom("sweden__gilead__2019.pdf")

