import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import time
import io
import re
from src import utils
from src.pdf import utils as pdf_utils
import numpy as np
import pandas as pd


from src.denormalize import fit_dataframe_to_schema

from tableschema import Schema
link_schema = Schema('schemas/efpia/link.json')

from src.constants.schema_enums.normalized.link import Link as NL
from src.constants.schema_enums.normalized.entity import Entity as NE
from src.constants.value_constants import *
from src.pdf.utils import Efpia_Pdf_Parsing_Exception

FOLDER = "belgium"
from src.constants.schemas import PROCESS_DIR
from src.constants.schemas import NORMALIZED_DIR
html_folder = os.path.join(PROCESS_DIR, FOLDER, "html")

po_dict_ids_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "po_dict_ids.csv"), dtype = str)
po_dict_ids_df.columns = ["hcp_id", "company_id", "col3", "full_name", "imani_id", "postal_code"]
po_dict_ids_df.set_index("company_id", inplace=True)
po_dict_ids_df = po_dict_ids_df[~po_dict_ids_df.index.duplicated(keep='first')]

company_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "company_ids.csv"), dtype = str)
company_df = company_df[~company_df.company_id.duplicated(keep='first')]

start = time.time()

def get_company_id(company_name):
    matches = company_df[company_df["full_name"].str.contains(company_name, regex=False)]
    if len(matches) == 1:
        return matches.iat[0,2]
    elif len(matches) == 0:
        logging.error(f"{company_name}")
    else:
        logging.debug(f"for company name {company_name}, we have {len(matches)} matches. Taking the first one {matches.iat[0,2]}")
        return matches.iat[0,2]


def to_company_id(company_name):
    return f'belgium__{company_name.replace(" ", "-").lower()}'


if __name__ == '__main__':

    html_files = [x for x in os.listdir(html_folder) if x.endswith(".html") and "po__" in x]
    html_files.sort()

    link_dfs = []
    recipients = []
    i = 0
    for file in html_files:
        i += 1
        recipient_type = file.split("__")[0]
        recipient_id = file.split("__")[1]
        recipient_entity_id = f"belgium__po__BE{recipient_id}"
        year = file.split("__")[2][0:4]


        #RECIPIENT
        r = po_dict_ids_df.loc[recipient_id]
        recipient = {
            NE.FULL_NAME: r["full_name"],
            NE.COUNTRY: "Belgium",
            NE.DIRECTORY_ID: "belgium_company_id",
            NE.DIRECTORY_ENTITY_ID: f"BE{recipient_id}",
            NE.ENTITY_ID: recipient_entity_id,
            NE.ENTITY_TYPE: EntityType.PO,
            NE.IS_PERSON: False
        }
        recipients.append(recipient)

        # reading the html table

        df = pd.read_html(os.path.join(html_folder, file))[0]
        #drop 2 first rows, they are headers
        df = df.iloc[2:]
        df = df.replace(np.nan, "")
        #there are nbsp chars that come up as \xa0
        df = df.applymap(lambda x : str(x).replace(u'\xa0', u' '))

        df.columns = ["source",
                      Category.CONSULTING_FEE,
                      Category.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
                      Category.FINANCIAL_SUPPORT,
                      Category.OTHER_SUPPORT]

        #SOURCE
        # we simply check that all the source companies can be found in the company list by name.
        # If so, we will put source_entity_id = lower(column1) and we are good to go.
        df["source"].map(get_company_id)

        #LINKS
        link_df = pd.DataFrame(columns=[x.value for x in list(NL)])

        # first we make 4 DFs - 1 for each of the value columns. Then we concat them, remove lines without amounts and fill the other values.

        #consulting_fee
        cf_df = pd.DataFrame(columns=[x.value for x in list(NL)])
        cf_df[NL.VALUE_TOTAL_AMOUNT] = df[Category.CONSULTING_FEE].map(pdf_utils.get_number)
        cf_df[NL.SOURCE_ORGANIZATION_ID] = df["source"].map(to_company_id)
        cf_df[NL.CATEGORY] = Category.CONSULTING_FEE
        cf_df[NL.TYPE] = Type.CASH

        #consulting_expenses
        ce_df = pd.DataFrame(columns=[x.value for x in list(NL)])
        ce_df[NL.VALUE_TOTAL_AMOUNT] = df[Category.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES].map(pdf_utils.get_number)
        ce_df[NL.SOURCE_ORGANIZATION_ID] = df["source"].map(to_company_id)
        ce_df[NL.CATEGORY] = Category.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES
        ce_df[NL.TYPE] = Type.CASH

        # consulting_expenses
        fs_df = pd.DataFrame(columns=[x.value for x in list(NL)])
        fs_df[NL.VALUE_TOTAL_AMOUNT] = df[Category.FINANCIAL_SUPPORT].map(pdf_utils.get_number)
        fs_df[NL.SOURCE_ORGANIZATION_ID] = df["source"].map(to_company_id)
        fs_df[NL.CATEGORY] = Category.FINANCIAL_SUPPORT
        fs_df[NL.TYPE] = Type.CASH

        # consulting_expenses
        os_df = pd.DataFrame(columns=[x.value for x in list(NL)])
        os_df[NL.VALUE_TOTAL_AMOUNT] = df[Category.OTHER_SUPPORT].map(pdf_utils.get_number)
        os_df[NL.SOURCE_ORGANIZATION_ID] = df["source"].map(to_company_id)
        os_df[NL.CATEGORY] = Category.OTHER_SUPPORT
        os_df[NL.TYPE] = Type.IN_KIND

        #concat and remove empty links
        link_df = pd.concat([cf_df, ce_df, fs_df, os_df])
        link_df = link_df[~pd.isna(link_df[NL.VALUE_TOTAL_AMOUNT])]

        #fill common columns
        link_df[NL.PUBLICATION_ID] = "belgium"
        link_df[NL.RECIPIENT_ENTITY_ID] = recipient_entity_id
        link_df[NL.RECIPIENT_ENTITY_TYPE] = EntityType.PO
        link_df[NL.YEAR] = year
        link_df[NL.VALUE_TOTAL_AMOUNT_EUR] = link_df[NL.VALUE_TOTAL_AMOUNT]
        link_df[NL.CURRENCY] = "EUR"
        link_df[NL.IS_AGGREGATED] = True
        link_df[NL.NUMBER_OF_AGGREGATED_RECIPIENTS] = 1


        link_dfs.append(link_df)
        utils.print_progress_bar(i, len(html_files), "Harmonize Belgium POs")


    all_link_df = pd.concat(link_dfs)
    all_link_df.reset_index(inplace=True, drop=True)
    all_link_df[NL.LINK_ID] = all_link_df.index.map(lambda x : f"belgium__po__{x}")
    all_link_df.to_csv(os.path.join(NORMALIZED_DIR, FOLDER, "link__po.csv"), index=False)

    recipient_df = pd.DataFrame(data=recipients, columns=[x.value for x in list(NE)])

    recipient_df.drop_duplicates(subset=[NE.ENTITY_ID], keep='first', inplace=True)
    recipient_df.to_csv(os.path.join(NORMALIZED_DIR, FOLDER, "entity__po.csv"), index=False)




