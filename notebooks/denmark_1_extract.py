# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Scraping Denmark
#
#

# ## Imports

import os
from pathlib import Path

import pandas as pd
from bs4 import BeautifulSoup

# Lets get the downloaded data.

dir_path = "data/0_downloads/denmark"
filenames = [dir_path + "/" + filename for filename in os.listdir(dir_path)]


# Each entry of the data is separated into two rows.
#
# ```
# <table id="report">
# <thead>
# ...
# </thead>
# <tbody>
#   <tr> <!-- First row, basic info -->
#     <td> Abdul Azim Azizi </td> <!-- Complete Name -->
#     <td> L&aelig;ge </td> <!-- Profesion -->
#     <td> <!-- Address -->
#             Azizi<br>
#             Kommenhaven 11   <br>
#             2730 Herlev
#     </td>
#     <td> 010NK </td> <!-- Registration number -->
#   </tr>
#   <tr> <!-- Complementary row -->
#     <td>
#       <div>
#         <span>Virksomhed</span><span>Honorar</span> <!-- Headers -->
#         <table id="reportInner"> <!-- New table which details fees -->
#           <tr>
#             <td >Boehringer Ingelheim Danmark A/S</td> <!-- Name of the company -->
#             <td>
#               <div>2019</div> <!-- Year -->
#               <div>DKK0.00</div><br> <!-- Sum received -->
#               <div>2018</div>
#               <div>DKK6,000.00</div><br>
#               <div>2017</div>
#               <div>DKK0.00</div><br>
#               </td>
#           </tr>
#           <tr >
#             <td><hr>
#               <ul >
#                 <li>Tilknytning: Forskning</li> <!-- "affiliation", ie type of service. Here it means "Research" -->
#                 <li>Gyldig til: 31. Dec 2019</li> <!-- Valid until -->
#               </ul><hr>
#             </td>
#           </tr>
#         </table>        
#         <table id="reportInner"> <!-- Same details, but for another enterprise -->
#           <tr>
#             <td>Lundbeck Pharma A/S</td>
#             <td>
#               <div >2019</div>
#               <div>DKK3,000.00</div><br>
#               <div>2018</div>
#               <div>DKK0.00</div><br>
#               <div>2017</div>
#               <div>DKK0.00</div><br>
#             </td>
#           </tr>
#           <tr>
#             <td><hr>
#               <ul >
#                 <li>Tilknytning: Fokusgruppeinterview</li> <!-- Affiliation: Focus group interview -->
#                 <li>Gyldig til: 12. Nov 2019</li>
#               </ul>
#               <hr>
#             </td>
#           </tr>
#         </table>
#       </div>
#     </td>
#   </tr>
# </tbody>
# </table>
# ```
# We have to parse and transform this structure.
#
# Since it's not that much the job of the scrapper to interpret the data, I can either :
# - create a JSON like structure in Python which mimics the HTML one
# - already create a row like, flat structure which will probably the output anyway
#
# I'll take the second option for now **but** since I don't know which one I'll need later, the first option is more versatile, and can be easily convert to a flat output.





def extract_denmark_data_from_html(html):
    output_rows = []
    
    primary_row = True # If we are on the first rows with names, or second with fees
    name = ""
    profession = ""
    address = ""
    registration_number = ""
    company_name = ""
    fee = ""
    year = ""
    reason = ""
    valid_until = ""
    
    soup = BeautifulSoup(html, features="html.parser")
    
    for row in soup.tbody.find_all("tr", recursive=False):
        if primary_row:
            data = row.find_all("td")
            name = data[0].get_text(strip=True)
            profession = data[1].get_text(strip=True)
            address = data[2].get_text(strip=True)
            registration_number = data[3].get_text(strip=True)

            primary_row = False
        else:

            # lets iterate over companies

            for company_table in row.find_all(id="reportInner"):

                company_name = company_table.find("td").get_text(strip=True)

                company_table_li = company_table.find_all("li")
                reason = company_table_li[0].get_text(strip=True)
                valid_until = company_table_li[1].get_text(strip=True)

                on_price = False

                for company_div in company_table.tr.find_all("td", recursive=False)[1].find_all("div"):

                    if on_price:
                        fee = company_div.get_text(strip=True)
                        output_rows.append(
                            {"name": name,
                             "profession": profession,
                             "address": address,
                             "registration_number": registration_number,
                             "company_name": company_name,
                             "year": year,
                             "fee": fee,
                             "reason": reason,
                             "valid_until": valid_until
                            })

                        on_price=False
                    else: 
                        year = company_div.get_text(strip=True)
                        on_price=True


            primary_row = True
    
    return output_rows


# +
aggregated_data = []

for filename in filenames:
    with open(filename, "r", encoding="utf-8") as f:
        try:
            html = f.read()
            rows = extract_denmark_data_from_html(html)
        except Exception as e:
            print(str(e) + "with file " + filename)
        aggregated_data.extend(rows)


# -

# At this point we can check that our data is correct.

df = pd.DataFrame(aggregated_data)
df.head(20)

print(f"Denmark has {df.size} entries")
uniq_names = df["name"].unique()
uniq_reg = df["registration_number"].unique()
uniq_companies = df["company_name"].unique()
print(f"There is {len(uniq_names)} unique names")
print(f"There is {len(uniq_reg)} unique registration_number")
print(f"There is {len(uniq_companies)} unique companies")

# +
extracted_path = "data/1_extracted/denmark/all.csv"

try:
    os.makedirs(Path(extracted_path).parents[0])
except FileExistsError:
    pass

df.to_csv(extracted_path)
print(f"extracted data saved to {extracted_path}")
# -


