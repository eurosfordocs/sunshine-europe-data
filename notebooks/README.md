# Jupyter Notebooks

This folder contains python exploratory notebooks 

Notebooks to ignore can be stored in `ignore` folder.

## Notebooks naming

Notebook names follow the following convention `COUNTRY_STEP`, for example `denmark_0_download`, with `STEP` in `0_download`, `1_extract`, `2_harmonize`.

## Relative import 

Add the `src` to the *PATH*:

```python
import os

os.chdir('..')
```

## Plugin JupyTEXT

**Update**: Here a nice [article](https://towardsdatascience.com/jupyter-notebooks-in-the-ide-visual-studio-code-versus-pycharm-5e72218eb3e8) from the maker of Jupytext which shows how and why you would use Jupytext, your classic Python IDE and/or the Jupyter Notebook interface.

**Note** (Julen): 
I currently don't need to launch jupyter with `jupyter.sh` and specifying the file conf. Indeed, as specified in the `README` of jupytext
> In most cases, Jupytext’s contents manager is activated automatically by Jupytext’s server extension.
So you might not need to do the following.

We use [JupyTEXT](https://github.com/mwouts/jupytext) plugin to version notebooks in `.py` and ignore `.ipynb` files, which contain data and are make it difficult to track changes with git. 

Commands to use it 

    source venv/bin/activate
    cd notebooks
    ./jupyter.sh

Open indistinctively `.ipynb` or `.py` paired files.

### Create a new file

From the Notebook editor (`jupyter notebook` or `jupyter lab`), you can *Save as...* and then change the extension to `*.py`. 
It generates a new file with an header like so:

```python
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---
```


### Help

If you modify one of the 2 files `.ipynb` or `.py` without using JupyTEXT, they are not compatible anymore. 

*Fix* : Delete (or move elswhere as a backup) one of the 2 files.

## Jupyter lab

I would recommend to use `jupyter lab` instead of `jupyter notebook` because it got now more features and is more convenient that the former one. For example *shortcuts* are included and *auto-completion*.

Just `pip install jupyter lab` to install it. You might want to run a `jupyter lab build` after installing *jupytext*.

To open a `*.py` as notebook, just *right click* on the file and *Open with Notebook*.
