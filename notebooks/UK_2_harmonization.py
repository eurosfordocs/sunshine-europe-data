# +
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import pandas as pd
import json
from tableschema import Schema

from src.denormalize import fit_dataframe_to_schema
from src.constants.schemas import PROCESS_DIR
link_schema = Schema('schemas/efpia/link.json')


# -

def harmonize_uk(year: int):
    xls_fp = f'{PROCESS_DIR}/uk/0_downloads/{year}/ABPI Full Report {year}.xlsx'
    hco_df = pd.read_excel(xls_fp, sheet_name= 0, engine="openpyxl")
    hcp_df = pd.read_excel(xls_fp, sheet_name= 1, engine="openpyxl")
    aggregated_df = pd.read_excel(xls_fp, sheet_name= 2, engine="openpyxl")

    #Drop first row because empty
    hco_df.columns = hco_df.iloc[0]
    hco_df.columns = hco_df.columns.fillna('Total')
    hco_df = hco_df.drop(0)

    hcp_df.columns = hcp_df.iloc[0]
    hcp_df.columns = hcp_df.columns.fillna('Total')
    hcp_df = hcp_df.drop(0)

    aggregated_df.columns = aggregated_df.iloc[0]
    aggregated_df.columns = aggregated_df.columns.fillna('Total')
    aggregated_df = aggregated_df.drop(0)

    #Set a unique identifier for each row
    hcp_df = hcp_df.reset_index()
    hcp_df['hcp_identifier'] = 'HCP'
    hcp_df['num'] = hcp_df['index'].astype(str)
    hcp_df['id'] = hcp_df['hcp_identifier'] + '-' + hcp_df['num']

    hco_df = hco_df.reset_index()
    hco_df['hco_identifier'] = 'HCO'
    hco_df['num'] = hco_df['index'].astype(str)
    hco_df['id'] = hco_df['hco_identifier'] + '-' + hco_df['num']


    # Import columns from the efpia schema
    schema = open('schemas/efpia/link.json')
    data = json.load(schema)
    columns = [data['fields'][k]['name'] for k in range(len(data['fields']))]

    # Create empty dataframes with the right column names, as a template that we will fill
    df = pd.DataFrame(columns=columns)
    hcp_df_harmonized = df.copy()
    hco_df_harmonized = df.copy()
    aggregated_df_rd_harmonized = df.copy()


    hcp_df_harmonized['link_publication_identifier'] = hcp_df['id']
    hcp_df_harmonized['publication_year'] = hcp_df['Year of Disclosure']
    hcp_df_harmonized['source_organization_full_name'] = hcp_df['Pharma Company Name']
    hcp_df_harmonized['disclosure_type'] = "individual"
    hcp_df_harmonized['recipient_type'] = 'HCP'
    hcp_df_harmonized['recipient_full_name'] = hcp_df['First Name'] + ' ' + hcp_df['Last Name']
    hcp_df_harmonized['recipient_location'] = hcp_df['Location']
    hcp_df_harmonized['recipient_address'] = hcp_df['Address Line 1'] + ' ' + hcp_df['Address Line 2'] + ' ' + hcp_df['Postcode']
    hcp_df_harmonized['recipient_city'] = hcp_df['City']
    hcp_df_harmonized['recipient_country'] = hcp_df['Country']
    hcp_df_harmonized['recipient_person_title'] = hcp_df['Title']
    hcp_df_harmonized['recipient_person_first_name'] = hcp_df['First Name']
    hcp_df_harmonized['recipient_person_last_name'] = hcp_df['Last Name']
    hcp_df_harmonized['recipient_person_profession'] = hcp_df['Role']
    hcp_df_harmonized['recipient_person_specialty'] = hcp_df['Speciality']
    #hcp_df_harmonized['recipient_person_role'] = 
    hcp_df_harmonized['recipient_person_organization'] = hcp_df['Institution Name']
    #hcp_df_harmonized['recipient_identifier'] = 
    #hcp_df_harmonized['amount__donation_and_grant'] = 
    #hcp_df_harmonized['amount__event_sponsorship'] = 
    hcp_df_harmonized['amount__registration_fees'] = hcp_df['Registration Fees']
    hcp_df_harmonized['amount__travel_accommodation'] = hcp_df['Travel & Accommodation']
    hcp_df_harmonized['amount__service_and_consultancy_fees'] = hcp_df['Fees']
    hcp_df_harmonized['amount__service_and_consultancy_related_expenses'] = hcp_df['Related expenses agreed in the fee for service or consultancy contract']
    #hcp_df_harmonized['amount__joint_working_link'] = 
    #hcp_df_harmonized['detail__joint_working_link'] = 
    hcp_df_harmonized['amount__total'] = hcp_df['Total']

    # b) HCO

    hco_df_harmonized['link_publication_identifier'] = hco_df['id']
    hco_df_harmonized['publication_year'] = hco_df['Year of Disclosure']
    hco_df_harmonized['source_organization_full_name'] = hco_df['Pharma Company Name']
    hco_df_harmonized['disclosure_type'] = "individual"
    hco_df_harmonized['recipient_type'] = 'HCO'
    hco_df_harmonized['recipient_full_name'] = hco_df['Institution Name']
    hco_df_harmonized['recipient_location'] = hco_df['Location']
    hco_df_harmonized['recipient_address'] = hco_df['Address Line 1'] + ' ' + hco_df['Address Line 2'] + ' ' + hco_df['Postcode']
    #hco_df_harmonized['recipient_city'] = 
    hco_df_harmonized['recipient_country'] = hco_df['Country']
    #hco_df_harmonized['recipient_person_title'] = 
    #hco_df_harmonized['recipient_person_first_name'] =
    #hco_df_harmonized['recipient_person_last_name'] = 
    #hco_df_harmonized['recipient_person_profession'] = 
    #hco_df_harmonized['recipient_person_specialty'] = 
    #hco_df_harmonized['recipient_person_role'] = 
    hco_df_harmonized['recipient_person_organization'] = hco_df['Institution Name']
    #hco_df_harmonized['recipient_identifier'] = 
    hco_df_harmonized['amount__donation_and_grant'] = hco_df['Donations and Grants to HCOs and Benefits in Kind to HCOs']
    hco_df_harmonized['amount__event_sponsorship'] = hco_df['Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an Event']
    hco_df_harmonized['amount__registration_fees'] = hco_df['Registration Fees']
    hco_df_harmonized['amount__travel_accommodation'] = hco_df['Travel & Accommodation']
    hco_df_harmonized['amount__service_and_consultancy_fees'] = hco_df['Fees']
    hco_df_harmonized['amount__service_and_consultancy_related_expenses'] = hco_df['Related expenses agreed in the fee for service or consultancy contract']
    hco_df_harmonized['amount__joint_working'] = hco_df['Joint Working Amount']
    hco_df_harmonized['detail__joint_working'] = hco_df['Joint Working Link']
    hco_df_harmonized['amount__total'] = hco_df['Total']


    # c) Aggregated dataframe

    aggregated_df = aggregated_df.reset_index()
    aggregated_df['agg_identifier'] = 'Aggregated'
    aggregated_df['id'] = aggregated_df['agg_identifier'] + '-' + aggregated_df['Pharma Company Name']

    # Differente dataframes depending on the mapping logic (R&D, amount, number, percentage)
    aggregated_df_rd = aggregated_df.loc[aggregated_df['Type of Value'] == 'Research and Development']
    aggregated_df_amt = aggregated_df.loc[(aggregated_df['Type of Value'] == 'HCP - Un-disclosed Aggregate Amount')|(aggregated_df['Type of Value'] == 'HCO - Un-disclosed Aggregate Amount')]
    aggregated_df_nbr = aggregated_df.loc[(aggregated_df['Type of Value'] == 'HCP - Un-disclosed Aggregate Number of Recipients')|(aggregated_df['Type of Value'] == 'HCO - Un-disclosed Aggregate Number of Recipients')]
    aggregated_df_perc = aggregated_df.loc[(aggregated_df['Type of Value'] == 'HCP - Un-disclosed Aggregate Recipients as % of All')|(aggregated_df['Type of Value'] == 'HCO - Un-disclosed Aggregate Recipients as % of All')]


    # RESEARCH & DEVELOPMENT
    aggregated_df_rd_harmonized['link_publication_identifier'] = aggregated_df_rd['id']
    aggregated_df_rd_harmonized['publication_year'] = aggregated_df_rd['Year of Disclosure']
    aggregated_df_rd_harmonized['source_organization_full_name'] = aggregated_df_rd['Pharma Company Name']
    aggregated_df_rd_harmonized['disclosure_type'] = "research and development"

    aggregated_df_rd_harmonized['amount__total'] = aggregated_df_rd['Total']


    ## AGGREATED HCP / HCO

    # Aggregated Amount

    aggregated_df_amt_harmonized = pd.DataFrame()
    aggregated_df_amt_harmonized['link_publication_identifier'] = aggregated_df_amt['id']
    aggregated_df_amt_harmonized['publication_year'] = aggregated_df_amt['Year of Disclosure']
    aggregated_df_amt_harmonized['source_organization_full_name'] = aggregated_df_amt['Pharma Company Name']
    aggregated_df_amt_harmonized['disclosure_type'] = "aggregated"
    aggregated_df_amt_harmonized['recipient_type'] = aggregated_df_amt['Type of Value'].astype(str).str[0:3]

    aggregated_df_amt_harmonized['amount__donation_and_grant'] = aggregated_df_amt['Donations and Grants to HCOs and Benefits in Kind to HCOs']
    aggregated_df_amt_harmonized['amount__event_sponsorship'] = aggregated_df_amt['Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an Event']
    aggregated_df_amt_harmonized['amount__registration_fees'] = aggregated_df_amt['Registration Fees']
    aggregated_df_amt_harmonized['amount__travel_accommodation'] = aggregated_df_amt['Travel & Accommodation']
    aggregated_df_amt_harmonized['amount__service_and_consultancy_fees'] = aggregated_df_amt['Fees']
    aggregated_df_amt_harmonized['amount__service_and_consultancy_related_expenses'] = aggregated_df_amt['Related expenses agreed in the fee for service or consultancy contract']
    #aggregated_df_amt_harmonized['amount__joint_working_link']
    #aggregated_df_amt_harmonized['detail__joint_working_link']
    aggregated_df_amt_harmonized['amount__total'] = aggregated_df_amt['Total']


    # Aggregated number of recipients

    aggregated_df_nbr_harmonized = pd.DataFrame()
    aggregated_df_nbr_harmonized['link_publication_identifier'] = aggregated_df_nbr['id']
    aggregated_df_nbr_harmonized['publication_year'] = aggregated_df_nbr['Year of Disclosure']
    aggregated_df_nbr_harmonized['source_organization_full_name'] = aggregated_df_nbr['Pharma Company Name']
    aggregated_df_nbr_harmonized['disclosure_type'] = "aggregated"
    aggregated_df_nbr_harmonized['recipient_type'] = aggregated_df_nbr['Type of Value'].astype(str).str[0:3]

    aggregated_df_nbr_harmonized['number_of_recipients__donation_and_grant'] = aggregated_df_nbr['Donations and Grants to HCOs and Benefits in Kind to HCOs']
    aggregated_df_nbr_harmonized['number_of_recipients__event_sponsorship'] = aggregated_df_nbr['Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an Event']
    aggregated_df_nbr_harmonized['number_of_recipients__registration_fees'] = aggregated_df_nbr['Registration Fees']
    aggregated_df_nbr_harmonized['number_of_recipients__travel_accommodation'] = aggregated_df_nbr['Travel & Accommodation']
    aggregated_df_nbr_harmonized['number_of_recipients__service_and_consultancy_fees'] = aggregated_df_nbr['Fees']
    aggregated_df_nbr_harmonized['number_of_recipients__service_and_consultancy_related_expenses'] = aggregated_df_nbr['Related expenses agreed in the fee for service or consultancy contract']

    # Aggregated recipients as percentage of all
    aggregated_df_perc_harmonized = pd.DataFrame()
    aggregated_df_perc_harmonized['link_publication_identifier'] = aggregated_df_perc['id']
    aggregated_df_perc_harmonized['publication_year'] = aggregated_df_perc['Year of Disclosure']
    aggregated_df_perc_harmonized['source_organization_full_name'] = aggregated_df_perc['Pharma Company Name']
    aggregated_df_perc_harmonized['disclosure_type'] = "aggregated"
    aggregated_df_perc_harmonized['recipient_type'] = aggregated_df_perc['Type of Value'].astype(str).str[0:3]

    aggregated_df_perc_harmonized['percentage_of_recipients__donation_and_grant'] = aggregated_df_perc['Donations and Grants to HCOs and Benefits in Kind to HCOs']
    aggregated_df_perc_harmonized['percentage_of_recipients__event_sponsorship'] = aggregated_df_perc['Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an Event']
    aggregated_df_perc_harmonized['percentage_of_recipients__registration_fees'] = aggregated_df_perc['Registration Fees']
    aggregated_df_perc_harmonized['percentage_of_recipients__travel_accommodation'] = aggregated_df_perc['Travel & Accommodation']
    aggregated_df_perc_harmonized['percentage_of_recipients__service_and_consultancy_fees'] = aggregated_df_perc['Fees']
    aggregated_df_perc_harmonized['percentage_of_recipients__service_and_consultancy_related_expenses'] = aggregated_df_perc['Related expenses agreed in the fee for service or consultancy contract']


    # set indexes to merge them horizontally
    aggregated_df_amt_harmonized.set_index(['source_organization_full_name', 'recipient_type'])
    aggregated_df_nbr_harmonized.set_index(['source_organization_full_name', 'recipient_type'])
    aggregated_df_perc_harmonized.set_index(['source_organization_full_name', 'recipient_type'])
    aggregated_df_rd_harmonized.set_index(['source_organization_full_name'])


    # merge aggregated DFs for HCPs and HCOs
    aggregated_df_hcp_hco = aggregated_df_amt_harmonized.merge(aggregated_df_nbr_harmonized).merge(aggregated_df_perc_harmonized)
    aggregated_df_hcp_hco = fit_dataframe_to_schema(aggregated_df_hcp_hco, link_schema)

    # concat vertically the 4 harmonized DFs
    final_df_harmonized = pd.concat([hco_df_harmonized, hcp_df_harmonized, aggregated_df_hcp_hco, aggregated_df_rd_harmonized], sort=False)
    final_df_harmonized.reset_index(drop=True, inplace=True)
    final_df_harmonized['link_publication_identifier'] = final_df_harmonized.index

    final_df_harmonized['currency'] = 'GBP'
    
    final_df_harmonized.to_csv(path_or_buf=f'{PROCESS_DIR}/uk/2_harmonized/uk__{year}.csv', index=False)


for year in [2016,2017,2018,2019]:
    harmonize_uk(year)
