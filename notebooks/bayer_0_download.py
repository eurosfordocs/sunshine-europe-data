import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
from src.pdf.utils import Efpia_Pdf_Parsing_Exception, diff_time
from src import utils
import time
import csv
from src.constants.efpia.header import Header
from src.pdf.process_df import RowType
from src.constants.schemas import PROCESS_DIR, NORMALIZED_DIR


logging.getLogger().setLevel(logging.INFO)

driver_path = "/Users/luc/Downloads/chromedriver_89"

chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(driver_path, options=chrome_options)
driver.implicitly_wait(5)


def get_header_from_text(header):
    if header == "Registration fees":
        return Header.REGISTRATION_FEES
    elif header == "Travel and accommodation":
        return Header.TRAVEL_ACCOMMODATION
    elif header == "Related expenses agreed in the fee for service or consultancy contract, including travel & accommodation relevant to the contract":
        return Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES
    elif header == "Fees":
        return Header.SERVICE_AND_CONSULTANCY_FEES
    elif header == "Donations and grants":
        return Header.DONATION_AND_GRANT
    elif header.startswith("Sponsorship agreements with HCOs"):
        return Header.EVENT_SPONSORSHIP
    elif header == "Fees":
        return Header.SERVICE_AND_CONSULTANCY_FEES


def get_individual_rows(driver, year, type, country):
    start_time = time.time()
    page = 1
    next_page = True
    rows = []

    # test if rows are showing, refresh otherwise

    url = f"http://transparency.bayer.com/search/?type={type}&year={year}&page={page}"

    if type == "hcp" and country == "spain":
        url += f"&full_name=dr."
    driver.get(url)

    if len(driver.find_elements_by_class_name("table-row")) == 0:
        logging.info("no rows, trying to refresh page")
        time.sleep(3)
        driver.get(url)

    if len(driver.find_elements_by_class_name("table-row")) == 0:
        logging.info("no rows, trying to refresh page")
        time.sleep(3)
        driver.get(url)

    if len(driver.find_elements_by_class_name("table-row")) == 0:
        logging.error("No rows after 3 tries, check what it going on. ")
        exit(1)

    # find number of pages
    last_page = 1
    if len(driver.find_elements_by_class_name("pagination")) > 0:
        driver.find_element_by_class_name("pagination").find_elements_by_tag_name("a")
        last_page = int(driver.find_element_by_class_name("pagination").find_elements_by_tag_name("a")[-2].text)
        logging.info(f"Last page is {last_page}")

    while page <= last_page:
        url = f"http://transparency.bayer.com/search/?type={type}&year={year}&page={page}"
        if type == "hcp" and country == "spain":
            url += f"&full_name=dr."
        driver.get(url)
        utils.print_progress_bar(page, last_page, f"Listing {type}s for {country} {year}")

        name_class, address_class, city_class = None, None, None
        headers = driver.find_element_by_class_name("table-header").find_elements_by_tag_name("div")

        for header in headers:
            if header.text == "Full name":
                name_class = header.get_attribute("class")
            if header.text == "Address":
                address_class = header.get_attribute("class")
            if header.text == "City":
                city_class = header.get_attribute("class")

        for table_row in driver.find_elements_by_class_name("table-row"):
            row = {}
            for k in list(Header):
                row[k.value] = None
            row[Header.ROW_TYPE] = RowType.HCP_IND if type == "hcp" else RowType.HCO_IND

            row[Header.FULL_NAME] = table_row.find_element_by_class_name(name_class).text
            row[Header.CITY] = table_row.find_element_by_class_name(city_class).text
            row[Header.COUNTRY] = country.capitalize()
            if address_class is not None:
                row[Header.ADDRESS] = table_row.find_element_by_class_name(address_class).text

            row["link"] = table_row.find_element_by_class_name("col-md-1").find_element_by_tag_name("a").get_attribute("href")

            row[Header.UNIQUE_IDENTIFIER] = table_row.find_element_by_class_name("col-md-1").find_element_by_tag_name("a").get_attribute("href")[53:]
            rows.append(row)
        page += 1

    logging.info(f"Found {len(rows)} {type}s on {page -1} pages for {country} {year}. Will scrape them individually now. ")
    i = 0
    for row in rows:
        i +=1
        if "link" not in row:
            logging.error(row)
        driver.get(row["link"])
        logging.debug(f"Getting details about {row[Header.FULL_NAME]} from link {row['link']}")
        for value_div in driver.find_elements_by_class_name("values"):
            header = value_div.find_element_by_class_name("col-sm-10").find_element_by_tag_name("b").text
            value = value_div.find_element_by_class_name("col-sm-6").find_element_by_tag_name("b").text
            if value != "0 EUR":
                row[get_header_from_text(header)] = value

        del row["link"]
        utils.print_progress_bar(i, len(rows), f"HCPs {year}")

    logging.info(f"{len(rows)} {type}s scraped in {diff_time(start_time)}")
    return rows


def get_aggregated_rows(driver, year, type, country):
    url = f"http://transparency.bayer.com/aggregated/?type={type}&year={year}"
    driver.get(url)

    value_row, recipients_row, percent_row = {}, {}, {}
    for k in list(Header):
        value_row[k.value], recipients_row[k.value], percent_row[k.value] = None, None, None


    for value_blocks in driver.find_elements_by_class_name("values-block"):
        current_header = Header.DONATION_AND_GRANT
        divs = value_blocks.find_elements_by_tag_name("div")
        for div in divs:
            if div.get_attribute("class") == "subtitle":
                current_header = get_header_from_text(div.text)
            elif div.get_attribute("class") == "values":
                row = None
                row_type = div.find_element_by_class_name("col-sm-10").text
                value = div.find_element_by_class_name("col-sm-6").text
                if "%" in row_type or "percentage" in row_type:
                    row = percent_row
                elif row_type.startswith("Number of recipients"):
                    row = recipients_row
                elif "amount" in row_type:
                    row = value_row
                else:
                    logging.error(f"unknown row type: {row_type}")
                row[current_header] = value

    if type == "hcp":
        value_row[Header.ROW_TYPE] = RowType.HCP_AGG_AMOUNT
        recipients_row[Header.ROW_TYPE] = RowType.HCP_AGG_RECIPIENTS
        percent_row[Header.ROW_TYPE] = RowType.HCP_AGG_PERCENT
    else:
        value_row[Header.ROW_TYPE] = RowType.HCO_AGG_AMOUNT
        recipients_row[Header.ROW_TYPE] = RowType.HCO_AGG_RECIPIENTS
        percent_row[Header.ROW_TYPE] = RowType.HCO_AGG_PERCENT
    logging.info(f"Scrapped aggregated {type} rows for {country} {year}")
    return [value_row, recipients_row, percent_row]


def get_rnd_row(driver, year, country):
    url = f"http://transparency.bayer.com/?year={year}"
    driver.get(url)
    div = driver.find_element_by_xpath("//div[@class='col-md-16 text-center content']")
    value = div.find_element_by_tag_name("span").text

    row = {}
    for k in list(Header):
        row[k.value] = None
    row[Header.TOTAL] = value
    row[Header.ROW_TYPE] = RowType.RND

    logging.info(f"Scrapped RND row for {country} {year}")
    return row


def download_country(country_code="DE", country="germany"):
    logging.info(country)
    try:
        extraction_dir = os.path.join(PROCESS_DIR, "bayer", "1_extracted")

        # The first url just sets the country, we don't need anything from this page
        first_url = f"http://transparency.bayer.com/{country_code}?language=en"
        driver.get(first_url)

        for year in ["2017", "2018", "2019"]:

            file = f"{country.lower()}__bayer__{year}.csv"
            if file not in os.listdir(extraction_dir):
                all_rows = []
                all_rows.extend(get_individual_rows(driver, year, type="hcp", country=country))
                all_rows.extend(get_aggregated_rows(driver, year, type="hcp", country=country))
                all_rows.extend(get_individual_rows(driver, year, type="hco", country=country))
                all_rows.extend(get_aggregated_rows(driver, year, type="hco", country=country))
                all_rows.extend([get_rnd_row(driver, year, country=country)])

                # write file:
                out_file = os.path.join(extraction_dir, f"{country.lower()}__bayer__{year}.csv")
                keys = all_rows[0].keys()
                with open(out_file, 'w', encoding="utf-8") as f:
                    dict_writer = csv.DictWriter(f, keys)
                    dict_writer.writeheader()
                    dict_writer.writerows(all_rows)
            else:
                logging.info(f"Skipping {country} / {year}, as the file already exists")




    except Exception as e:
        logging.error(e)
        pass


#download_country("ES", "spain")
download_country("SE", "sweden")
#download_country("DE", "germany")
#download_country("CH", "switzerland")
#download_country("IT", "italy")
#download_country("ES", "spain")


#download_country("BG", "bulgaria")
#download_country("EE", "estonia")
download_country("FI", "finland")
#download_country("GR", "greece")
#download_country("HR", "croatia")
#download_country("HU", "hungary")
#download_country("LT", "lithuania")
#download_country("LA", "latvia")
#download_country("LU", "luxembourg")
#download_country("MT", "malta")
#download_country("NO", "norway")
#download_country("PO", "poland")
#download_country("RS", "serbia")
#download_country("RU", "russia")
#download_country("RS", "serbia")
#download_country("SI", "slovenia")
#download_country("UA", "ukraine")


driver.close()
