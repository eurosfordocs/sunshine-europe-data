import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import logging
import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from src.pdf.utils import Efpia_Pdf_Parsing_Exception, diff_time
import time

from src.constants.schemas import PROCESS_DIR, NORMALIZED_DIR
import os
import json
import pandas as pd
from src.pdf.process_df import RowType

from src.constants.efpia.header import Header
from src.constants.efpia.header import VALUE_HEADERS
from src.pdf.utils import get_number_null_if_zero

from src.pdf.standardize import standardize


# +

logging.getLogger().setLevel(logging.INFO)
# -


downloads_dir = os.path.join(PROCESS_DIR, "spain_pfizer")
harmonized_dir = os.path.join(PROCESS_DIR,  "spain", "2_harmonized")

# get all json: do the process manually in a browser. Once you have the data, look in the inspector, the request looks like
# https://www.pfizer.es/api/Transparency.aspx?type=2&letra=&year=2017&sEcho=2&iColumns=12&sColumns=%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C&iDisplayStart=0&iDisplayLength=12000&mDataProp_0=FullName&sSearch_0=&bRegex_0=false&bSearchable_0=false&bSortable_0=false&mDataProp_1=City&sSearch_1=&bRegex_1=false&bSearchable_1=false&bSortable_1=false&mDataProp_2=Country&sSearch_2=&bRegex_2=false&bSearchable_2=false&bSortable_2=false&mDataProp_3=Address&sSearch_3=&bRegex_3=false&bSearchable_3=false&bSortable_3=false&mDataProp_4=DNI&sSearch_4=&bRegex_4=false&bSearchable_4=false&bSortable_4=false&mDataProp_5=Donation&sSearch_5=&bRegex_5=false&bSearchable_5=false&bSortable_5=false&mDataProp_6=SponsorshipAgreement&sSearch_6=&bRegex_6=false&bSearchable_6=false&bSortable_6=false&mDataProp_7=RegistrationFee&sSearch_7=&bRegex_7=false&bSearchable_7=false&bSortable_7=false&mDataProp_8=Travel&sSearch_8=&bRegex_8=false&bSearchable_8=false&bSortable_8=false&mDataProp_9=Fee&sSearch_9=&bRegex_9=false&bSearchable_9=false&bSortable_9=false&mDataProp_10=RelatedExpenses&sSearch_10=&bRegex_10=false&bSearchable_10=false&bSortable_10=false&mDataProp_11=Total&sSearch_11=&bRegex_11=false&bSearchable_11=false&bSortable_11=false&sSearch=&bRegex=false&iSortCol_0=0&sSortDir_0=asc&iSortingCols=1&_=1607275132321
# change iDisplayLength param to 12000. Then change the year manually and the type (1 is hcp, 2 is hco)

#rnd values are copied manually
rnd = {
    2017: 14212889.90,
    2018: 13843787.62,
    2019: 16043085.99
}


def parse_jsons():
    for year in [2017,2018,2019]:
        year_df = None
        for type in ["hcp", "hco"]:
            json_fname = f"spain__pfizer__{year}__{type}.json"
            with open(os.path.join(downloads_dir, json_fname), "r") as json_file:
                data = json.load(json_file)
                df = pd.DataFrame(data["aaData"])

                efpia_df = df.rename(columns={
                    'FullName': Header.FULL_NAME,
                    'City': Header.CITY,
                    'Country': Header.COUNTRY,
                    'Address': Header.ADDRESS,
                    'DNI': Header.UNIQUE_IDENTIFIER,
                    'Donation': Header.DONATION_AND_GRANT,
                    'SponsorshipAgreement': Header.EVENT_SPONSORSHIP,
                    'RegistrationFee': Header.REGISTRATION_FEES,
                    'Travel': Header.TRAVEL_ACCOMMODATION,
                    'Fee': Header.SERVICE_AND_CONSULTANCY_FEES,
                    'RelatedExpenses': Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
                    'Total': Header.TOTAL
                })
                efpia_df[Header.ROW_TYPE] = 1 if type == "hcp" else 5
                efpia_df[Header.RND] = None

                efpia_df = efpia_df.replace(to_replace="No aplica", value="")
                efpia_df = efpia_df.replace(to_replace="0,00", value="")

                efpia_df.apply(pd.to_numeric, errors='ignore')

                for h in VALUE_HEADERS:
                    efpia_df[h] = efpia_df[h].map(get_number_null_if_zero)

                if year_df is None:
                    year_df = efpia_df
                else:
                    year_df = year_df.append(efpia_df, ignore_index=True)


                logging.info(f"added {type} / {year}")

        rnd_row = {}
        for k in list(Header):
            rnd_row[k.value] = None
        rnd_row[Header.TOTAL] = rnd[year]
        rnd_row[Header.ROW_TYPE] = RowType.RND
        year_df = year_df.append(rnd_row, ignore_index=True)
        logging.info(f"added rnd row to {year}")

        standardized_df = standardize(year_df, year, company="pfizer", currency="EUR")
        csv_file = os.path.join(harmonized_dir, f"spain__pfizer__{year}.csv")
        standardized_df.to_csv(csv_file, index=False)

        logging.info(f"standardized {year}")

if __name__ == '__main__':
    parse_jsons()


