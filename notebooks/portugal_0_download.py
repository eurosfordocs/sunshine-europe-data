
import errno
import logging
import os
from retry import retry
from timeout_decorator import timeout, TimeoutError

logging.basicConfig(level=logging.INFO)

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

logging.getLogger().setLevel(logging.INFO)




class LoopingException(Exception):
    """
    raised when looping
    """
    pass



class PortugalCrawler():
    """
    Crawl the website for interests in pharma of portugal
    The website is a singlepage wich loads data with some JS driven request, not very friendly for a crawler
    
    """
    
    selectors = {
        "navigation_wait": "#ctl00_updateProgress[style*=\"visibility:hidden\"]",
        "current_page": "#tabs a.pager-class-selected",
        "farther_page": "#tabs td.pager-class:nth-last-child(2) > a",
        "farther_before_page": "#tabs td.pager-class:nth-child(2) > a",
        "before_page": "#tabs td.pager-class:nth-child(2) > a",
        "next_page": "#tabs td.pager-class:nth-last-child(1) > a",
        "data": "#ctl00_ContentPlaceHolder1_gvDoacoesPublic > tbody",
    }
    
    
    def __init__(self, start_url: str, year: int, save_path: str, driver_path: str, headless=False) -> None:
        """
        Currently progress_db is a redis instance
        """
        chrome_options = webdriver.ChromeOptions()
        if headless:
            chrome_options.add_argument('headless')
        chrome_options.add_argument('no-sandbox')
        
        self.chrome_options = chrome_options
        self.driver_path = driver_path
        self.url = start_url
        self.progress_db = {}
        self.year = year
        self.save_path = save_path


        try:
            os.makedirs(save_path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        
    def start(self):
        self.browser = webdriver.Chrome(self.driver_path, options=self.chrome_options)
        self.get_page(self.url)
        
    def wait_for_nav(self, timeout: int = 30, poll_frequency: float = 0.2):
        wait = WebDriverWait(self.browser, timeout, poll_frequency=poll_frequency)
        wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, self.selectors["navigation_wait"])))

    @retry(TimeoutError, tries=3)
    @timeout(20)
    def get_page(self, url: str):
        self.browser.get(url)
        
    def click(self, element_selector: str):
        self.browser.find_element_by_css_selector(element_selector).click()
        
    def click_navigation(self, css_selector: str):

        el = self.browser.find_element_by_css_selector(css_selector)

        ActionChains(self.browser).move_to_element(el).perform()
        el.click()
        self.wait_for_nav()
    
    def get_current_page_number(self) -> int:
        page_number = self.browser.find_element_by_css_selector(self.selectors["current_page"]).text
        try:
            int_page_number = int(page_number)
            return int_page_number
        except ValueError as ve:
            raise Exception("invalid string for a page number: " + str(ve))
        
    def goto(self, target_page_number: int) -> None:
        """
        Navigating in this website is slow and cumbersome
        """
        precedent_page = 0
        current_page = self.get_current_page_number()
        
        loop_count = 0
        
        while current_page != target_page_number:

            if current_page <= precedent_page:
                loop_count += 1
                logging.warning(f"looping {loop_count} times")

                if loop_count > 3:
                    raise LoopingException("We are looping at page " + str(current_page))
            
            if target_page_number > current_page:
                farther_page_number = int(self.browser.find_element_by_css_selector(self.selectors["farther_page"]).text)
                
                if current_page == farther_page_number:
                    # special case, when we jump from 1 to 20, we need to go on 21 to see 31
                    self.click_navigation(self.selectors["next_page"])
                    
                elif target_page_number >= farther_page_number:
                    # jump as far as we can
                    self.click_navigation(self.selectors["farther_page"])
                    
                elif target_page_number < farther_page_number:
                    # just iterate on next button until you're there
                    self.click_navigation(self.selectors["next_page"])
                    
                    
            else:
                logging.error("TODO: not handled")
                
            
            precedent_page = current_page
            current_page = self.get_current_page_number()

            logging.info(f"Jumped at page {current_page} from page {precedent_page}")
        logging.info(f"finally on page {current_page}")
        
    
    def save_html(self, year : str, filename, css_selector=""):
        #if year folder doens't exist, create it
        if year not in os.listdir(self.save_path):
            os.mkdir(os.path.join(self.save_path, year))

        filepath = os.path.join(self.save_path, year, filename)
        
        with open(filepath, "w", encoding="utf-8") as f:
            if not(css_selector == ""):
                html = self.browser.find_element_by_css_selector(css_selector).get_attribute("outerHTML")
                f.write(html)
            else: 
                html = self.browser.find_element_by_css_selector("body").get_attribute("outerHTML")
                f.write(html)
        logging.info(f"saved {filename}")
        
        
    
    def save_all_pages(self, starting_at_page: int = 0):
        on_last_page = False

        saved_pages = [x for x in os.listdir(os.path.join(self.save_path, str(year))) if x.endswith(".html")]
        if len(saved_pages) > 0:
            last_saved_page = sorted(saved_pages)[-1][:-5][18:]
            self.goto(int(last_saved_page) + 1)
        
        while not(on_last_page):
            try:
                current_page = self.get_current_page_number()
                f_name = f'portugal_{self.year}_page{current_page}.html'
                self.save_html(year=str(year), filename=f_name, css_selector=self.selectors["data"])
                self.go_to_next_page()
            except LoopingException as le:
                logging.info(str(le))
                on_last_page = True
                raise le
            
                
            
    
    def go_to_next_page(self):
        current_page = self.get_current_page_number()
        self.goto(current_page + 1)
        if current_page == self.get_current_page_number():
            raise LoopingException("We are looping at page " + str(current_page))
    
    def close(self):
        """
        Should be called at the end
        """
        self.browser.close()


# -


url = "https://placotrans.infarmed.pt/Publico/ListagemPublica.aspx"

# +
# logging.critical("critical")
# logging.error("error")
# logging.warning("warning")
# logging.info("info")
# logging.debug("debug")
# +


def scrap_portugal(year: int, starting_at: int = 1):
    save_path = os.path.join("data", "process_data", "portugal", "0_downloads")
    driver_path = "/Users/luc/Downloads/chromedriver"
    portugal_crawler = PortugalCrawler(url, headless=False, year=year, save_path=save_path, driver_path=driver_path)


    done = False

    while not(done):
        try:
            portugal_crawler.start()
            portugal_crawler.click_navigation(f'#ctl00_ContentPlaceHolder1_1_{year}')

            logging.info("Currently on page " + str(portugal_crawler.get_current_page_number()))
            portugal_crawler.save_all_pages(starting_at)

        except LoopingException as le:
            logging.error("looping :" + str(le))
            done = True
            
        finally:
            portugal_crawler.close()
# -
years = [2019, 2018, 2017, 2016, 2015, 2014, 2013]
for year in years:
    scrap_portugal(year)
