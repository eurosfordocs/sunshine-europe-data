import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import time
import io
import re
from src import utils
from src.pdf import utils as pdf_utils
import numpy as np
from src.denormalize import fit_dataframe_to_schema
from tableschema import Schema
link_schema = Schema('schemas/efpia/link.json')
import pandas as pd
from src.constants.schema_enums.efpia.link import Link as EL
from src.constants.value_constants import *
from src.constants.schemas import PROCESS_DIR
from src.pdf.utils import Efpia_Pdf_Parsing_Exception


FOLDER = "belgium"

hcp_dict_ids_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "hcp_dict_ids.csv"), dtype = str)
hcp_dict_ids_df.columns = ["hcp_id", "company_id", "col3", "full_name", "imani_id", "postal_code"]
hcp_dict_ids_df.set_index("hcp_id", inplace=True)

hco_dict_ids_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "hco_dict_ids.csv"), dtype = str)
hco_dict_ids_df.columns = ["hcp_id", "company_id", "col3", "full_name", "imani_id", "postal_code"]
hco_dict_ids_df.set_index("company_id", inplace=True)
hco_dict_ids_df = hco_dict_ids_df[~hco_dict_ids_df.index.duplicated(keep='first')]


po_dict_ids_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "po_dict_ids.csv"), dtype = str)
po_dict_ids_df.columns = ["hcp_id", "company_id", "col3", "full_name", "imani_id", "postal_code"]
po_dict_ids_df.set_index("company_id", inplace=True)
po_dict_ids_df = po_dict_ids_df[~po_dict_ids_df.index.duplicated(keep='first')]

html_folder = os.path.join(PROCESS_DIR, FOLDER, "html")
start = time.time()

if __name__ == '__main__':

    html_files = [x for x in os.listdir(html_folder) if x.endswith(".html")]
    html_files.sort(reverse=True)

    links_df = None
    i = 0
    for file in html_files:
        i += 1
        recipient_type = file.split("__")[0]
        recipient_id = file.split("__")[1]
        year = file.split("__")[2][0:4]

        df = pd.read_html(os.path.join(html_folder, file))[0]

        #drop 2 first rows, they are headers
        df = df.iloc[2:]
        df = df.replace(np.nan, "")

        #there are nbsp chars that come up as \xa0
        df = df.applymap(lambda x : str(x).replace(u'\xa0', u' '))

        #set columns
        if recipient_type == "hco":
            df.columns = [EL.SOURCE_ORGANIZATION_FULL_NAME,
                          EL.AMOUNT__DONATION_AND_GRANT,
                          EL.AMOUNT__EVENT_SPONSORSHIP,
                          EL.AMOUNT__REGISTRATION_FEES,
                          EL.AMOUNT__TRAVEL_ACCOMMODATION,
                          EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES,
                          EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES]
            df[EL.AMOUNT__DONATION_AND_GRANT] = df[EL.AMOUNT__DONATION_AND_GRANT].apply(lambda x: pdf_utils.get_number(x))
            df[EL.AMOUNT__EVENT_SPONSORSHIP] = df[EL.AMOUNT__EVENT_SPONSORSHIP].apply(lambda x: pdf_utils.get_number(x))
        elif recipient_type == "hcp":
            df.columns = [EL.SOURCE_ORGANIZATION_FULL_NAME,
                          EL.AMOUNT__REGISTRATION_FEES,
                          EL.AMOUNT__TRAVEL_ACCOMMODATION,
                          EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES,
                          EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES]

        elif recipient_type == "po":
            # PO html files are managed separately because of their extra categories. This is in the notebook belgium_4_normalize_po
            continue

            
            
        for c in [EL.AMOUNT__REGISTRATION_FEES, 
                  EL.AMOUNT__TRAVEL_ACCOMMODATION, 
                  EL.AMOUNT__SERVICE_AND_CONSULTANCY_FEES, 
                  EL.AMOUNT__SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
                  "financial_support",
                  "other_support"]:
             if c in df.columns: 
                    df[c] = df[c].apply(lambda x: pdf_utils.get_number(x))
     
        #add other columns
        df[EL.RECIPIENT_IDENTIFIER] = recipient_id
        df[EL.RECIPIENT_TYPE] = recipient_type.upper()
        df[EL.PUBLICATION_YEAR] = year
        df[EL.DISCLOSURE_TYPE] = DisclosureType.INDIVIDUAL

        # add fields related to the recipient

        if recipient_type == "hco":
            recipient = hco_dict_ids_df.loc[recipient_id]
        elif recipient_type == "hcp":
            recipient = hcp_dict_ids_df.loc[recipient_id]
            # full name example: CHAUMONT, PHILIPPE (Medical specialist in cardiology)
            regexp = re.compile('(.*),(.*)\((.*)\)')
            df[EL.RECIPIENT_PERSON_FIRST_NAME] = regexp.search(recipient["full_name"]).group(1).strip()
            df[EL.RECIPIENT_PERSON_LAST_NAME] =  regexp.search(recipient["full_name"]).group(2).strip()
            df[EL.RECIPIENT_PERSON_PROFESSION] = regexp.search(recipient["full_name"]).group(3).strip()
        else:
            recipient = po_dict_ids_df.loc[recipient_id]

        df[EL.RECIPIENT_FULL_NAME] = recipient["full_name"]
        df[EL.RECIPIENT_ADDRESS] = recipient["postal_code"]
        df[EL.RECIPIENT_COUNTRY] = "Belgium"
        df[EL.CURRENCY] = "EUR"

        df = fit_dataframe_to_schema(df, link_schema)

        if links_df is None:
            links_df = df
        else:
            links_df = links_df.append(df, ignore_index=True)

        utils.print_progress_bar(i, len(html_files), "Harmonize Belgium")


    links_df.reset_index(inplace=True, drop=True)
    links_df[EL.LINK_PUBLICATION_IDENTIFIER] = links_df.index
    links_df.to_csv(os.path.join(PROCESS_DIR, FOLDER, "2_harmonized", "link.csv"), index=False)
