# +

import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from src.pdf.utils import Efpia_Pdf_Parsing_Exception, diff_time
from src import utils
import time
import csv
from src.constants.efpia.header import Header
from src.pdf.process_df import RowType
from src.pdf.settings import settings
from src.constants.schemas import PROCESS_DIR
# -

import requests
from bs4 import BeautifulSoup

logging.getLogger().setLevel(logging.INFO)


def get_url(url):
    try:
        return requests.get(url, timeout=7)
    except Exception as x:
        logging.info("timeout, trying again")
        return requests.get(url, timeout=10)


def get_individual_rows(year, type, locale, country):
    start_time = time.time()
    page = 0
    rows = []

    # test if rows are showing, refresh otherwise

    next_page_exists = True


    while next_page_exists:

        url = f"https://public.janssentransferofvalue.com/{locale}/{type}-individual/{year}?page={page}"
        logging.info(url)

        response = get_url(url)
        soup = BeautifulSoup(response.content, "lxml")


        if len(soup.find_all(attrs={"class":"pager-next"})) == 0:
            next_page_exists = False

        table_headers = soup.table.thead.find_all("tr")[1].find_all("th")

        headers = {}
        for i in range(2, len(table_headers)):
            text = table_headers[i].text.strip(' \n\t')
            header = settings.get_exact_match(settings.Sections.HEADERS, text)
            if header is None:
                logging.error(f"unkonw header: {text}")

            else:
                headers[i] = header

        # absolutely horrible, but this is because germany has twice a header REISE UND UNTERKUNFT that means two diff things.
        headers[len(table_headers)-1] = Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES

        table_rows = soup.table.tbody.find_all("tr")
        for tr in table_rows:
            tds = tr.find_all("td")
            first_name = tds[1].text.strip(' \n\t')
            last_name = tds[0].text.strip(' \n\t')
            row = {
                Header.ROW_TYPE: RowType.HCP_IND if type == "hcp" else RowType.HCO_IND,
                Header.FULL_NAME: f"{first_name} {last_name}",
                Header.COUNTRY: country.capitalize()
            }
            for header_index in headers:
                row[headers[header_index]] = tds[header_index].text.strip(' \n\t')
            rows.append(row)
        page += 1
    logging.info(f"{len(rows)} {type}s scraped in {diff_time(start_time)}")

    return rows


def get_aggregated_rows(year, type, country, locale):
    url = f"https://public.janssentransferofvalue.com/{locale}/{type}-aggregated/{year}"
    response = get_url(url)
    soup = BeautifulSoup(response.content, "lxml")

    rows = []
    headers = {}
    if soup.table is None:
        return []

    table_headers = soup.table.find_all("thead")[1].tr.find_all("td")
     #   driver.find_element_by_class_name("hco-hpc-aggregated").find_elements_by_tag_name("thead")[1].find_element_by_tag_name("tr").find_elements_by_tag_name("TD")
    for i in range(1, len(table_headers)):
        header = settings.get_exact_match(settings.Sections.HEADERS, table_headers[i].text.strip(' \n\t'))
        if header is None:
            logging.error(f"unkonw header: {table_headers[i].text}")
            exit(1)
        headers[i] = header

    # absolutely horrible, but this is because germany has two header "REISE UND UNTERKUNFT", so manually changing the last one
    headers[len(table_headers)-1] = Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES

    table_rows = soup.table.tbody.find_all("tr")

    row_types = [RowType.HCP_AGG_AMOUNT, RowType.HCP_AGG_RECIPIENTS, RowType.HCP_AGG_PERCENT]
    if type == "hco":
        row_types = [RowType.HCO_AGG_AMOUNT, RowType.HCO_AGG_RECIPIENTS, RowType.HCO_AGG_PERCENT]

    for i in range(3):
        tds = table_rows[i].find_all("td")
        row = {
            Header.ROW_TYPE: row_types[i]
        }
        for header_index in headers:
            row[headers[header_index]] = tds[header_index].text.strip(' \n\t')
        rows.append(row)


    return rows

def get_rnd_row(year, locale):
    url =  f"https://public.janssentransferofvalue.com/{locale}/rnd-report/{year}"
    response = get_url(url)
    soup = BeautifulSoup(response.content, "lxml")

    row = {
        Header.ROW_TYPE: RowType.RND,
        Header.TOTAL: soup.find(attrs={"class":"body-value"}).text.strip(' \n\t')
    }
    return row


# https://public.janssentransferofvalue.com/de_de/hcp-aggregated/2017

def download_country(locale="de_de", folder="germany"):
    logging.info(folder)

    for year in ["2017", "2018", "2019"]:

        # hcp individual:

        file = f"{folder.lower()}__janssen__{year}.csv"
        out_dir = os.path.join(PROCESS_DIR, "janssen", "1_extracted")
        if file not in os.listdir(out_dir):
            all_rows = []
            all_rows.extend(get_individual_rows(year, type="hcp", locale = locale, country=folder))
            all_rows.extend(get_aggregated_rows(year, type="hcp", locale = locale, country=folder))
            all_rows.extend(get_individual_rows(year, type="hco", locale = locale, country=folder))
            all_rows.extend(get_aggregated_rows(year, type="hco", locale = locale, country=folder))
            all_rows.append(get_rnd_row(year, locale = locale))

            # write file:
            out_file = os.path.join(out_dir, file)

            keys = [h.value for h in list(Header)]
            with open(out_file, 'w', encoding="utf-8") as f:
                dict_writer = csv.DictWriter(f, keys)
                dict_writer.writeheader()
                dict_writer.writerows(all_rows)
        else:
            logging.info(f"Skipping {folder} / {year}, as the file already exists")


download_country("ch_fr", "switzerland")
download_country("es_es", "spain")
download_country("se_se", "sweden")
download_country("it_it", "italy")
download_country("de_de", "germany")


