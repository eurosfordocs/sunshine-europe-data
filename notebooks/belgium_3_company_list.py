# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Belgium 3 - Company related stuff
#
# This notebook does different things that are quite unique to the Belgium situation. It should run after belgium_harmonize. 

# ### imports

import os
import numpy as np
import requests
import urllib
import io
import re
import difflib
import pandas as pd
import logging
import time

if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")

# +
import src.utils as utils
from src.pdf import utils as pdf_utils
from src.constants.schema_enums.efpia.link import Link as EL
from src.constants.value_constants import *
from src.constants.schemas import PROCESS_DIR
from src.constants.schemas import NORMALIZED_DIR

FOLDER = "belgium"
# -

# ## finding companies ID
# from the links obtained previously, we get a list of unique company names, but we only have the name. We look them up again in betransparent.be and obtain to Belgium company ID. 
# We store the "raw" result as a csv in 0_downloads: company_ids.csv (same format as HCPs and HCOs)
# We save them as normalized entity__industry.csv

session_id = 'AWP_CSESSIONA3150588=AD7C2CFD84154231399DBD76140DB9589172FDA2'
url = 'https://extranet.betransparent.be/uk/index.awp'
headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Sec-GPC': '1',
        'Origin': 'https://extranet.betransparent.be',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://extranet.betransparent.be/uk/index.awp',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Cookie': session_id
    }


def request(raw_data, retry=0):
    try:
        r = requests.post(url=url, headers=headers, data=raw_data, timeout=10)
        return r
    except (requests.Timeout, requests.exceptions.ConnectionError):
        # wait 30 sec and retry
        logging.info(f"there was a timeout, waiting {(retry+1)*60} seconds and trying again")
        time.sleep((retry+1)*60)
        return request(raw_data, retry=retry+1)
        pass


def search_company(company):
    global companies_df
    global not_found_companies
    pa2 = urllib.parse.quote(company)
    raw_data = f"WD_ACTION_=AJAXEXECUTE&EXECUTEPROC=index.Search&WD_CONTEXTE_=A1&PA1=4&PA2={pa2}"
    r = request(raw_data)
    if len(r.text) > 0:
        df = pd.read_csv(io.StringIO(r.text), sep="\t", quotechar="'", header=None)
        df.columns = companies_df.columns
        companies_df = companies_df.append(df, ignore_index=True)    
    else: 
        not_found_companies.append(company)


def scrape_companies():
    global not_found_companies
    companies_df = pd.DataFrame(data=None, columns = ["hcp_id","hco_id","company_id","full_name","imani_id","postal_code"])
    not_found_companies = []
    companies_to_search = np.unique(companies_df["source_organization_full_name"].values)
    for c in companies_to_search: 
        search_company(c)
    assert len(not_found_companies) == 0 
    companies_df.drop_duplicates(inplace=True)
    companies_df.to_csv(os.path.join(PROCESS_DIR, FOLDER, "company_ids.csv"), index=False)
    return companies_df


fp = os.path.join(PROCESS_DIR, FOLDER, "company_ids.csv")
if (os.path.exists(fp)): 
    companies_df = pd.read_csv(fp)
else: 
    companies_df = scrape_companies

# +
df = pd.DataFrame(data=None, columns = [
    "entity_id",
    "directory_id",
    "directory_entity_id",
    "full_name",
    "is_person",
    "person_first_name",
    "person_last_name",
    "person_profession",
    "person_specialty",
    "person_organization_name",
    "entity_type",
    "organization_activity_area",
    "website",
    "location",
    "address",
    "city",
    "state_province_region",
    "postal_code",
    "country",
    "latitude",
    "longitude",
    "valid_since",
    "valid_until"
], dtype=str)
regexp = re.compile('(.*) \((.*)\)')

df["full_name"] = companies_df["full_name"].apply(lambda x : regexp.search(x).group(1))

# suffixing duplicates with _1
df['full_name'] = df['full_name'].where(~df['full_name'].duplicated(), df['full_name'] + '_1')

df["directory_entity_id"] = companies_df["full_name"].apply(lambda x : regexp.search(x).group(2))
df["directory_id"] = "belgium_company_id"
df["entity_id"] = df["full_name"].apply(lambda x : f"belgium__{x.replace(' ', '-').lower()}")
df["is_person"] = False
df["entity_type"] = "Industry"
df["country"] = "Belgium"

df.to_csv(os.path.join(NORMALIZED_DIR, FOLDER, "entity__industry.csv"), index=False)
# -

# ## parsing RND values
# We have the RND values in form of text files extracted from the images. We read them, ensure that we can match the company name in the companies that we have and then save them as harmonized links. 

#obtained from what comes at the end
replacement_map = {
    'MERZ PHARMA GMBH CO. KGAA': 'MERZ PHARMA GMBH & CO. KGAA',
    'BAUSCH LOMB PHARMA': 'BAUSCH & LOMB PHARMA',
    'JOHNSON JOHNSON MEDICAL': 'JOHNSON & JOHNSON MEDICAL',
    'OTSUKA PHARMACEUTICAL EUROPE LTD 1': 'OTSUKA PHARMACEUTICAL EUROPE LTD',
    'SMITH NEPHEW': 'SMITH & NEPHEW',
    'TAKEDA BELGIUM 1': 'TAKEDA BELGIUM',
    'VETOQUINOL': 'VÉTOQUINOL',
    'BAYER 1': 'BAYER'
}

links_rnd = pd.DataFrame(data=None, columns = [x.value for x in list(EL)])
for year in ["2017", "2018", "2019"]:
    rnd_folder = os.path.join(PROCESS_DIR, FOLDER, "rnd", year)
    rnd_files = [x for x in os.listdir(rnd_folder) if x.endswith(".txt")]
    for rnd_file in rnd_files:
        company_name = rnd_file.split("__")[2].replace("_", " ")
        if company_name in replacement_map: 
            company_name = replacement_map[company_name]
        year = rnd_file.split("__")[3][0:4]
        with open(os.path.join(rnd_folder, rnd_file), 'r') as f:
            text = f.read()
        rnd_value = pdf_utils.get_number(text)
        if rnd_value is not None and rnd_value > 0:
            row = {
                EL.SOURCE_ORGANIZATION_FULL_NAME: company_name.replace("_", " "),
                EL.DISCLOSURE_TYPE: DisclosureType.RND,
                EL.AMOUNT__TOTAL: rnd_value,
                EL.PUBLICATION_YEAR: year,
                EL.CURRENCY:"EUR",
                EL.RECIPIENT_COUNTRY:"Belgium"
            }
            links_rnd = links_rnd.append(row, ignore_index=True)

links_rnd.reset_index(inplace=True, drop=True)
links_rnd[EL.LINK_PUBLICATION_IDENTIFIER] = links_rnd.index
links_rnd[EL.LINK_PUBLICATION_IDENTIFIER] = links_rnd[EL.LINK_PUBLICATION_IDENTIFIER].apply(lambda x: f"rnd_{x}")
links_rnd.to_csv(os.path.join(PROCESS_DIR, FOLDER, "2_harmonized", "rnd.csv"), index=False)

# ## see if all RND companies are in the other companies

links_df = pd.read_csv(os.path.join(PROCESS_DIR, FOLDER, "2_harmonized", "link.csv"), dtype="str")

rnd_companies = np.unique(links_rnd["source_organization_full_name"].values)
other_companies = np.unique(links_df["source_organization_full_name"].values)

unfound_rnd_companies = [x for x in rnd_companies if x not in other_companies]
close_results = {}
for x in unfound_rnd_companies: 
    close_results[x] = difflib.get_close_matches(x, other_companies, n=1)
close_results

assert len(unfound_rnd_companies) == 0 
