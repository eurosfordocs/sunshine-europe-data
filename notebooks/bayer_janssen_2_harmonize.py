# ## Harmonize Bayer and Janssen
# This takes the csv files downloaded from bayer and janssen notebooks, and harmonizes them. The result goes into the country folder > 2_harmonized, so it is mixed with the rest of the country harmonized CSVs.  
# For bayer, lots of countries are downloaded but not used. 

import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")

import pandas

from src import utils
from src.constants.efpia.header import VALUE_HEADERS
from src.constants.schemas import PROCESS_DIR
from src.pdf.standardize import standardize
from src.pdf.utils import get_number_null_if_zero

logging.getLogger().setLevel(logging.INFO)


def harmonize_country(extracted_dir, country,  currency):
    for file in os.listdir(extracted_dir):
        if file.endswith(".csv") and file.startswith(f"{country}__"): #to avoid _ds_store files on macos
            with open(os.path.join(extracted_dir, file), "r") as f:
                raw_df = pandas.read_csv(f)
                for h in VALUE_HEADERS:
                    raw_df[h] = raw_df[h].map(get_number_null_if_zero)

                company, year, country_code = utils.parse_file_name(file.lower())
                df = standardize(raw_df, currency=currency, year=year, company=company)
                csv_file = os.path.join(PROCESS_DIR, country, "2_harmonized", file)
                df.to_csv(csv_file, index=False)
                logging.info(f"harmonized {file} to {csv_file}")


def harmonize_bayer():
    extracted_dir = os.path.join(PROCESS_DIR, "bayer", "1_extracted")
    harmonize_country(extracted_dir,"germany", "EUR")
    harmonize_country(extracted_dir,"switzerland", "CHF")
    harmonize_country(extracted_dir,"sweden", "SEK")
    harmonize_country(extracted_dir,"italy", "EUR")
    harmonize_country(extracted_dir,"spain", "EUR")


def harmonize_janssen():
    extracted_dir = os.path.join(PROCESS_DIR, "janssen", "1_extracted")
    harmonize_country(extracted_dir,"germany", "EUR")
    harmonize_country(extracted_dir,"switzerland", "CHF")
    harmonize_country(extracted_dir,"sweden", "SEK")
    harmonize_country(extracted_dir,"italy", "EUR")
    harmonize_country(extracted_dir,"spain", "EUR")


def harmonize_all():
    harmonize_bayer()
    harmonize_janssen()


if __name__ == '__main__':
    harmonize_all()


