# # Ireland part 2
# This step is rather simple, it parses the html files and saves the results as hamronized CSVs. If there are some company name changes you have to delete the harmonized CSV with the old names, otherwise the data is doubled. You can empty the harmonized folder before starting. 

import logging
import os
if os.path.split(os.getcwd())[-1] == "notebooks": 
    os.chdir("..")
from src.constants.schemas import PROCESS_DIR, NORMALIZED_DIR
from src.pdf.standardize import standardize
from src.pdf import process_df
from src import utils
from src.pdf.utils import Efpia_Pdf_Parsing_Exception
import pandas
from src.constants.efpia.header import Header
logging.getLogger().setLevel(logging.INFO)


FOLDER = "ireland"
headers = [
    Header.ROW_TYPE,
    Header.FULL_NAME,
    Header.CITY,
    Header.COUNTRY,
    Header.ADDRESS,
    Header.UNIQUE_IDENTIFIER,
    Header.DONATION_AND_GRANT,
    Header.EVENT_SPONSORSHIP,
    Header.REGISTRATION_FEES,
    Header.TRAVEL_ACCOMMODATION,
    Header.SERVICE_AND_CONSULTANCY_FEES,
    Header.SERVICE_AND_CONSULTANCY_RELATED_EXPENSES,
    Header.TOTAL
]

dl_dir = os.path.join(PROCESS_DIR, FOLDER, "0_downloads")
harmonized_dir = os.path.join(PROCESS_DIR, FOLDER, "2_harmonized")
normalized_dir = os.path.join(NORMALIZED_DIR, FOLDER)

rows_to_drop = [
    "INDIVIDUAL NAMED DISCLOSURE",
    "Total number of individual HCP disclosures",
    "Total number of individual HCO disclosures",
    "OTHER, NOT INCLUDED ABOVE",
    "AGGREGATE DISCLOSURE"
]
aggregate_rows = [
    "Aggregate amount attributable to transfers of value to such recipients",
    "Number of recipients in aggregate disclosure",
    "Of the total number of recipients whose ToV was disclosed, % disclosed in aggregate",
    "Research & Development Transfers of Value"
]


def pre_process_df(df):
    # drop certain rows
    for line, row in df.iterrows():
        for drop_value in rows_to_drop:
            if drop_value in str(row[Header.FULL_NAME]):
                df.drop(line, inplace=True)
                break

    # put indiv headers to empty, except full name, so that the normal process_df function can identify rows correctly
    for i, row in df.iterrows():
        if row[Header.FULL_NAME] in aggregate_rows:
            for h in [Header.COUNTRY, Header.CITY, Header.ADDRESS, Header.UNIQUE_IDENTIFIER]:
                df.at[i, h] = ""

    df.reset_index(drop=True, inplace=True)


if __name__ == '__main__':

    files = os.listdir(dl_dir)
    files.sort()

    html_count = sum(1 if f[-4:] == "html" else 0 for f in files)
    logging.info(f"Found {html_count} files to harmonize in folder {dl_dir}")

    i = 0
    for file in files:
        if file[-4:] == "html" and file [0:7] == "ireland":
            i += 1
            file_path = os.path.join(dl_dir, file)
            with open(file_path) as f:
                try:
                    df = pandas.read_html(f.read())[0]
                    df.columns = headers
                    pre_process_df(df)
                    df = process_df.process_df(df, file ,rnd_df=None )
                    company, year, country = utils.parse_file_name(file.lower())
                    standard_df = standardize(df, year, company, currency="EUR")

                    csv_file = os.path.join(harmonized_dir, f"{file.lower()[0:-5]}.csv")
                    standard_df.to_csv(csv_file, index=False)
                except Efpia_Pdf_Parsing_Exception as e:
                    logging.error(f"{file}: {e}")

            utils.print_progress_bar(i, html_count, "Harmonize")





