# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Scraping Denmark
#
#

# ## Imports

import requests
from bs4 import BeautifulSoup


# ## Fetch the denkmark

# Here the url...

url = "https://laegemiddelstyrelsen.dk/da/godkendelse/sundhedspersoners-tilknytning-til-virksomheder/lister-over-tilknytning-til-virksomheder/apotekere,-laeger,-sygeplejersker-og-tandlaeger/"

# A quick verification show us that we only display 10 entry per page. That's not enough !
# By looking at the URL params, we see we only need to add the param `max`

url += "?max=99999"

# However, the backend max it at 100. We still divided the number of necessary requests by 10 !
# Lets's fetch the HTML

r = requests.get(url)
# print(r.text)

# Now time to analyze it with beautiful soup
#
# The data is in `<table id="report">`, so the correct *CSS selector* is `#report`.

soup = BeautifulSoup(r.text, features="html.parser")
report = soup.find(id="report")


# Let's get the number of page we have.
#
# The form is quite simple : a `offset=INT` URL parameter is added to the URL. Since we're capped at *100* person per webpage, we have to increment this `offset` parameter by *100* to find the next data.
#
# When do we have to stop ? We need to look at the number of pages in total, here *31*, which can be found with the last element `a.step` and then multiply by 100.
#
# So here we juste have to fetch all the `[url + "&offset=" + 100*n for n in range(31)`

page_nav = soup.select("a.step")
last_page = int(page_nav[-1].get_text())

print(f"There is {last_page} pages to scrap")

urls = [f"{url}&offset={100*n}" for n in range(last_page)]


def fetch_and_save_html(url, filename, css_selector=""):
    with open(filename, "w", encoding="utf-8") as f:
        r = requests.get(url)
        if not(css_selector == ""):
            soup = BeautifulSoup(r.text, features="html.parser")
            f.write(str(soup.select(css_selector)[0]))
        else: 
            f.write(r.text)


filenames = [f"data/0_downloads/denmark/page_{n}.html" for n in range(last_page)]

for i in range(len(urls)):
    fetch_and_save_html(urls[i], filenames[i], "#report")
    print(f"{i}: saved {filenames[i]}")


